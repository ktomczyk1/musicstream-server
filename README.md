# Music Stream
Projekt z przedmiotu "Projektowanie mechatroniczne".  
Serwer i aplikacja klienta na system Windows do strumieniowania muzyki.  
(aplikacja mobilna w osobnym repozytorium)  
Wykona�:  
**Kamil Tomczyk**, Akademia G�rniczo-Hutnicza im. S.
Staszica w Krakowie,  
In�ynieria Mechatroniczna, stopie� II,
rok II (rok akad. 2019/20)
****
* Projekt klienta: *./MusicStreamClient*  
* Projekt serwera: *./MusicStreamServer*  
* Dokumentacja opisuj�ca komunikacj� mi�dzy serwerem i 
klientem: *./MusicStreamServer/CommDoc.md*  