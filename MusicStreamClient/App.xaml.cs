﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using MusicStreamServer;

namespace MusicStreamClient
{
    /// <summary>
    /// Main class of MusicStream client application.
    /// Provides functions for establishing and aborting connection
    /// with server and sending request to it.
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    public partial class App : Application
    {
        private static string CONN_ADRESS = "192.168.43.9";
        private static int CONN_PORT = 7200;

        /// <summary>
        /// After what time to check missing info (about songs, albums etc.) again 
        /// if not all has been already collected. In miliseconds.
        /// </summary>
        public const int MISSING_INFO_CHECK_DELAY = 200;

        private uint loggedUserId = 0;
        private string loggedUsername;
        public uint UserId => loggedUserId;
        public string Username => loggedUsername;

        Socket servSock;
        CommunicateListener commListener;

        public CommunicateListener GetCommunicateListener => commListener;

        App() : base()
        {
            LoadConf();
        }
        ~App()
        {
            try
            {
                commListener.Close();
                servSock.Disconnect(false);
                //Thread.Sleep(5000);
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Loads server configuration data from .conf file
        /// </summary>
        static void LoadConf()
        {
            try
            {
                FileInfo ufo = new FileInfo("./settings.conf");
                if (ufo.Exists)
                {
                    FileStream fs = ufo.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
                    StreamReader sr = new StreamReader(fs);
                    string line = "";
                    string[] elements;
                    while (!sr.EndOfStream)
                    {
                        line = sr.ReadLine();
                        if (line.Length > 0)
                        {
                            if (line[0] != '#' && line[0] != '\n')
                            {
                                if (line.Contains("="))
                                {
                                    elements = line.Split(new char[] { '=' }, 2);
                                    switch (elements[0].ToLower()) //checking key
                                    {
                                        case "ipadress":
                                            CONN_ADRESS = elements[1].Trim();
                                            break;
                                        case "port":
                                            try
                                            {
                                                CONN_PORT = int.Parse(elements[1].Trim());
                                            }
                                            catch (Exception) { }
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    sr.Close();
                }
                else
                {
                    FileStream fs = ufo.Open(FileMode.OpenOrCreate, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);
                    sw.WriteLine("# MusicStream server config file");
                    sw.WriteLine("ipadress=" + CONN_ADRESS);
                    sw.WriteLine("port=" + CONN_PORT.ToString());
                    sw.Close();
                }
            }
            catch (Exception)
            {
                Console.Write("ERROR: major error while configuration file reading.");
            }
        }

        /// <summary>
        /// Tries to connect with the server and log in the user.
        /// </summary>
        /// <param name="username">Username of the user to log in</param>
        /// <param name="password">Password of the user to log in</param>
        /// <returns>Result of try as communication prefix</returns>
        public COMM_PREFIX LoginTry(string username, string password)
        {
            //connecting
            IPAddress ipAddress = IPAddress.Parse(CONN_ADRESS);
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, CONN_PORT);
            servSock = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                byte[] loginRequest = new byte[256];
                servSock.Connect(localEndPoint);
                try
                {
                    //sending request
                    loginRequest[0] = (byte)COMM_PREFIX.COMMC_LOGIN_TRY;
                    byte[] usernameArr = new byte[20];
                    byte[] passHash = new byte[32];
                    usernameArr = Encoding.ASCII.GetBytes(username.PadRight(20));
                    using (SHA256 sha256Hash = SHA256.Create())
                    {
                        passHash = sha256Hash.ComputeHash(Encoding.ASCII.GetBytes(password.PadRight(20)));
                    }
                    Array.Copy(usernameArr, 0, loginRequest, 4, 20);
                    Array.Copy(passHash, 0, loginRequest, 32, 32);
                    servSock.Send(loginRequest);

                    //receiving answer
                    servSock.Receive(loginRequest);
                    if ((COMM_PREFIX)loginRequest[0] == COMM_PREFIX.COMMS_LOGIN_SUCCESS)
                    {
                        commListener = new CommunicateListener(servSock);
                        loggedUsername = username;
                        loggedUserId = BitConverter.ToUInt32(loginRequest, 1);
                    }
                    return (COMM_PREFIX)loginRequest[0];
                }
                catch (Exception) { }
            }
            catch (Exception) { }
            return COMM_PREFIX.COMM_FAILURE;
        }
        /// <summary>
        /// Tries to connect with the server, register the user and log in.
        /// </summary>
        /// <param name="username">Username of the user to register</param>
        /// <param name="password">Password of the user to register</param>
        /// <param name="email">Email adress of the user to register</param>
        /// <returns>A tuple containing:<list type="number">
        /// <item>Result of try to log in if registering suceeded 
        /// or result of try to register if it failed.</item>
        /// <item>Byte after prefix if register failed. (0 otherwise)</item>
        /// </list></returns>
        public Tuple<COMM_PREFIX, byte> RegisterTry(string username, string password, string email)
        {
            //connecting
            IPAddress ipAddress = IPAddress.Parse(CONN_ADRESS);
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, CONN_PORT);
            servSock = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                byte[] regRequest = new byte[256];
                servSock.Connect(localEndPoint);
                try
                {
                    //sending request
                    regRequest[0] = (byte)COMM_PREFIX.COMMC_REGISTER_TRY;
                    byte[] usernameArr = new byte[20];
                    byte[] passHash = new byte[32];
                    usernameArr = Encoding.ASCII.GetBytes(username.PadRight(20));
                    byte[] emailArr = Encoding.UTF8.GetBytes(email);
                    byte emailLen = (byte)Math.Min(emailArr.Length, 192);
                    using (SHA256 sha256Hash = SHA256.Create())
                    {
                        passHash = sha256Hash.ComputeHash(Encoding.ASCII.GetBytes(password.PadRight(20)));
                    }
                    regRequest[3] = emailLen;
                    Array.Copy(usernameArr, 0, regRequest, 4, 20);
                    Array.Copy(passHash, 0, regRequest, 32, 32);
                    Array.Copy(emailArr, 0, regRequest, 64, emailLen);
                    servSock.Send(regRequest);

                    //receiving answer
                    servSock.Receive(regRequest);
                    if((COMM_PREFIX)regRequest[0] == COMM_PREFIX.COMMS_REGISTER_SUCCESS)
                        return new Tuple<COMM_PREFIX, byte>(LoginTry(username, password), 0);
                    return new Tuple<COMM_PREFIX, byte>((COMM_PREFIX)regRequest[0], regRequest[1]);
                }
                catch (Exception) { }
            }
            catch (Exception) { }
            return new Tuple<COMM_PREFIX, byte>(COMM_PREFIX.COMM_FAILURE,0);
        }
        /// <summary>
        /// Logs out the user and disconnects the socket connection.
        /// </summary>
        public void Logout()
        {
            commListener.Close();
            byte[] request = new byte[256];
            request[0] = (byte)COMM_PREFIX.COMMC_LOGOUT_REQ;
            lock (servSock)
                servSock.Send(request);
            servSock.Disconnect(true);
        }
        /// <summary>
        /// Sends the answer to confirm user actibity
        /// </summary>
        public void ConfirmActivity()
        {
            if (!servSock.Connected)
                throw new Exception("Socket not available.");
            byte[] request = new byte[256];
            request[0] = (byte)COMM_PREFIX.COMMC_REACT_ACTIVE;
            lock (servSock)
                servSock.Send(request);
        }

        //request sending methods, each can throw Exception if socket is not available
        /// <summary>
        /// Sends search request to the server.
        /// </summary>
        /// <param name="query">Search query (if empty string, show everything)</param>
        /// <param name="places">Places to search, see <see cref="MusicStreamServer.CommDoc.md"/></param>
        /// <param name="order">Order of result sorting, see <see cref="MusicStreamServer.CommDoc.md"/></param>
        /// <param name="skip">How many results from the beginning to skip</param>
        /// <param name="isAlbum">True if searching albums, false if searching songs</param>
        public void SendSearchRequest(string query, byte places, byte order, uint skip, bool isAlbum)
        {
            if (!servSock.Connected)
                throw new Exception("Socket not available.");
            byte[] request = new byte[256];
            request[0] = (byte)(isAlbum ? COMM_PREFIX.COMMC_SEARCH_ALBUMS : COMM_PREFIX.COMMC_SEARCH_SONGS);
            request[1] = (byte)( ((order << 4) & 0xf0) | (places & 0x0f) );
            if (query.Length > 120)
                query = query.Substring(0, 120);
            Array.Copy(BitConverter.GetBytes(skip), 0, request, 4, 4);
            byte[] bQuery = Encoding.UTF8.GetBytes(query);
            request[2] = (byte)bQuery.Length;
            for (int i = 0; i < bQuery.Length; i++)
                request[i + 16] = bQuery[i];
            lock (servSock)
                servSock.Send(request);
        }
        /// <summary>
        /// Sends request for song or album informations to server.
        /// </summary>
        /// <param name="id">ID of song or album</param>
        /// <param name="isAlbum">True if requesting album information, 
        /// false if requesting song information</param>
        public void SendInfoRequest(uint id, bool isAlbum)
        {
            if (!servSock.Connected)
                throw new Exception("Socket not available.");
            byte[] request = new byte[256];
            request[0] = (byte)(isAlbum ? COMM_PREFIX.COMMC_INFO_ALBUM : COMM_PREFIX.COMMC_INFO_SONG);
            Array.Copy(BitConverter.GetBytes(id), 0, request, 4, 4);
            lock (servSock)
                servSock.Send(request);
        }
        /// <summary>
        /// Sends request for list of playlist IDs belonging to logged in user.
        /// </summary>
        public void SendPlaylistRequest() { SendPlaylistRequest(0); }
        /// <summary>
        /// Sends request for playlist information (and thus list of song IDs it's composed of)
        /// or list of playlist IDs belonging to logged in user.
        /// </summary>
        /// <param name="id">ID of playlist or 0 if requesting list of playlist IDs</param>
        public void SendPlaylistRequest(uint id)
        {
            if (!servSock.Connected)
                throw new Exception("Socket not available.");
            byte[] request = new byte[256];
            if (id != 0)
            {
                request[0] = (byte)COMM_PREFIX.COMMC_PLAYLIST_LOAD;
                Array.Copy(BitConverter.GetBytes(id), 0, request, 1, 4);
            }
            else
                request[0] = (byte)COMM_PREFIX.COMMC_PLAYLIST_GETLIST;
            lock(servSock)
                servSock.Send(request);
        }
        /// <summary>
        /// Sends request for saving a playlist to server.
        /// </summary>
        /// <param name="list">Playlist to save</param>
        public void SendPlaylistSaveRequest(Playlist list)
        {
            if (!servSock.Connected)
                throw new Exception("Socket not available.");
            byte[] request = new byte[256];
            request[0] = (byte)COMM_PREFIX.COMMC_PLAYLIST_SAVE;
            Array.Copy(BitConverter.GetBytes(list.CreationTime.Ticks), 0, request, 1, 8);
            byte songcount = (byte)Math.Min(list.Songs.Count,48);
            request[9] = songcount;
            Array.Copy(BitConverter.GetBytes((ushort)list.Length), 0, request, 10, 2);
            Array.Copy(Encoding.UTF8.GetBytes(list.Name.PadRight(52)), 0, request, 12, 52);
            for (int i = 0; i < songcount; i++)
                Array.Copy(BitConverter.GetBytes(list.Songs[i]), 0, request, 64 + 4 * i, 4);
            lock (servSock)
                servSock.Send(request);
        }
        /// <summary>
        /// Sends request for coverart data stream to server.
        /// </summary>
        /// <param name="id">Album ID</param>
        /// <param name="offset">Offset from beginning of image data, in bytes</param>
        public void SendCoverartRequest(uint id, uint offset)
        {
            if (!servSock.Connected)
                throw new Exception("Socket not available.");
            byte[] request = new byte[256];
            if(id!=0)
            request[0] = (byte)COMM_PREFIX.COMMC_COVERART_REQUEST;
            Array.Copy(BitConverter.GetBytes(id), 0, request, 1, 4);
            Array.Copy(BitConverter.GetBytes(offset), 0, request, 5, 4);
            lock (servSock)
                servSock.Send(request);
            commListener.WaitForLongMessage();
        }
        /// <summary>
        /// Sends request for song data stream to server.
        /// </summary>
        /// <param name="id">Song ID</param>
        /// <param name="startingPacketIndex">Index of the first requested packet to send, 
        /// packets have fixed size, see:
        /// <see cref="Song.BYTES_PER_SECTION"/></param>
        public void SendSongStreamRequest(uint id, uint startingPacketIndex)
        {
            if (!servSock.Connected)
                throw new Exception("Socket not available.");
            byte[] request = new byte[256];
            request[0] = (byte)COMM_PREFIX.COMMC_SONG_REQUEST;
            Array.Copy(BitConverter.GetBytes(id), 0, request, 1, 4);
            Array.Copy(BitConverter.GetBytes(startingPacketIndex), 0, request, 5, 4);
            lock (servSock)
                servSock.Send(request);
            commListener.WaitForLongMessage();
        }
    }
}
