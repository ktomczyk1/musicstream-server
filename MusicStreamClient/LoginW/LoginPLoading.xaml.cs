﻿using System.Windows.Controls;

namespace MusicStreamClient.LoginW
{
    /// <summary>
    /// Page containing idle loading animation
    /// </summary>
    public partial class LoginPLoading : Page
    {
        public LoginPLoading()
        {
            InitializeComponent();
            LoadingLabel.Content = globNames.loginLoading;
        }
    }
}
