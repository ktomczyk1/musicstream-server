﻿using System;
using System.Windows;
using System.Windows.Controls;
using MusicStreamServer;

namespace MusicStreamClient.LoginPages
{
    /// <summary>
    /// Page containing login form
    /// Starting page of LoginWindow
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    public partial class LoginPLogin : Page
    {
        public LoginPLogin()
        {
            InitializeComponent();

            //setting the localized names
            Title = "Music Stream - " + globNames.loginLogin;
            LoginHeader.Content = globNames.loginLogin;
            UsernameLabel.Content = globNames.loginUsername + ":";
            PasswordLabel.Content = globNames.loginPassword + ":";
            LoginButton.Content = globNames.loginLoginButton;
            RegisterLink.Content = globNames.loginRegister;

            FailureLabel.Content = " ";
        }

        /// <summary>
        /// Event handler for button "RegisterLink" click.
        /// Changes current page of login window to register page.
        /// </summary>
        private void GoToRegisterPage(object sender, RoutedEventArgs e)
        {
            LoginWindow loginMainWindow = (LoginWindow) Application.Current.MainWindow;
            if (loginMainWindow != null)
            {
                loginMainWindow.mainFrame.Source = new Uri("LoginPRegister.xaml", UriKind.Relative);
                loginMainWindow.Title = "Music Stream - " + globNames.loginRegister;
            }
        }

        /// <summary>
        /// Event handler for button "LoginButton" click.
        /// Tries to log in with entered data and reacts accordingly.
        /// </summary>
        private void EnterLogin(object sender, RoutedEventArgs e) { EnterLoginTry(); }
        /// <see cref="EnterLogin(object, RoutedEventArgs)"/>
        /// <returns>True if logged in successfully. False otherwise.</returns>
        private bool EnterLoginTry()
        {
            //checking if there is any username entered
            if(UsernameBox.Text == "")
            {
                FailureLabel.Content = globNames.loginRegvalUsername;
                return false;
            }
            string username = UsernameBox.Text;
            string password = PasswordBox.Password;

            //changing page to loading
            LoginWindow loginMainWindow = (LoginWindow)Application.Current.MainWindow;
            if (loginMainWindow != null)
                loginMainWindow.mainFrame.Navigate(new LoginW.LoginPLoading());
            else
                return false;

            //trying to login
            App currApp = (App)Application.Current;
            COMM_PREFIX loginResult = currApp.LoginTry(username, password);
            switch(loginResult)
            {
                case COMM_PREFIX.COMMS_LOGIN_FAILED_UNKNOWN:
                    {
                        loginMainWindow.mainFrame.Navigate(this);
                        FailureLabel.Content = globNames.loginFailureGeneral;
                        return false;
                    }
                case COMM_PREFIX.COMMS_LOGIN_FAILED_USERNAME:   //incorrect username
                    {
                        loginMainWindow.mainFrame.Navigate(this);
                        FailureLabel.Content = globNames.loginFailureUsername;
                        return false;
                    }
                case COMM_PREFIX.COMMS_LOGIN_FAILED_PASSWORD:   //incorrect password
                    {
                        loginMainWindow.mainFrame.Navigate(this);
                        FailureLabel.Content = globNames.loginFailurePassword;
                        return false;
                    }
                case COMM_PREFIX.COMMS_LOGIN_SUCCESS:           //logged in successfully
                    {
                        currApp.MainWindow = new MainClientWindow();
                        currApp.MainWindow.Show();
                        loginMainWindow.Close();
                        return true;
                    }
                case COMM_PREFIX.COMM_FAILURE:
                    {
                        loginMainWindow.mainFrame.Navigate(this);
                        FailureLabel.Content = globNames.loginFailureConnection;
                        return false;
                    }
            }
            return false;
        }
    }
}
