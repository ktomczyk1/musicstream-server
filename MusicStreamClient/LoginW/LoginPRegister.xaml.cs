﻿using MusicStreamServer;
using System;
using System.Windows;
using System.Windows.Controls;

namespace MusicStreamClient
{
    /// <summary>
    /// Page containing register form
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    public partial class LoginPRegister : Page
    {
        public LoginPRegister()
        {
            InitializeComponent();

            //setting the localized names
            Title = "Music Stream - " + globNames.loginRegister;
            RegisterHeader.Content = globNames.loginRegister;
            UsernameLabel.Content = globNames.loginUsername + ":";
            //EmailLabel.Content = globNames.loginEmail + ":";
            PasswordLabel.Content = globNames.loginPassword + ":";
            RepPassLabel.Content = globNames.loginRepeatPassword + ":";
            RegisterButton.Content = globNames.loginRegisterButton;
            LoginLink.Content = globNames.loginLogin;

            FailureLabel.Content = " ";
        }

        /// <summary>
        /// Event handler for button "RegisterButton" click.
        /// Tries to log in with entered data and reacts accordingly.
        /// </summary>
        private void EnterRegister(object sender, RoutedEventArgs e) { EnterRegisterTry(); }
        /// <see cref="EnterRegister(object, RoutedEventArgs)"/>
        /// <returns>True if registered succesfully, false otherwise</returns>
        private bool EnterRegisterTry()
        {
            //validation of entered data
            if(UsernameBox.Text == "")
            {
                FailureLabel.Content = globNames.loginRegvalUsername;
                return false;
            }
            /*if(EmailBox.Text == "")
            {
                FailureLabel.Content = globNames.loginRegvalEmailEmpty;
                return false;
            }*/
            if(PasswordBox.Password.Length < 4)
            {
                FailureLabel.Content = globNames.loginRegvalPasswordShort;
                return false;
            }
            if(PasswordBox.Password != RepPassBox.Password)
            {
                FailureLabel.Content = globNames.loginRegvalPasswordMatch;
                return false;
            }
            string username = UsernameBox.Text;
            //string email = EmailBox.Text;
            string email = username + "@mail";
            string password = PasswordBox.Password;

            //changing page to loading
            LoginWindow loginMainWindow = (LoginWindow)Application.Current.MainWindow;
            if (loginMainWindow != null)
                loginMainWindow.mainFrame.Source = new Uri("LoginPLoading.xaml", UriKind.Relative);
            else
                return false;

            //trying to register
            App currApp = (App)Application.Current;
            Tuple<COMM_PREFIX, byte> registerResult = currApp.RegisterTry(username, password, email);
            switch(registerResult.Item1)
            {
                case COMM_PREFIX.COMMS_LOGIN_SUCCESS:       //registered and logged in
                    currApp.MainWindow = new MainClientWindow();
                    currApp.MainWindow.Show();
                    loginMainWindow.Close();
                    return true;
                case COMM_PREFIX.COMMS_LOGIN_FAILED_INUSE:      //registered but failed to login
                case COMM_PREFIX.COMMS_LOGIN_FAILED_PASSWORD:
                case COMM_PREFIX.COMMS_LOGIN_FAILED_USERNAME:
                case COMM_PREFIX.COMMS_LOGIN_FAILED_UNKNOWN:
                    if (loginMainWindow != null)
                    {
                        loginMainWindow.mainFrame.Source = new Uri("LoginPLogin.xaml", UriKind.Relative);
                        loginMainWindow.Title = "Music Stream - " + globNames.loginLogin;
                    }
                    return true;
                case COMM_PREFIX.COMMS_REGISTER_FAILED_TAKEN:   //email or username taken
                    loginMainWindow.mainFrame.Navigate(this);
                    if (registerResult.Item2 > 1)
                        FailureLabel.Content = globNames.loginFailureEmailTaken;
                    else
                        FailureLabel.Content = globNames.loginFailureUsernameTaken;
                    return false;
                case COMM_PREFIX.COMMS_REGISTER_FAILED_OTHER:   //unknown failure
                    loginMainWindow.mainFrame.Navigate(this);
                    FailureLabel.Content = globNames.loginFailureGeneralRegister;
                    return false;
                case COMM_PREFIX.COMM_FAILURE:          //communication failure
                    loginMainWindow.mainFrame.Navigate(this);
                    FailureLabel.Content = globNames.loginFailureConnection;
                    return false;
            }
            return false;
        }

        /// <summary>
        /// Event handler for button "LoginLink" click.
        /// Changes current page of login window to login page.
        /// </summary>
        private void GoToLoginPage(object sender, RoutedEventArgs e)
        {
            LoginWindow loginMainWindow = (LoginWindow)Application.Current.MainWindow;
            if (loginMainWindow != null)
            {
                loginMainWindow.mainFrame.Source = new Uri("LoginPLogin.xaml", UriKind.Relative);
                loginMainWindow.Title = "Music Stream - " + globNames.loginLogin;
            }
        }
    }
}
