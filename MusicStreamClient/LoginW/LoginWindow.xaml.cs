﻿using System.Windows;

namespace MusicStreamClient
{
    /// <summary>
    /// Window displaying forms to login or register
    /// Starting window of application
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
            Title = "Music Stream - " + globNames.loginLogin;
        }

    }
    
}
