﻿<Application x:Class="MusicStreamClient.App"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:local="clr-namespace:MusicStreamClient"
             StartupUri="LoginW/LoginWindow.xaml">
    <Application.Resources>
        <ResourceDictionary>
            <Color x:Key="MainBackgroundColor">#2040c0</Color>
            <Color x:Key="DarkBackgroundColor">#0020a0</Color>
            <Color x:Key="MainTextColor">#c0c0ff</Color>
            <Color x:Key="DarkTextColor">#8080c0</Color>
            <SolidColorBrush x:Key="MainBackgroundBrush" Color="{StaticResource MainBackgroundColor}"/>
            <SolidColorBrush x:Key="DarkBackgroundBrush" Color="{StaticResource DarkBackgroundColor}"/>
            <SolidColorBrush x:Key="MainTextBrush" Color="{StaticResource MainTextColor}"/>
            <SolidColorBrush x:Key="DarkTextBrush" Color="{StaticResource DarkTextColor}"/>


            <!-- scroll bar redesign -->
            <Style x:Key="NewScrollBarRepeatButton" TargetType="{x:Type RepeatButton}">
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type RepeatButton}">
                            <Border BorderThickness="{TemplateBinding BorderThickness}"
                                    BorderBrush="{TemplateBinding BorderBrush}"
                                    Background="{TemplateBinding Background}"/>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
                <Setter Property="Background" Value="{StaticResource DarkBackgroundBrush}"/>
                <Setter Property="BorderThickness" Value="1"/>
                <Setter Property="BorderBrush" Value="{StaticResource MainBackgroundBrush}"/>
            </Style>
            <Style x:Key="NewScrollBarThumb" TargetType="{x:Type Thumb}">
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type Thumb}">
                            <Border Background="{TemplateBinding Background}"
                                    BorderBrush="{TemplateBinding BorderBrush}"
                                    BorderThickness="{TemplateBinding BorderThickness}"/>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
                <Setter Property="Background" Value="{StaticResource MainBackgroundBrush}"/>
                <Setter Property="BorderThickness" Value="1 0"/>
                <Setter Property="BorderBrush" Value="{StaticResource MainBackgroundBrush}"/>
                <Style.Triggers>
                    <Trigger Property="IsMouseOver" Value="True">
                        <Setter Property="Background" Value="{StaticResource DarkTextBrush}"/>
                    </Trigger>
                    <Trigger Property="IsDragging" Value="True">
                        <Setter Property="Background" Value="{StaticResource MainTextBrush}"/>
                    </Trigger>
                </Style.Triggers>
            </Style>
            <ControlTemplate x:Key="NewVerticalScrollBar" TargetType="{x:Type ScrollBar}">
                <Track Name="PART_Track" IsDirectionReversed="True">
                    <Track.DecreaseRepeatButton>
                        <RepeatButton Command="ScrollBar.PageUpCommand" 
                                      Style="{StaticResource NewScrollBarRepeatButton}"/>
                    </Track.DecreaseRepeatButton>
                    <Track.Thumb>
                        <Thumb Style="{StaticResource NewScrollBarThumb}"/>
                    </Track.Thumb>
                    <Track.IncreaseRepeatButton>
                        <RepeatButton Command="ScrollBar.PageDownCommand"
                                      Style="{StaticResource NewScrollBarRepeatButton}"/>
                    </Track.IncreaseRepeatButton>
                </Track>
            </ControlTemplate>
            <ControlTemplate x:Key="NewHorizontalScrollBar" TargetType="{x:Type ScrollBar}">
                <Track Name="PART_Track" IsDirectionReversed="False">
                    <Track.DecreaseRepeatButton>
                        <RepeatButton Command="ScrollBar.PageLeftCommand" 
                                      Style="{StaticResource NewScrollBarRepeatButton}"/>
                    </Track.DecreaseRepeatButton>
                    <Track.Thumb>
                        <Thumb Style="{StaticResource NewScrollBarThumb}" BorderThickness="0 1"/>
                    </Track.Thumb>
                    <Track.IncreaseRepeatButton>
                        <RepeatButton Command="ScrollBar.PageRightCommand"
                                      Style="{StaticResource NewScrollBarRepeatButton}"/>
                    </Track.IncreaseRepeatButton>
                </Track>
            </ControlTemplate>
            <Style TargetType="{x:Type ScrollBar}">
                <Setter Property="Template" Value="{StaticResource NewVerticalScrollBar}"/>
                <Setter Property="Background" Value="{StaticResource DarkBackgroundBrush}"/>
                <Style.Triggers>
                    <Trigger Property="Orientation" Value="Horizontal">
                        <Setter Property="Template" Value="{StaticResource NewHorizontalScrollBar}"/>
                    </Trigger>
                </Style.Triggers>
            </Style>
            

            <!-- main styles for app -->
            <Style x:Key="FlatButton" TargetType="{x:Type Button}">
                <Setter Property="Background" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="Foreground" Value="{StaticResource MainBackgroundBrush}"/>
                <Setter Property="BorderBrush" Value="Transparent"/>
                <Setter Property="BorderThickness" Value="2"/>
                <Setter Property="FontWeight" Value="Bold"/>
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type Button}">
                            <Border Background="{TemplateBinding Background}" 
                                    BorderBrush="{TemplateBinding BorderBrush}" 
                                    BorderThickness="{TemplateBinding BorderThickness}"
                                    Padding="{TemplateBinding Padding}">
                                <ContentPresenter HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" 
                                                  VerticalAlignment="{TemplateBinding VerticalContentAlignment}"/>
                            </Border>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
                <Style.Triggers>
                    <Trigger Property="IsMouseOver" Value="True">
                        <Setter Property="Background" Value="{StaticResource MainBackgroundBrush}"/>
                        <Setter Property="Foreground" Value="{StaticResource MainTextBrush}"/>
                        <Setter Property="BorderBrush" Value="{StaticResource MainTextBrush}"/>
                    </Trigger>
                </Style.Triggers>
            </Style>
            <Style x:Key="RadioFlat" TargetType="{x:Type RadioButton}">
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type RadioButton}">
                            <StackPanel Orientation="Horizontal">
                                <Canvas Height="{TemplateBinding Height}" Width="{TemplateBinding Height}">
                                    <Ellipse Fill="{TemplateBinding Background}" 
                                             Stroke="{TemplateBinding BorderBrush}"
                                             StrokeThickness="{Binding RelativeSource={RelativeSource TemplatedParent}, 
                                                                       Path=BorderThickness.Top}"
                                             Height="{TemplateBinding Height}" Width="{TemplateBinding Height}"
                                             Canvas.Top="0" Canvas.Left="0"/>
                                </Canvas>
                                <Frame Width="5"/>
                                <ContentPresenter HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" 
                                                  VerticalAlignment="{TemplateBinding VerticalContentAlignment}"/>
                            </StackPanel>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
                <Setter Property="Foreground" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="BorderBrush" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="BorderThickness" Value="3"/>
                <Setter Property="VerticalContentAlignment" Value="Top" />
                <Setter Property="Height" Value="16"/>
                <Setter Property="Background" Value="{StaticResource MainBackgroundBrush}"/>
                <Setter Property="Margin" Value="3"/>
                <Setter Property="FontWeight" Value="Normal"/>
                <Setter Property="FontSize" Value="12"/>
                <Style.Triggers>
                    <Trigger Property="IsChecked" Value="True">
                        <Setter Property="Background" Value="{StaticResource MainTextBrush}"/>
                    </Trigger>
                    <MultiTrigger>
                        <MultiTrigger.Conditions>
                            <Condition Property="IsMouseOver" Value="True"/>
                            <Condition Property="IsChecked" Value="False"/>
                        </MultiTrigger.Conditions>
                        <Setter Property="Background" Value="{StaticResource DarkTextBrush}"/>
                    </MultiTrigger>
                </Style.Triggers>
            </Style>
            <Style x:Key="CheckFlat" TargetType="{x:Type CheckBox}">
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type CheckBox}">
                            <StackPanel Orientation="Horizontal">
                                <Canvas Height="{TemplateBinding Height}" Width="{TemplateBinding Height}">
                                    <Rectangle Fill="{TemplateBinding Background}" 
                                               Stroke="{TemplateBinding BorderBrush}"
                                               StrokeThickness="{Binding RelativeSource={RelativeSource TemplatedParent}, 
                                                                         Path=BorderThickness.Top}"
                                               Height="{TemplateBinding Height}" Width="{TemplateBinding Height}"
                                               Canvas.Top="0" Canvas.Left="0"/>
                                </Canvas>
                                <Frame Width="5"/>
                                <ContentPresenter HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" 
                                                  VerticalAlignment="{TemplateBinding VerticalContentAlignment}"/>
                            </StackPanel>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
                <Setter Property="Foreground" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="BorderBrush" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="BorderThickness" Value="3"/>
                <Setter Property="VerticalContentAlignment" Value="Top" />
                <Setter Property="Height" Value="16"/>
                <Setter Property="Background" Value="{StaticResource MainBackgroundBrush}"/>
                <Setter Property="Margin" Value="3"/>
                <Setter Property="FontWeight" Value="Normal"/>
                <Setter Property="FontSize" Value="12"/>
                <Style.Triggers>
                    <Trigger Property="IsChecked" Value="True">
                        <Setter Property="Background" Value="{StaticResource MainTextBrush}"/>
                    </Trigger>
                    <MultiTrigger>
                        <MultiTrigger.Conditions>
                            <Condition Property="IsMouseOver" Value="True"/>
                            <Condition Property="IsChecked" Value="False"/>
                        </MultiTrigger.Conditions>
                        <Setter Property="Background" Value="{StaticResource DarkTextBrush}"/>
                    </MultiTrigger>
                    <MultiTrigger>
                        <MultiTrigger.Conditions>
                            <Condition Property="IsMouseOver" Value="True"/>
                            <Condition Property="IsChecked" Value="True"/>
                        </MultiTrigger.Conditions>
                        <Setter Property="Background" Value="{StaticResource MainTextBrush}"/>
                        <Setter Property="BorderBrush" Value="{StaticResource DarkTextBrush}"/>
                    </MultiTrigger>
                </Style.Triggers>
            </Style>
            <Style x:Key="GroupBoxFlat" TargetType="{x:Type GroupBox}">
                <Setter Property="BorderThickness" Value="0"/>
                <Setter Property="Foreground" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="FontWeight" Value="Bold"/>
                <Setter Property="FontSize" Value="15"/>
            </Style>
            <Style x:Key="FlatFieldBox" TargetType="{x:Type Control}">
                <Setter Property="Background" Value="{StaticResource DarkTextBrush}"/>
                <Setter Property="BorderBrush" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="BorderThickness" Value="1" />
                <Setter Property="Foreground" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="VerticalContentAlignment" Value="Center"/>
                <Style.Triggers>
                    <Trigger Property="IsFocused" Value="True">
                        <Setter Property="Background" Value="{StaticResource MainTextBrush}"/>
                        <Setter Property="Foreground" Value="{StaticResource MainBackgroundBrush}"/>
                    </Trigger>
                </Style.Triggers>
            </Style>
            <Style x:Key="BaseLabel" TargetType="{x:Type Label}">
                <Setter Property="Foreground" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="BorderBrush" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="VerticalContentAlignment" Value="Top" />
                <Setter Property="Height" Value="16"/>
                <Setter Property="Background" Value="{StaticResource MainBackgroundBrush}"/>
                <Setter Property="Margin" Value="3"/>
                <Setter Property="Padding" Value="0"/>
                <Setter Property="FontWeight" Value="Normal"/>
                <Setter Property="FontSize" Value="12"/>
            </Style>
            <Style x:Key="ListLabel" TargetType="{x:Type Label}" BasedOn="{StaticResource BaseLabel}">
                <Setter Property="Margin" Value="4 6"/>
            </Style>
            <Style x:Key="ListItem" TargetType="{x:Type ListViewItem}">
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type ListViewItem}">
                            <Border Background="{TemplateBinding Background}" 
                                    BorderBrush="{TemplateBinding BorderBrush}" 
                                    BorderThickness="{TemplateBinding BorderThickness}"
                                    Padding="{TemplateBinding Padding}"
                                    Margin="{TemplateBinding Margin}">
                                <ContentControl Foreground="{TemplateBinding Foreground}">
                                    <ContentPresenter HorizontalAlignment="Stretch" Width="Auto"
                                             VerticalAlignment="{TemplateBinding VerticalContentAlignment}"
                                             TextBlock.Foreground="{TemplateBinding Foreground}"/>
                                </ContentControl>
                            </Border>
                            <ControlTemplate.Resources>
                                <Style TargetType="{x:Type Label}">
                                    <Setter Property="Foreground" Value="{Binding RelativeSource={
                                        RelativeSource AncestorType={x:Type ListViewItem}
                                        }, Path=Foreground}"/>
                                </Style>
                            </ControlTemplate.Resources>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
                <Setter Property="Background" Value="{StaticResource DarkBackgroundBrush}"/>
                <Setter Property="Foreground" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="BorderThickness" Value="1"/>
                <Setter Property="BorderBrush" Value="{StaticResource DarkBackgroundBrush}"/>
                <Setter Property="Margin" Value="4 1"/>
                <Setter Property="Padding" Value="0"/>
                <Style.Triggers>
                    <Trigger Property="IsMouseOver" Value="True">
                        <Setter Property="BorderBrush" Value="{StaticResource DarkTextBrush}"/>
                    </Trigger>
                    <Trigger Property="IsSelected" Value="True">
                        <Setter Property="BorderBrush" Value="{StaticResource MainTextBrush}"/>
                    </Trigger>
                </Style.Triggers>
            </Style>
            <Style x:Key="DefListView" TargetType="{x:Type ListView}">
                <Setter Property="Background" Value="{StaticResource DarkBackgroundBrush}"/>
                <Setter Property="HorizontalContentAlignment" Value="Stretch"/>
                <Setter Property="BorderThickness" Value="0"/>
            </Style>
            

            <!-- extra login window styles -->
            <Style x:Key="HeadLabel" TargetType="{x:Type Label}">
                <Setter Property="Margin" Value="5"/>
                <Setter Property="FontSize" Value="40"/>
                <Setter Property="FontWeight" Value="Bold"/>
                <Setter Property="Foreground" Value="{StaticResource MainTextBrush}"/>
            </Style>
            <Style x:Key="Horline" TargetType="{x:Type Separator}">
                <Setter Property="Margin" Value="10 0"/>
                <Setter Property="Height" Value="15" />
                <Setter Property="Background" Value="{StaticResource MainTextBrush}"/>
            </Style>
            <Style x:Key="FieldLabel" TargetType="{x:Type Label}">
                <Setter Property="Margin" Value="10 -2"/>
                <Setter Property="Foreground" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="FontWeight" Value="Light"/>
            </Style>
            <Style x:Key="FieldBox" TargetType="{x:Type Control}" BasedOn="{StaticResource FlatFieldBox}">
                <Setter Property="Margin" Value="15 5"/>
                <Setter Property="Height" Value="30"/>
            </Style>
            <Style x:Key="ExtraLink" TargetType="{x:Type Button}">
                <Setter Property="Template">
                    <Setter.Value>
                        <ControlTemplate TargetType="{x:Type Button}">
                            <ContentPresenter HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}" 
                                              VerticalAlignment="{TemplateBinding VerticalContentAlignment}"/>
                        </ControlTemplate>
                    </Setter.Value>
                </Setter>
                <Setter Property="Margin" Value="15 5"/>
                <Setter Property="VerticalAlignment" Value="Bottom" />
                <Setter Property="HorizontalAlignment" Value="Stretch" />
                <Setter Property="HorizontalContentAlignment" Value="Left" />
                <Setter Property="Foreground" Value="{StaticResource MainTextBrush}"/>
                <Setter Property="FontWeight" Value="Bold" />
                <Setter Property="FontSize" Value="14"/>
                <Style.Triggers>
                    <Trigger Property="IsMouseOver" Value="True">
                        <Setter Property="Cursor" Value="Hand"/>
                        <Setter Property="Foreground" Value="{StaticResource DarkTextBrush}"/>
                    </Trigger>
                </Style.Triggers>
            </Style>
            <Style x:Key="FailureLabel" TargetType="{x:Type Label}">
                <Setter Property="Margin" Value="10 5"/>
            </Style>
            <Style x:Key="EnterButton" TargetType="{x:Type Button}" BasedOn="{StaticResource FlatButton}">
                <Setter Property="Margin" Value="15 5"/>
                <Setter Property="Height" Value="50"/>
                <Setter Property="FontSize" Value="20" />
                <Setter Property="VerticalAlignment" Value="Bottom" />
            </Style>
        </ResourceDictionary>
    </Application.Resources>
</Application>
