﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using MusicStreamServer;
using NAudio.Wave;

namespace MusicStreamClient
{
    /// <summary>
    /// IValueCoverter to convert number of seconds to more human-friendly
    /// hh:mm:ss format.
    /// </summary>
    public class TimerLengthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return "";
            uint seconds = (uint)value;
            uint minutes = seconds / 60;
            uint hours = minutes / 60;
            seconds -= 60 * minutes;
            minutes -= 60 * hours;
            string upperString = hours > 0 ? (hours + ":" + minutes.ToString("00")) : (minutes.ToString());
            return upperString + ":" + seconds.ToString("00");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// IValueConverter that simply adds "album-" before numeric value.
    /// </summary>
    public class AlbumItemNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "album-" + value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    /// <summary>
    /// Data structure with INotifyPropertyChanged interface
    /// to keep music player progress bar lengths and 
    /// update them properly.
    /// </summary>
    public class ProgressDisplay : INotifyPropertyChanged
    {
        private uint fullTimeLength;
        private uint loadTimeLength;
        private uint playTimeLength;
        private uint barLength;

        public event PropertyChangedEventHandler PropertyChanged;

        //getters of lengths of all 3 bars
        public double FullPos => barLength;
        public double LoadPos => ((double)loadTimeLength / (double)fullTimeLength) * (double)barLength;
        public double PlayPos => ((double)playTimeLength / (double)fullTimeLength) * (double)barLength;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="fullTime">Full length of song in seconds</param>
        /// <param name="length">Full length of progress bar</param>
        public ProgressDisplay(uint fullTime, uint length)
        {
            fullTimeLength = fullTime;
            loadTimeLength = 0;
            playTimeLength = 0;
            barLength = length;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FullPos"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("LoadPos"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PlayPos"));
        }
        /// <summary>
        /// Resets loaded and played bar to 0.
        /// </summary>
        /// <param name="fullTime">New total length of song in seconds</param>
        public void ResetTotalTime(uint fullTime)
        {
            fullTimeLength = fullTime;
            loadTimeLength = 0;
            playTimeLength = 0;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("LoadPos"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PlayPos"));
        }
        /// <summary>
        /// Update length of loaded bar.
        /// </summary>
        /// <param name="load">Length of loaded part of song, in seconds</param>
        public void UpdateLoad(uint load)
        {
            loadTimeLength = load;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("LoadPos"));
        }
        /// <summary>
        /// Update length of played bar.
        /// </summary>
        /// <param name="play">Total length of played part of song, in seconds</param>
        public void UpdatePlay(uint play)
        {
            playTimeLength = play;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PlayPos"));
        }
        /// <summary>
        /// Change total length of progress bar.
        /// </summary>
        /// <param name="length">New full length of progress bar</param>
        public void Resize(uint length)
        {
            barLength = length;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FullPos"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("LoadPos"));
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("PlayPos"));
        }
    }

    /// <summary>
    /// Custom implementation of WaveProvider
    /// </summary>
    public class MyWaveProvider : IWaveProvider
    {
        public WaveFormat WaveFormat { get; private set; }
        private byte[] waveBuffer;
        private int played;
        private int mwpLoaded;
        const int BUFFER_LENGTH = (int)Song.BYTES_PER_SECTION * 10000;

        public MyWaveProvider(WaveFormat wf)
        {
            this.WaveFormat = wf;
            played = 0;
            mwpLoaded = 0;
        }

        public int SynchronizeBuffers(byte[] extBuffer, int loaded)
        {
            try
            {
                if (extBuffer != null)
                {
                    if (waveBuffer == null)
                        waveBuffer = new byte[extBuffer.Length];
                    //int startInd = (played >= extBuffer.Length) ? extBuffer.Length : played;
                    //int maxRead = Math.Min(BUFFER_LENGTH, extBuffer.Length - startInd);
                    int toLoad = loaded - mwpLoaded;
                    if (toLoad > 0)
                    {
                        Array.Copy(extBuffer, mwpLoaded, waveBuffer, mwpLoaded, toLoad);
                        mwpLoaded = loaded;
                    }
                    return played;
                }
                else
                {
                    played = 0;
                    mwpLoaded = 0;
                    //waveBuffer = new byte[BUFFER_LENGTH];
                    return 0;
                }
            }
            catch(Exception e) { throw e; }
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            try
            {
                if (waveBuffer != null)
                {
                    int unplayed = waveBuffer.Length - played;
                    int trueCount = (count > unplayed) ? unplayed : count;
                    lock (waveBuffer)
                        Array.Copy(waveBuffer, played, buffer, offset, trueCount);
                    played += trueCount;
                    return (trueCount < 1) ? 1 : trueCount;
                }
                else
                {
                    Array.Copy(new byte[count], 0, buffer, offset, count);
                    return count;
                }
            }
            catch(Exception e) { throw e; }
            //return count;
        }

        public int SetPosition(int nPos)
        {
            nPos = Math.Min(nPos, mwpLoaded);
            played = nPos;
            return played;
        }
    }
}
