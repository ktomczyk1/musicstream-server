﻿using MusicStreamServer;
using System;
using System.IO;
using System.Windows;

namespace MusicStreamClient
{
    /// <summary>
    /// Class of objects to compose images from data stream packets.
    /// Used for collecting album coverart data and saving it in image files.
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    class ImageFileComposer
    {
        private bool busy = false;
        private uint totalLength = 0;
        private uint readBytes = 0;
        private byte[] data;
        private uint id;
        public const string DEFAULT_COVERPATH = "../ImageResources/noCover.png";

        /// <summary>
        /// Event launched when coverart composing has been finalized.
        /// Should be handled by changing image on UI and sending request
        /// for next coverart if necessary.
        /// </summary>
        /// <see cref="CommunicateListener.coverartDownloadedEvent"/>,
        /// <seealso cref="IdRelatedEvent"/>
        public event IdRelatedEvent coverartDownloadedEvent;

        public uint Id => id;

        public ImageFileComposer() { }

        /// <summary>
        /// Tries to assign ID of album cover to compose and checks if its data
        /// can be handled.
        /// </summary>
        /// <param name="nid">ID of album</param>
        /// <returns>True if data can be handled (ID of that album is assigned),
        /// false if can't (other ID is already assigned and composer is busy).</returns>
        public bool AssignId(uint nid)
        {
            if(busy)
                return nid == id;
            else
            {
                id = nid;
                busy = true;
                return true;
            }
        }
        /// <summary>
        /// Handles the coverart stream packet received from CommunicateListener.
        /// <see cref="CommunicateListener"/>
        /// Composes the image and either finalizes or sends request for next data stream.
        /// </summary>
        /// <param name="msg">Packet</param>
        /// <param name="bytesRead">Amount of bytes read</param>
        public void ReadBytes(byte[] msg, uint totalMsgLength)
        {
            if (totalMsgLength < 12)
                throw new ArgumentException("Too short data.");
            if (msg[0] != (byte)COMM_PREFIX.COMMS_COVERART_STREAM || BitConverter.ToUInt32(msg, 1) != id)
                throw new ArgumentException("Improper msg stream.");

            if(totalLength == 0)
            {
                //initialising total length - first packet of coverart
                totalLength = BitConverter.ToUInt32(msg,5);
                data = new byte[totalLength];
                readBytes = 0;
            }
            uint offset = BitConverter.ToUInt32(msg, 9);
            if(offset > readBytes)
            {
                //if offset is bigger than read data chunk
                if (readBytes == totalLength)
                    FinalizeCollecting();
                else
                {
                    //some data has been skipped
                    App currApp = (App)Application.Current;
                    currApp.SendCoverartRequest(id, readBytes);
                }
            }
            else
            {
                //offset proper - can try to read data
                uint startOfUnknownByes = (readBytes - offset) + 13;
                if (startOfUnknownByes > totalMsgLength)
                    goto EndOfFunction; //no new data provided
                uint newUnknownBytes = Math.Min(totalMsgLength - startOfUnknownByes, totalLength-readBytes);
                Array.Copy(msg, startOfUnknownByes, data, readBytes, newUnknownBytes);
                readBytes += newUnknownBytes;
                //checking size of read data chunk
                if (readBytes == totalLength)
                    FinalizeCollecting();
                else
                {
                    App currApp = (App)Application.Current;
                    currApp.SendCoverartRequest(id, readBytes);
                }
            }
        EndOfFunction:;
        }

        /// <summary>
        /// Saves collected image data in file and resets the variables
        /// preparing the composer to handle next image.
        /// </summary>
        private void FinalizeCollecting()
        {
            try
            {
                string tPath = Path.GetTempPath();
                DirectoryInfo tDirInfo = new DirectoryInfo(tPath + "MusicStream");
                if (!tDirInfo.Exists)
                    tDirInfo.Create();
                string fileName = "tempCover" + id + ".png";
                string fullPath = tDirInfo.FullName + Path.DirectorySeparatorChar + fileName;
                FileStream cFile = File.Open(fullPath, FileMode.Create, FileAccess.Write);
                cFile.Write(data, 0, (int)totalLength);
                cFile.Close();
                coverartDownloadedEvent(id);
            }
            catch (Exception) { }

            data = null;
            totalLength = 0;
            readBytes = 0;
            id = 0;
            busy = false;
        }

        /// <summary>
        /// Path to coverart of selected album.
        /// </summary>
        /// <param name="selId">ID of album</param>
        /// <returns>Path to the coverart</returns>
        static public string GetAlbumPath(uint selId)
        {
            try
            {
                string tPath = Path.GetTempPath();
                string fileName = "tempCover" + selId + ".png";
                string fullPath = tPath + "MusicStream" + Path.DirectorySeparatorChar + fileName;
                if (File.Exists(fullPath))
                    return fullPath;
                else
                    return DEFAULT_COVERPATH;
            }
            catch (Exception) { return DEFAULT_COVERPATH; }
        }
        /// <summary>
        /// Checks if coverart of album is already downlaoded.
        /// </summary>
        /// <param name="selId">ID of album</param>
        /// <returns>True, if coverart is downloaded, false otherwise.</returns>
        static public bool IsAlreadyDownloaded(uint selId)
        { return GetAlbumPath(selId) != DEFAULT_COVERPATH; }

    }
}
