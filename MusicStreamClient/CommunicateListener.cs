﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using MusicStreamServer;
using System.Windows;
using System.Threading.Tasks;
using System.Threading;
using System.Text;
using MusicStreamClient.MainClientW;

namespace MusicStreamClient
{
    public delegate void SearchEvent(List<uint> ids, uint matches, uint skip);
    public delegate void AlbumInfoEvent(Album album, uint playlistId, float rating, uint trackCount);
    public delegate void SongInfoEvent(ListedSong song, uint albumId, float rating);
    public delegate void PlaylistSongsEvent(Playlist playlist);
    public delegate void PlaylistLoadListEvent(List<uint> playlists);
    public delegate void IdRelatedEvent(uint id);

    /// <summary>
    /// Class of objects to handle listening for communicates from server,
    /// created after every successfull loggin in.
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    public class CommunicateListener
    {
        private Socket listenedSocket;
        private bool open = true;
        private byte[] sDataBuffer = new byte[256];
        private byte[] lDataBuffer = new byte[32*1024];
        private uint lMessageAwaiting = 0;
        private int readByteLength = 0;

        ImageFileComposer imageComposer;

        /// <summary>
        /// Event launched when album search results are received.
        /// </summary>
        /// <seealso cref="SearchEvent"/>
        public event SearchEvent albumSearchResultEvent;
        /// <summary>
        /// Event launched when song search results are received.
        /// </summary>
        /// <seealso cref="SearchEvent"/>
        public event SearchEvent songSearchResultEvent;
        /// <summary>
        /// Event launched when album informations are received.
        /// </summary>
        /// <seealso cref="AlbumInfoEvent"/>
        public event AlbumInfoEvent albumInfoResultEvent;
        /// <summary>
        /// Event launched when song informations are received.
        /// </summary>
        /// <seealso cref="SongInfoEvent"/>
        public event SongInfoEvent songInfoResultEvent;
        /// <summary>
        /// Event launched when playlist informations (containing list of
        /// song IDs the playlist is composed of) are received.
        /// </summary>
        /// <seealso cref="PlaylistSongsEvent"/>
        public event PlaylistSongsEvent playlistSongListEvent;
        /// <summary>
        /// Event launched when list of playlist IDs belonging to
        /// user is received.
        /// </summary>
        /// <seealso cref="PlaylistLoadListEvent"/>
        public event PlaylistLoadListEvent playlistLoadListEvent;
        /// <summary>
        /// Event launched when playlist has been saved successfully.
        /// </summary>
        public event IdRelatedEvent playlistSaveSuccessEvent;
        /// <summary>
        /// Event launched when coverart composing has been finalized.
        /// Should be handled by changing image on UI and sending request
        /// for next coverart if necessary.
        /// </summary>
        /// <see cref="ImageFileComposer.coverartDownloadedEvent"/>,
        /// <seealso cref="IdRelatedEvent"/>
        public event IdRelatedEvent coverartDownloadedEvent;

        /// <summary>
        /// Constructor of CommunicateListener.
        /// </summary>
        /// <param name="sock">Socket to communicate with server</param>
        public CommunicateListener(Socket sock)
        {
            listenedSocket = sock;
            imageComposer = new ImageFileComposer();
            imageComposer.coverartDownloadedEvent += coverartDownloadedPropagate;
            Thread comm = new Thread(Listen);
            comm.Start();
        }
        /// <summary>
        /// Closes the CommunicateListener, should result in abort of communication handling loop
        /// </summary>
        internal void Close() { open = false; }

        /// <summary>
        /// Informs CommunicateListener that there will be long packet to receive
        /// </summary>
        public void WaitForLongMessage() { lMessageAwaiting++; }
        /// <summary>
        /// Event handler for ImageFileComposer internal event, propagates the event.
        /// </summary>
        /// <param name="id">ID of album which cover has been downloaded</param>
        public void coverartDownloadedPropagate(uint id)
        {
            Application.Current.Dispatcher.Invoke((Action)delegate {
                coverartDownloadedEvent(id);
            });
        }

        /// <summary>
        /// Initiates communication with server and 
        /// continues handling the messages until communication is aborted.
        /// </summary>
        private async void Listen()
        {
            bool isMessageLong = false;
            byte[] tempBuffer = new byte[32 * 1024];

            //communication handling loop
            while(open)
            {
                try
                {
                    //handling long data packets
                    if(lMessageAwaiting > 0)
                    {
                        readByteLength = listenedSocket.Receive(lDataBuffer);
                        if (open)
                        {
                            COMM_PREFIX prefix = (COMM_PREFIX)lDataBuffer[0];
                            if (prefix == COMM_PREFIX.COMMS_SONG_STREAM)
                            {
                                //song stream handling
                                isMessageLong = true;
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                                    if (mcw != null)
                                    {
                                        MCWSoundPlayer sp = mcw.SoundPlayerBar;
                                        Action<object> tAc = delegate (object ob)
                                         {
                                             try
                                             {
                                                 Tuple<byte[], int> tup = (Tuple<byte[], int>)ob;
                                                 sp.HandleSongStream(tup.Item1, tup.Item2);
                                             }
                                             catch (Exception) { }
                                         };
                                        Tuple<byte[], int> input = new Tuple<byte[], int>(new byte[lDataBuffer.Length], readByteLength);
                                        Array.Copy(lDataBuffer, input.Item1, lDataBuffer.Length);
                                        Task t = new Task(tAc, input);
                                        t.Start();
                                    }
                                });
                                lMessageAwaiting--;
                            }
                            else if (prefix == COMM_PREFIX.COMMS_COVERART_STREAM)
                            {
                                //coverart stream hanling
                                uint id = BitConverter.ToUInt32(lDataBuffer, 1);
                                if (!ImageFileComposer.IsAlreadyDownloaded(id))
                                {
                                    await Task.Run(() =>
                                    {
                                        try
                                        {
                                            if (imageComposer.AssignId(id) || imageComposer.Id == id)
                                                imageComposer.ReadBytes(lDataBuffer, (uint)readByteLength);
                                        }
                                        catch (Exception) { }
                                    });
                                }
                                isMessageLong = true;
                                lMessageAwaiting--;
                            }
                            else
                            {
                                //packet isn't long, passing it further
                                isMessageLong = false;
                                if (readByteLength == 256)
                                    Array.Copy(lDataBuffer, sDataBuffer, 256);
                                else
                                    sDataBuffer[0] = (byte)COMM_PREFIX.COMM_FAILURE;
                            }
                        }
                    }
                    else
                    {
                        isMessageLong = false;
                        readByteLength = listenedSocket.Receive(sDataBuffer);
                    }

                    //handling short data packets
                    if(!isMessageLong && open)
                    {
                        if (readByteLength != 256)
                            sDataBuffer[0] = (byte)COMM_PREFIX.COMM_FAILURE;
                        //throw new Exception("Received packet has wrong size");
                        COMM_PREFIX prefix = (COMM_PREFIX)sDataBuffer[0];
                        switch (prefix)
                        {
                            case COMM_PREFIX.COMMS_LOGOUT:
                                open = false;
                                Application.Current.Dispatcher.Invoke((Action)delegate {
                                    MainClientWindow currWind = (MainClientWindow)Application.Current.MainWindow;
                                    if (currWind != null)
                                        currWind.CallInformator("You have been logged out.");
                                });
                                break;
                            case COMM_PREFIX.COMMS_REACT_ISACTIVE:
                                try 
                                {
                                    App currApp = (App)Application.Current;
                                    currApp.ConfirmActivity();
                                }
                                catch (Exception) { }
                                break;
                            case COMM_PREFIX.COMMS_ALBUM_LIST:
                            case COMM_PREFIX.COMMS_SONG_LIST:
                                await Task.Run(() => {
                                    try { HandleSearchResult(sDataBuffer); }
                                    catch (Exception) { }
                                });
                                break;
                            case COMM_PREFIX.COMMS_ALBUM_INFO:
                            case COMM_PREFIX.COMMS_SONG_INFO:
                                await Task.Run(() => {
                                    try { HandleInfoRespond(sDataBuffer); }
                                    catch (Exception) { }
                                    });
                                break;
                            case COMM_PREFIX.COMMS_PLAYLIST_LIST:
                            case COMM_PREFIX.COMMS_PLAYLIST_SONGS:
                            case COMM_PREFIX.COMMS_PLAYLIST_RESP_SUCCESS:
                            case COMM_PREFIX.COMMS_PLAYLIST_RESP_FAILED:
                                await Task.Run(() => {
                                    try { HandlePlaylistRespond(sDataBuffer); }
                                    catch (Exception) { }
                                });
                                break;
                            default:
                                break;
                        }
                    }
                }
                catch (Exception) { }
            }
        }

        //Routines to handle some various types of packets
        //They all tage 256 byte long msg packet
        //Usually launch some events or display something on informator bar
        //Also, can throw ArgumentException if wrong packet type
        private void HandleSearchResult(byte[] msg)
        {
            if (msg[0] != (byte)COMM_PREFIX.COMMS_ALBUM_LIST && msg[0] != (byte)COMM_PREFIX.COMMS_SONG_LIST)
                throw new ArgumentException("Message not of COMMS_ALBUM_LIST or COMMS_SONG_LIST type.");

            //acquiring data
            uint startingIndex = BitConverter.ToUInt32(msg, 4);
            uint totalMatches = BitConverter.ToUInt32(msg, 8);
            byte sentMatches = msg[12];
            List<uint> idList = new List<uint> { };
            for (int i = 0; i < sentMatches; i++)
                idList.Add(BitConverter.ToUInt32(msg, 16 + i * 4));

            //propagating data
            if ((COMM_PREFIX)msg[0] == COMM_PREFIX.COMMS_ALBUM_LIST)
            {
                Application.Current.Dispatcher.Invoke((Action)delegate{ 
                    albumSearchResultEvent(idList, totalMatches, startingIndex);
                });
            }
            else
            {
                Application.Current.Dispatcher.Invoke((Action)delegate {
                    songSearchResultEvent(idList, totalMatches, startingIndex);
                });
            }
        }
        private void HandleInfoRespond(byte[] msg)
        {
            if (msg[0] != (byte)COMM_PREFIX.COMMS_ALBUM_INFO && msg[0] != (byte)COMM_PREFIX.COMMS_SONG_INFO)
                throw new ArgumentException("Message not of COMMS_ALBUM_INFO or COMMS_SONG_INFO type.");

            //acquiring data
            if(msg[1]==0)
            {
                uint id = BitConverter.ToUInt32(msg, 2);
                uint associatedId = BitConverter.ToUInt32(msg, 6);
                ushort totalFormalLength = BitConverter.ToUInt16(msg, 10);
                byte nameLength = (byte)Math.Min(msg[12], (byte)240);
                byte artistLength = (byte)Math.Min(msg[13], 240 - nameLength);
                byte trackNum = msg[14];
                float rating = (float)msg[15] / 32;
                byte fullLen = (byte)(artistLength + nameLength);
                string name = Encoding.UTF8.GetString(msg, 16, nameLength);
                string artist = Encoding.UTF8.GetString(msg, 16 + nameLength, artistLength);

                //propagating data
                if((COMM_PREFIX)msg[0] == COMM_PREFIX.COMMS_ALBUM_INFO)
                {
                    Album nAlbum = new Album(id, name, artist, "", new List<uint> { });
                    nAlbum.SetTotalFormalLength(totalFormalLength);
                    Application.Current.Dispatcher.Invoke((Action)delegate {
                        albumInfoResultEvent(nAlbum, associatedId, rating, trackNum);
                    });
                }
                else
                {
                    ListedSong nSong = new ListedSong(id, trackNum, name, artist, totalFormalLength);
                    Application.Current.Dispatcher.Invoke((Action)delegate {
                        songInfoResultEvent(nSong, associatedId, rating);
                    });
                }
            }
            else
            {
                //song or album doesn't exist
                Application.Current.Dispatcher.Invoke((Action)delegate {
                    MainClientWindow currWind = (MainClientWindow)Application.Current.MainWindow;
                    if (currWind != null)
                        currWind.CallDebugInformator("Tried to get info about non-existing song or album.");
                });
            }
        }
        private void HandlePlaylistRespond(byte[] msg)
        {


            switch((COMM_PREFIX)msg[0])
            {
                case COMM_PREFIX.COMMS_PLAYLIST_SONGS:
                    {
                        uint id = BitConverter.ToUInt32(msg, 1);
                        uint ownerId = BitConverter.ToUInt32(msg, 5);
                        byte tracks = Math.Min(msg[9], (byte)48);
                        ushort totalLength = BitConverter.ToUInt16(msg, 10);
                        string name = Encoding.UTF8.GetString(msg, 12, 52);
                        List<uint> songIDs = new List<uint> { };
                        for (int i = 0; i < tracks; i++)
                            songIDs.Add(BitConverter.ToUInt32(msg, 64 + 4 * i));
                        Playlist playlist = new Playlist(id, ownerId, totalLength, songIDs);
                        playlist.SetName(name);
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            playlistSongListEvent(playlist);
                        });
                        break;
                    }
                case COMM_PREFIX.COMMS_PLAYLIST_LIST:
                    {
                        uint id = BitConverter.ToUInt32(msg, 1);
                        byte amount = Math.Min((byte)60, msg[5]);
                        List<uint> songIDs = new List<uint> { };
                        for (int i = 0; i < amount; i++)
                            songIDs.Add(BitConverter.ToUInt32(msg, 16 + i * 4));
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            playlistLoadListEvent(songIDs);
                        });
                        break;
                    }
                case COMM_PREFIX.COMMS_PLAYLIST_RESP_SUCCESS:
                case COMM_PREFIX.COMMS_PLAYLIST_RESP_FAILED:
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate {
                            MainClientWindow currWind = (MainClientWindow)Application.Current.MainWindow;
                            if (currWind != null)
                            {
                                if ((COMM_PREFIX)msg[0] == COMM_PREFIX.COMMS_PLAYLIST_RESP_SUCCESS)
                                {
                                    currWind.CallInformator("Successfully saved the playlist.");
                                    uint id = BitConverter.ToUInt32(msg, 2);
                                    playlistSaveSuccessEvent(id);
                                }
                                else
                                    currWind.CallInformator("Failed to save the playlist.");
                            }
                        });
                        break;
                    }
            }
        } 
    }
}
