﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace MusicStreamClient.MainClientW
{
    /// <summary>
    /// Page displayed as tab of MainClientWindow.
    /// Contains search form and frame displaying either SongList or AlbumGrid page.
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    public partial class MCWContentLibrary : Page
    {
        /// <summary>
        /// Data structure for buttons in paginator.
        /// </summary>
        private struct PaginatorField
        {
            public string PagText { get; set; } //text on button
            public bool IsNum { get; set; } //is button numeric (dots "..." aren't)
            public bool IsActive { get; set; } //is button active (selected page)
            public PaginatorField(string txt, bool isnum, bool isact)
            { PagText = txt; IsNum = isnum; IsActive = isact; }
        }

        private bool awaitingSearchResult = false;

        //pages to display in central frame
        private MCWAlbumGrid albumGridView;
        private MCWSongList songListView;

        //variables to keep last search parameters
        private string lastSearchQuery = "";
        private byte lastSearchPlaces = 7;
        private byte lastSearchOrder = 0;
        private bool lastSearchIsAlbum = true;
        private bool initialSearchDone = false;

        public MCWContentLibrary()
        {
            InitializeComponent();

            //setting the localized names
            LibraryAlbums.Content = globNames.contentSearchCatAlbums;
            LibrarySongs.Content = globNames.contentSearchCatSongs;
            ShowSearch.Content = globNames.contentSearchShowSearch;
            ShowAll.Content = globNames.contentSearchShowAll;
            PlacesGroupbox.Header = globNames.contentSearchPlacesHeader;
            PlacesSongNames.Content = globNames.contentSearchPlacesSongs;
            PlacesAlbumNames.Content = globNames.contentSearchPlacesAlbums;
            PlacesArtists.Content = globNames.contentSearchPlacesArtist;
            SortGroupbox.Header = globNames.contentSearchSortHeader;
            SortNameAZ.Content = globNames.contentSearchSortName + " A-Z";
            SortNameZA.Content = globNames.contentSearchSortName + " Z-A";
            SortArtistAZ.Content = globNames.contentSearchSortArtist + " A-Z";
            SortArtistZA.Content = globNames.contentSearchSortArtist + " Z-A";
            SortLengthAsc.Content = globNames.contentSearchSortLength + " " + globNames.contentSearchSortAscending;
            SortLengthDes.Content = globNames.contentSearchSortLength + " " + globNames.contentSearchSortDescending;
            //SortRating.Content = globNames.contentSearchSortRating;

            //initializing things
            App currApp = (App)Application.Current;
            currApp.GetCommunicateListener.albumSearchResultEvent += DisplayNewAlbumGrid;
            currApp.GetCommunicateListener.songSearchResultEvent += DisplayNewSongList;
            songListView = new MCWSongList();
            albumGridView = new MCWAlbumGrid();
            //initializing first default search when frame loads
            LibraryDisplayframe.Loaded += delegate {
                if (!initialSearchDone)
                { ShowSearch_Page(1); initialSearchDone = true; }
            };
        }

        /// <summary>
        /// Event handler for "Show all" button click.
        /// Checks the input on side panel and sends search request.
        /// </summary>
        private void ShowAll_Click(object sender, RoutedEventArgs e)
        {
            byte sortSelect = 0, sort = 0;
            bool catAlbum = (LibraryAlbums.IsChecked ?? !(LibrarySongs.IsChecked ?? false));

            //sort order checking
            if (SortNameAZ.IsChecked ?? false)
                sortSelect += 1;
            if (SortNameZA.IsChecked ?? false)
                sortSelect += 2;
            if (SortArtistAZ.IsChecked ?? false)
                sortSelect += 4;
            if (SortArtistZA.IsChecked ?? false)
                sortSelect += 8;
            if (SortLengthAsc.IsChecked ?? false)
                sortSelect += 16;
            if (SortLengthDes.IsChecked ?? false)
                sortSelect += 32;
            //if (SortRating.IsChecked ?? false)
            //    sortSelect += 64;
            switch (sortSelect)
            {
                case 1: sort = 0; break;
                case 2: sort = 8; break;
                case 4: sort = 1; break;
                case 8: sort = 9; break;
                case 16: sort = 2; break;
                case 32: sort = 10; break;
                //case 64: sort = 3; break;
                default: sort = 16; break; //somehow - incorrect value of sortSelect
            }

            //trying to send a request
            if (sort != 16)
            {
                App currApp = (App)Application.Current;
                currApp.SendSearchRequest("", 7, sort, 0, catAlbum);
                awaitingSearchResult = true;
                lastSearchQuery = "";
                lastSearchOrder = sort;
                lastSearchPlaces = 7;
                lastSearchIsAlbum = catAlbum;
            }
            else
            {
                MainClientWindow currWind = (MainClientWindow)Application.Current.MainWindow;
                currWind.CallInformator("I don't know how but you selected more than one sorting order");
            }
        }
        /// <summary>
        /// Event handler for "Search" button click.
        /// Checks the input on side panel and sends search request.
        /// </summary>
        private void ShowSearch_Click(object sender, RoutedEventArgs e)
        {
            byte places = 0, sortSelect = 0, sort = 0;
            bool catAlbum = (LibraryAlbums.IsChecked ?? !(LibrarySongs.IsChecked ?? false));

            //chechking places to search
            if (PlacesSongNames.IsChecked ?? false)
                places += 1;
            if (PlacesAlbumNames.IsChecked ?? false)
                places += 2;
            if (PlacesArtists.IsChecked ?? false)
                places += 4;

            //checking sort order
            if (SortNameAZ.IsChecked ?? false)
                sortSelect += 1;
            if (SortNameZA.IsChecked ?? false)
                sortSelect += 2;
            if (SortArtistAZ.IsChecked ?? false)
                sortSelect += 4;
            if (SortArtistZA.IsChecked ?? false)
                sortSelect += 8;
            if (SortLengthAsc.IsChecked ?? false)
                sortSelect += 16;
            if (SortLengthDes.IsChecked ?? false)
                sortSelect += 32;
            //if (SortRating.IsChecked ?? false)
            //    sortSelect += 64;
            switch (sortSelect)
            {
                case 1: sort = 0; break;
                case 2: sort = 8; break;
                case 4: sort = 1; break;
                case 8: sort = 9; break;
                case 16: sort = 2; break;
                case 32: sort = 10; break;
                //case 64: sort = 3; break;
                default: sort = 16; break; //somehow - incorrect value of sortSelect
            }

            //trying to send search request
            if (places != 0 && sort != 16)
            {
                string query = SearchQuery.Text;
                App currApp = (App)Application.Current;
                currApp.SendSearchRequest(query, places, sort, 0, catAlbum);
                awaitingSearchResult = true;

                lastSearchQuery = query;
                lastSearchOrder = sort;
                lastSearchPlaces = places;
                lastSearchIsAlbum = catAlbum;
            }
            else
            {
                MainClientWindow currWind = (MainClientWindow)Application.Current.MainWindow;
                if (currWind != null)
                {
                    if (places == 0)
                        currWind.CallInformator("You must select at least 1 place to search");
                    else
                        currWind.CallInformator("I don't know how but you selected more than one sorting order");
                }
            }
        }
        /// <summary>
        /// Sends search request with criteria as previous search but changed
        /// skip resulting from selected page (and window size).
        /// Launched by paginator button handler <see cref="PaginButton_Click(object, RoutedEventArgs)"/>
        /// </summary>
        /// <param name="newPage">Selected page</param>
        private void ShowSearch_Page(uint newPage)
        {
            int maxFit;
            double actHeight = LibraryDisplayframe.ActualHeight;
            double actWidth = LibraryDisplayframe.ActualWidth;
            if (lastSearchIsAlbum)
                maxFit = albumGridView.GetMaxFittingAmount(actHeight, actWidth);
            else
                maxFit = songListView.GetMaxFittingAmount(actHeight);
            uint skip = (uint) Math.Max(maxFit * (newPage - 1), 0);

            //trying to send search request
            if (lastSearchPlaces != 0 && lastSearchOrder != 16)
            {
                App currApp = (App)Application.Current;
                currApp.SendSearchRequest(lastSearchQuery, lastSearchPlaces, lastSearchOrder,
                    skip, lastSearchIsAlbum);
                awaitingSearchResult = true;
            }
        }

        /// <summary>
        /// Event handler that runs when album list search result is received.
        /// Loads collected data on AlbumGrid page, displays it and updates paginator.
        /// </summary>
        /// <param name="ids">IDs of found albums</param>
        /// <param name="matches">Total amount of results matching criteria in server library</param>
        /// <param name="skip">Amount of results from beginning skipped in returned list</param>
        public void DisplayNewAlbumGrid(List<uint> ids, uint matches, uint skip)
        {
            if (awaitingSearchResult)
            {
                songListView.AbortMissingInfoCheck();
                int maxFit = albumGridView.GetMaxFittingAmount(LibraryDisplayframe.ActualHeight,
                    LibraryDisplayframe.ActualWidth);
                ids = ids.Take(maxFit).ToList();
                albumGridView.LoadNewList(ids);

                int pages = (int)Math.Ceiling((float)matches / (float)maxFit);
                int currPag = (int)Math.Floor((float)skip / (float)maxFit) + 1;
                SetPaginator(pages, currPag);

                LibraryDisplayframe.Navigate(albumGridView);
                awaitingSearchResult = false;
            }
        }
        /// <summary>
        /// Event handler that runs when song list search result is received.
        /// Loads collected data on SongList page, displays it and updates paginator.
        /// </summary>
        /// <param name="ids">IDs of found songs</param>
        /// <param name="matches">Total amount of results matching criteria in server library</param>
        /// <param name="skip">Amount of results from beginning skipped in returned list</param>
        public void DisplayNewSongList(List<uint> ids, uint matches, uint skip)
        {
            if (awaitingSearchResult)
            {
                albumGridView.AbortMissingInfoCheck();
                int maxFit = songListView.GetMaxFittingAmount(LibraryDisplayframe.ActualHeight);
                ids = ids.Take(maxFit).ToList();
                songListView.LoadNewList(ids);

                int pages = (int)Math.Ceiling((float)matches / (float)maxFit);
                int currPag = (int)Math.Floor((float)skip / (float)maxFit) + 1;
                SetPaginator(pages, currPag);

                LibraryDisplayframe.Navigate(songListView);
                awaitingSearchResult = false;
            }
        }

        /// <summary>
        /// Resets and sets the content of paginator by updating data structures.
        /// </summary>
        /// <param name="totalAmount">Total amount of pages</param>
        /// <param name="selectedPage">Page that is selected (or null if none is)</param>
        private void SetPaginator(int totalAmount, int? selectedPage)
        {
            List<PaginatorField> paginatorList = new List<PaginatorField> { };
            bool skipping = false;
            int countDist = 8; //how many pages far from active to draw
            for(int i = 1; i<= totalAmount; i++)
            {
                if (i==1 || i==totalAmount || Math.Abs(i - (selectedPage??-countDist))<=countDist )
                {
                    if (i == selectedPage)
                        paginatorList.Add(new PaginatorField(i.ToString(), true, true));
                    else
                        paginatorList.Add(new PaginatorField(i.ToString(), true, false));
                    skipping = false;
                }
                else
                {
                    if(!skipping)
                    {
                        paginatorList.Add(new PaginatorField("...", false, false));
                        skipping = true;
                    }
                }
            }
            PaginatorControl.ItemsSource = paginatorList;
        }

        /// <summary>
        /// Event handler for clicking paginator buttons.
        /// Runs function that sends request to get content of selected page.
        /// <see cref="ShowSearch_Page(uint)"/>
        /// </summary>
        private void PaginButton_Click(object sender, RoutedEventArgs e)
        {
            Button pagButton = (Button)sender;
            if(pagButton!=null)
            {
                try
                {
                    uint nPage = UInt16.Parse(pagButton.Content.ToString());
                    ShowSearch_Page(nPage);
                }
                catch (Exception) { }
            }
        }
    }
}
