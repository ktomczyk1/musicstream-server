﻿using MusicStreamServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MusicStreamClient.MainClientW
{
    /// <summary>
    /// Page displayed on content library page,
    /// contains list of loaded songs.
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    public partial class MCWSongList : Page
    {
        private List<ListedSong> listOfSongs;
        private List<uint> listOfAlbumIds;
        private Dictionary<uint, bool> fullInfo; //"is song info collected" dictionary, key is song ID

        private bool missingInfo = false;
        Task missingInfoTask;

        //height of single song list element
        private double listElementHeight = 34;

        /// <summary>
        /// Default constructor, loads empty list of songs
        /// </summary>
        public MCWSongList()
        {
            InitializeComponent();
            missingInfoTask = new Task(delegate { });
            missingInfoTask.Start();

            //assigning event handlers
            App currApp = (App)Application.Current;
            currApp.GetCommunicateListener.songInfoResultEvent +=
                delegate (ListedSong song, uint albumId, float rating) { UpdateSong(song, albumId); };

            //initialising lists with empty containers
            listOfSongs = new List<ListedSong> { };
            listOfAlbumIds = new List<uint> { };
            fullInfo = new Dictionary<uint, bool> { };
            SongListView.ItemsSource = listOfSongs;
        }
        /// <summary>
        /// Constructor with argument, loads songs in passed list.
        /// </summary>
        /// <param name="idList">List of song IDs to load</param>
        public MCWSongList(List<uint> idList)
        {
            InitializeComponent();
            missingInfoTask = new Task(delegate { });
            missingInfoTask.Start();

            //assigning event handlers
            App currApp = (App)Application.Current;
            currApp.GetCommunicateListener.songInfoResultEvent +=
                delegate (ListedSong song, uint albumId, float rating) { UpdateSong(song,albumId); };

            //loading list passed in argument
            LoadNewList(idList);
        }
        /// <summary>
        /// Constructor with argument, loads songs in passed list.
        /// </summary>
        /// <param name="initList">List of songs to load</param>
        public MCWSongList(List<ListedSong> initList)
        {
            InitializeComponent();

            //loading list passed in argument
            LoadNewList(initList);
        }

        /// <summary>
        /// Resets the page, loads new list of songs and starts routines for collecting missing info
        /// about them.
        /// </summary>
        /// <param name="idList">List of song IDs of songs to display on page</param>
        public void LoadNewList(List<uint> idList)
        {
            //initialising
            listOfSongs = new List<ListedSong> { };
            listOfAlbumIds = new List<uint> { };
            fullInfo = new Dictionary<uint, bool> { };
            for (int i = 0; i < idList.Count; i++)      //iterating through song IDs
            {
                if (!listOfSongs.Exists(x => x.Id == idList[i]))
                {
                    listOfSongs.Add(new ListedSong(idList[i], (uint)i + 1, "", "", 0));
                    listOfAlbumIds.Add(0);
                    if (!fullInfo.ContainsKey(idList[i]))
                        fullInfo.Add(idList[i], false);
                }
            }
            SongListView.ItemsSource = listOfSongs;

            //starting routines for collecting missing informations
            missingInfo = true;
            CheckForMissingInfo();
            if (missingInfoTask.IsCompleted)
            {
                missingInfoTask = new Task(async delegate
                {
                    while (missingInfo)
                    {
                        await Task.Delay(App.MISSING_INFO_CHECK_DELAY);
                        CheckForMissingInfo();
                    }
                });
                missingInfoTask.Start();
            }
        }
        /// <summary>
        /// Resets the page and loads new list of songs.
        /// </summary>
        /// <param name="initList">List of listed song objects to display on page</param>
        public void LoadNewList(List<ListedSong> initList)
        {
            //initialising
            listOfSongs = initList;
            fullInfo = new Dictionary<uint, bool> { };
            foreach (ListedSong s in listOfSongs)//iterating through songs
            {
                //adding to dictionary information that song info is already collected
                if (!fullInfo.ContainsKey(s.Id))
                    fullInfo.Add(s.Id, true);
            }
            SongListView.ItemsSource = listOfSongs;

            missingInfo = false;
        }

        /// <summary>
        /// Event handler that runs when new song info is received. 
        /// Updates the information about that song if it's visible on page
        /// and checks for missing info.
        /// </summary>
        /// <param name="song">ListedSong object constructed from received info</param>
        /// <param name="albumId">ID of album associated with that song</param>
        public void UpdateSong(ListedSong song, uint albumId)
        {
            int index = listOfSongs.FindIndex(x => x.Id == song.Id);
            if (index != -1)
            {
                listOfSongs[index] = song;
                listOfAlbumIds[index] = albumId;
                fullInfo[song.Id] = true;
                CheckForMissingInfo();
            }
        }
        /// <summary>
        /// Checks if there is song ID on page iformation about are unknown.
        /// If yes, sends request for that information.
        /// </summary>
        private void CheckForMissingInfo()
        {
            try
            {
                if (fullInfo.ContainsValue(false))
                {
                    uint id = fullInfo.First(x => x.Value == false).Key;
                    App currApp = (App)Application.Current;
                    currApp.SendInfoRequest(id, false);
                }
                else
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        SongListView.Items.Refresh();
                    });
                    missingInfo = false;
                }
            }
            catch (Exception) { }
        }
        /// <summary>
        /// Aborts any further checking for missing info.
        /// </summary>
        public void AbortMissingInfoCheck() { missingInfo = false; }

        /// <summary>
        /// Event handler for double clicking on songs on page.
        /// Adds selected song to the playlist
        /// </summary>
        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(sender!= null)
            {
                ListedSong song = listOfSongs[SongListView.SelectedIndex];
                uint albId = listOfAlbumIds[SongListView.SelectedIndex];
                
                MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                if (mcw != null)
                    mcw.PlaylistView.AddSong(song, albId);
            }
        }

        /// <summary>
        /// Calculate maximal amount of songs that will fit on page
        /// </summary>
        /// <param name="containerHeight">Height of the page container</param>
        /// <returns>The amount of songs</returns>
        public int GetMaxFittingAmount(double containerHeight)
        { return (int)Math.Floor(containerHeight / listElementHeight); }
    }
}
