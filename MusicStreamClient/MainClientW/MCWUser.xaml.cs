﻿using MusicStreamServer;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MusicStreamClient.MainClientW
{
    /// <summary>
    /// Page displayed as tab of MainClientWindow.
    /// Contains user-related settings (currently only logout button)
    /// and list of saved playlists.
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    public partial class MCWUser : Page
    {
        List<Playlist> playlistsOfUser;
        Dictionary<uint, bool> fullInfo; //"is playlist info collected" dictionary, key is playlist ID

        /// <summary>
        /// Default and only constructor.
        /// </summary>
        /// <param name="userId">ID of user logged in</param>
        public MCWUser(uint userId)
        {
            InitializeComponent();

            UserLogoutButton.Content = globNames.loginLogout;

            //initialising empty lists
            playlistsOfUser = new List<Playlist> { };
            fullInfo = new Dictionary<uint, bool> { };
            PlaylistList.ItemsSource = playlistsOfUser;

            //assinging event handlers and sending request for playlist ID list
            App currApp = (App)Application.Current;
            currApp.GetCommunicateListener.playlistLoadListEvent += LoadPlaylistIDs;
            currApp.GetCommunicateListener.playlistSongListEvent += UpdatePlaylist;
            currApp.GetCommunicateListener.playlistSaveSuccessEvent += AddPlaylist;
            currApp.SendPlaylistRequest();
        }

        /// <summary>
        /// Event handler that runs when list of playlists belonging to user is received.
        /// </summary>
        /// <param name="ids">List of playlist IDs of playlists belonging to user.</param>
        public void LoadPlaylistIDs(List<uint> ids)
        {
            foreach (uint id in ids)
                fullInfo.Add(id, false);
            CheckForMissingInfo();
        }
        /// <summary>
        /// Event handler that runs when playlist is saved sucessfully.
        /// </summary>
        /// <param name="id">ID of save playlist</param>
        public void AddPlaylist(uint id)
        {
            if (!fullInfo.ContainsKey(id))
                fullInfo.Add(id, false);
            CheckForMissingInfo();
        }
        /// <summary>
        /// Event handler that runs when new playlist info is received.
        /// </summary>
        /// <param name="pl">Playlist structure constructed from collected info</param>
        public void UpdatePlaylist(Playlist pl)
        {
            if(fullInfo.ContainsKey(pl.Id))
            {
                if (fullInfo[pl.Id] == false)
                {
                    fullInfo[pl.Id] = true;
                    playlistsOfUser.Add(pl);
                    CheckForMissingInfo();
                }
            }
        }
        /// <summary>
        /// Checks if there is playlist ID on page iformation about are unknown.
        /// If yes, sends request for that information.
        /// </summary>
        private void CheckForMissingInfo()
        {
            if (fullInfo.ContainsValue(false))
            {
                uint id = fullInfo.First(x => x.Value == false).Key;
                App currApp = (App)Application.Current;
                currApp.SendPlaylistRequest(id);
            }
            else
                PlaylistList.Items.Refresh();
        }

        /// <summary>
        /// Event handler for double clicking on playlists in list.
        /// Loads the selected playlist.
        /// </summary>
        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Playlist selectedPlaylist = playlistsOfUser[PlaylistList.SelectedIndex];
            MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
            mcw.PlaylistView.ClearPlaylist();
            mcw.PlaylistView.AddManySongs(selectedPlaylist.Songs);
        }

        /// <summary>
        /// Event handler for "Logout" button click.
        /// Logs out the user, closes main window and opens new login window.
        /// </summary>
        private void UserLogoutButton_Click(object sender, RoutedEventArgs e)
        {
            App currApp = (App)Application.Current;
            if (currApp != null)
            {
                currApp.Logout();
                MainClientWindow clientWind = (MainClientWindow)currApp.MainWindow;
                currApp.MainWindow = new LoginWindow();
                currApp.MainWindow.Show();
                if (clientWind != null)
                    clientWind.Close();
            }
        }

    }
}
