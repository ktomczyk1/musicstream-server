﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using MusicStreamServer;

namespace MusicStreamClient.MainClientW
{
    /// <summary>
    /// Page displayed as tab of MainClientWindow.
    /// Contains informations about selected album
    /// and list of song in it.
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    public partial class MCWAlbumInfo : Page
    {
        private Album selectedAlbum;
        private Playlist playlist; //playlist associated with the album
        private List<ListedSong> listOfSongs;
        private Dictionary<uint, bool> fullInfo; //"is song info collected" dictionary, key is song ID

        /// <summary>
        /// Constructor without arguments. Loads empty album.
        /// </summary>
        public MCWAlbumInfo()
        {
            InitializeComponent();

            //localisation
            PlayAsListButton.Content = globNames.albumInfoWPlayAsList;
            AddToPlaylistButton.Content = globNames.albumInfoWAddToList;

            //initialising lists with empty containers
            selectedAlbum = new Album(0, "", "", "../ImageResources/noCover.png", null);
            AlbumInfoSide.DataContext = selectedAlbum;
            listOfSongs = new List<ListedSong> { };
            playlist = new Playlist(0, 0, 0, null);
            fullInfo = new Dictionary<uint, bool> { };
            AlbumList.ItemsSource = listOfSongs;

            //assigning event handlers
            App currApp = (App)Application.Current;
            currApp.GetCommunicateListener.songInfoResultEvent +=
                delegate (ListedSong song, uint albumId, float rating) { UpdateSong(song); };
            currApp.GetCommunicateListener.playlistSongListEvent += UpdatePlaylist;
            currApp.GetCommunicateListener.coverartDownloadedEvent += UpdateCoverart;
        }
        /// <summary>
        /// Constructor with arguments. Loads given album.
        /// </summary>
        /// <param name="infAlbum">Album to load</param>
        /// <param name="playlistId">ID of playlist associated with album</param>
        public MCWAlbumInfo(Album infAlbum, uint playlistId)
        {
            InitializeComponent();

            //localisation
            PlayAsListButton.Content = globNames.albumInfoWPlayAsList;
            AddToPlaylistButton.Content = globNames.albumInfoWAddToList;

            //loading the album
            selectedAlbum = infAlbum;
            AlbumInfoSide.DataContext = selectedAlbum;
            listOfSongs = new List<ListedSong> { };
            playlist = new Playlist(playlistId, 0, 0, null);
            fullInfo = new Dictionary<uint, bool> { };
            AlbumList.ItemsSource = listOfSongs;

            //assigning event handlers
            App currApp = (App)Application.Current;
            currApp.GetCommunicateListener.songInfoResultEvent +=
                delegate (ListedSong song, uint albumId, float rating) { UpdateSong(song); };
            currApp.GetCommunicateListener.playlistSongListEvent += UpdatePlaylist;
            currApp.GetCommunicateListener.coverartDownloadedEvent += UpdateCoverart;
            currApp.SendPlaylistRequest(playlistId);
            if(!ImageFileComposer.IsAlreadyDownloaded(selectedAlbum.Id))
                currApp.SendCoverartRequest(selectedAlbum.Id, 0);
        }
        /// <summary>
        /// Loads new album.
        /// </summary>
        /// <param name="infAlbum">Album object to load</param>
        /// <param name="playlistId">ID of playlist associated with album</param>
        public void LoadNewAlbum(Album infAlbum, uint playlistId)
        {
            selectedAlbum = infAlbum;
            AlbumInfoSide.DataContext = selectedAlbum;
            listOfSongs = new List<ListedSong> { };
            playlist = new Playlist(playlistId, 0, 0, null);
            fullInfo = new Dictionary<uint, bool> { };
            AlbumList.ItemsSource = listOfSongs;

            //sending playlist request and checking the coverart
            App currApp = (App)Application.Current;
            currApp.SendPlaylistRequest(playlistId);
            if (!ImageFileComposer.IsAlreadyDownloaded(selectedAlbum.Id))
                currApp.SendCoverartRequest(selectedAlbum.Id, 0);
            else
                UpdateCoverart(selectedAlbum.Id);
        }

        /// <summary>
        /// Event handler that runs when new song info is received. 
        /// Updates the information about that song if it belongs to album
        /// and checks for missing info.
        /// </summary>
        /// <param name="song">ListedSong object constructed from received info</param>
        public void UpdateSong(ListedSong song)
        {
            int index = listOfSongs.FindIndex(x => x.Id == song.Id);
            if(index != -1)
            {
                listOfSongs[index] = song;
                fullInfo[song.Id] = true;
                CheckForMissingInfo();
            }
        }
        /// <summary>
        /// Event handler that runs when new playlist info is collected.
        /// If playlist ID matches the ID of playlist associated with the loaded album
        /// loads the list of songs from playlist to internal list and checks for
        /// missing info about songs.
        /// </summary>
        /// <param name="newPl">Playlist structure composed from collected info.</param>
        public void UpdatePlaylist(Playlist newPl)
        {
            if (newPl.Id == playlist.Id)
            {
                playlist = newPl;
                listOfSongs = new List<ListedSong> { };
                fullInfo = new Dictionary<uint, bool> { };
                for (int i = 0; i < newPl.Songs.Count; i++)   //iterating through songs in playlist
                {
                    if (!listOfSongs.Exists(x => x.Id == newPl.Songs[i]))
                    {
                        listOfSongs.Add(new ListedSong(newPl.Songs[i], (uint)i + 1, "", "", 0));
                        if(!fullInfo.ContainsKey(newPl.Songs[i]))
                            fullInfo.Add(newPl.Songs[i], false);
                    }
                }
                AlbumList.ItemsSource = listOfSongs;
                CheckForMissingInfo();
                AlbumList.Items.Refresh(); 
            }
        }
        /// <summary>
        /// Event handler that runs when new album is downloaded. 
        /// Updates the coverart if it's coverart of displayed album.
        /// </summary>
        /// <param name="id">ID of album which coverart has been downloaded</param>
        private void UpdateCoverart(uint id)
        {
            if(id == selectedAlbum.Id)
            {
                selectedAlbum.SetNewCoverart(ImageFileComposer.GetAlbumPath(selectedAlbum.Id));
                AlbumInfoSide.DataContext = selectedAlbum;
                AlbumCoverart.Source = new BitmapImage(new Uri(ImageFileComposer.GetAlbumPath(selectedAlbum.Id)));
            }
        }
        /// <summary>
        /// Checks if there is song ID on page iformation about are unknown.
        /// If yes, sends request for that information.
        /// </summary>
        private void CheckForMissingInfo()
        {
            try
            {
                if (fullInfo.ContainsValue(false))
                {
                    uint id = fullInfo.First(x => x.Value == false).Key;
                    App currApp = (App)Application.Current;
                    currApp.SendInfoRequest(id, false);
                }
                else
                    AlbumList.Items.Refresh();
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Event handler for double clicking the song in the list.
        /// Adds the selected song to the playlist.
        /// </summary>
        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem lvi = (ListViewItem)sender;
            if (sender != null)
            {
                ListedSong song = listOfSongs[AlbumList.SelectedIndex];

                MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                if (mcw != null)
                    mcw.PlaylistView.AddSong(song, selectedAlbum);
            }
        }
        /// <summary>
        /// Event handler for "Play as list" button click.
        /// Clears the current playlist and adds all the songs from album to the playlist.
        /// </summary>
        private void PlayAsListButton_Click(object sender, RoutedEventArgs e)
        {
            MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
            mcw.PlaylistView.ClearPlaylist();
            mcw.PlaylistView.AddManySongs(listOfSongs, selectedAlbum);
        }
        /// <summary>
        /// Event handler for "Add to playlist" button click.
        /// Adds all the songs from album to the playlist.
        /// </summary>
        private void AddToPlaylistButton_Click(object sender, RoutedEventArgs e)
        {
            MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
            mcw.PlaylistView.AddManySongs(listOfSongs, selectedAlbum);
        }
    }
}
