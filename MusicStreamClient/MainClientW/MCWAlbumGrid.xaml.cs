﻿using MusicStreamServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MusicStreamClient.MainClientW
{
    /// <summary>
    /// Page displayed on content library page,
    /// contains grid of loaded albums.
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    public partial class MCWAlbumGrid : Page
    {
        private List<Album> listOfAlbums;
        private Dictionary<uint,uint> associatedPlaylists;
        private Dictionary<uint, bool> fullInfo; //"is album info collected" dictionary, key is album ID

        private bool missingInfo = false;
        Task missingInfoTask;

        //dimenisions of single album grid element
        private double gridElementHeight = 220;
        private double gridElementWidth = 148;

        /// <summary>
        /// Default constructor, loads empty list of albums
        /// </summary>
        public MCWAlbumGrid()
        {
            InitializeComponent();
            missingInfoTask = new Task(delegate { });
            missingInfoTask.Start();

            //assigning event handlers
            App currApp = (App)Application.Current;
            currApp.GetCommunicateListener.albumInfoResultEvent +=
                delegate (Album album, uint playlist, float rating, uint trackCount) { UpdateAlbum(album, trackCount, playlist); };
            currApp.GetCommunicateListener.coverartDownloadedEvent += UpdateCoverart;

            //initialising lists with empty containers
            listOfAlbums = new List<Album> { };
            associatedPlaylists = new Dictionary<uint, uint> { };
            fullInfo = new Dictionary<uint, bool> { };
            AlbumListView.ItemsSource = listOfAlbums;
        }
        /// <summary>
        /// Constructor with argument, loads albums in passed list.
        /// </summary>
        /// <param name="idList">List of albums IDs to load</param>
        public MCWAlbumGrid(List<uint> idList)
        {
            InitializeComponent();
            missingInfoTask = new Task(delegate { });
            missingInfoTask.Start();

            //assigning event handlers
            App currApp = (App)Application.Current;
            currApp.GetCommunicateListener.albumInfoResultEvent +=
                delegate (Album album, uint playlist, float rating, uint trackCount) { UpdateAlbum(album,trackCount,playlist); };
            currApp.GetCommunicateListener.coverartDownloadedEvent += UpdateCoverart;
            
            //loading list passed in argument
            LoadNewList(idList);
        }

        /// <summary>
        /// Resets the page, loads new list of albums and starts routines for collecting missing info
        /// about them.
        /// </summary>
        /// <param name="idList">List of album IDs of albums to display on page</param>
        public void LoadNewList(List<uint> idList)
        {
            //initialising
            listOfAlbums = new List<Album> { };
            associatedPlaylists = new Dictionary<uint, uint> { };
            fullInfo = new Dictionary<uint, bool> { };
            for (int i = 0; i < idList.Count; i++)      //iterating through album IDs
            {
                if (!listOfAlbums.Exists(x => x.Id == idList[i]))
                {
                    string coverartPath = ImageFileComposer.GetAlbumPath(idList[i]);
                    listOfAlbums.Add(new Album(idList[i], "", "", coverartPath, new List<uint> { }));
                    if (!associatedPlaylists.ContainsKey(idList[i]))
                        associatedPlaylists.Add(idList[i], 0);
                    if (!fullInfo.ContainsKey(idList[i]))
                        fullInfo.Add(idList[i], false);
                }
            }
            AlbumListView.ItemsSource = listOfAlbums;

            //starting routines for collecting missing informations
            missingInfo = true;
            CheckForMissingInfo();
            if (missingInfoTask.IsCompleted)
            {
                missingInfoTask = new Task(async delegate
                {
                    while (missingInfo)
                    {
                        await Task.Delay(App.MISSING_INFO_CHECK_DELAY);
                        CheckForMissingInfo();
                    }
                });
                missingInfoTask.Start();
            }
        }

        /// <summary>
        /// Event handler that runs when new album info is received. 
        /// Updates the information about that album if it's visible on page
        /// and checks for missing info.
        /// </summary>
        /// <param name="album">Album object constructed from received info</param>
        /// <param name="trackCount">Amount of tracks in album</param>
        /// <param name="playlist">ID of playlist associated with that album</param>
        public void UpdateAlbum(Album album, uint trackCount, uint playlist)
        {
            int index = listOfAlbums.FindIndex(x => x.Id == album.Id);
            if(index != -1)
            {
                List<uint> fakeSongList = new List<uint> { };
                for (int i = 0; i < trackCount; i++)
                    fakeSongList.Add(0);
                if(!ImageFileComposer.IsAlreadyDownloaded(album.Id))
                {
                    App currApp = (App)Application.Current;
                    currApp.SendCoverartRequest(album.Id, 0);
                }
                string coverPath = ImageFileComposer.GetAlbumPath(album.Id);
                listOfAlbums[index] = new Album(album.Id, album.Name, album.Artist, coverPath, fakeSongList);
                listOfAlbums[index].SetTotalFormalLength(album.FormalLength);
                associatedPlaylists[album.Id] = playlist;
                fullInfo[album.Id] = true;
                CheckForMissingInfo();
            }
        }
        /// <summary>
        /// Event handler that runs when new album is downloaded. 
        /// Updates the coverart if its album is on the page and checks if there
        /// are any not-downloaded coverarts on the page left.
        /// </summary>
        /// <param name="id">ID of album which coverart has been downloaded</param>
        public void UpdateCoverart(uint id)
        {
            int index = listOfAlbums.FindIndex(x => x.Id == id);
            if(index != -1)
            {
                listOfAlbums[index].SetNewCoverart(ImageFileComposer.GetAlbumPath(id));
                AlbumListView.Items.Refresh();
            }

            //checking albums if there are any non-downloaded covers left
            foreach (Album a in listOfAlbums)
            {
                if (!ImageFileComposer.IsAlreadyDownloaded(a.Id))
                {
                    App currApp = (App)Application.Current;
                    currApp.SendCoverartRequest(a.Id, 0);
                    break;
                }
            }
        }
        /// <summary>
        /// Checks if there is album ID on page iformation about are unknown.
        /// If yes, sends request for that information.
        /// </summary>
        private void CheckForMissingInfo()
        {
            try
            {
                if (fullInfo.ContainsValue(false))
                {
                    uint id = fullInfo.First(x => x.Value == false).Key;
                    App currApp = (App)Application.Current;
                    currApp.SendInfoRequest(id, true);
                }
                else
                {
                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        AlbumListView.Items.Refresh();
                    });
                    missingInfo = false;
                }
            }
            catch (Exception) { }
        }
        /// <summary>
        /// Aborts any further checking for missing info.
        /// </summary>
        public void AbortMissingInfoCheck() { missingInfo = false; }

        /// <summary>
        /// Event handler for double clicking on albums on page.
        /// Navigates window to info about selected album
        /// <see cref="MainClientWindow.GoToAlbumInfoView(Album, uint)"/>
        /// </summary>
        private void ContentControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ContentControl cc = (ContentControl)sender;
            if(sender != null)
            {
                try
                {
                    uint id = UInt32.Parse(cc.Tag.ToString().Remove(0, 6));
                    Album al = listOfAlbums.Find(x => x.Id == id);
                    
                    MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                    if(mcw!=null)
                    {
                        if (fullInfo[id])
                            mcw.GoToAlbumInfoView(al, associatedPlaylists[al.Id]);
                    }
                }
                catch(Exception) { }
            }
        }

        /// <summary>
        /// Calculate maximal amount of albums that will fit on page
        /// </summary>
        /// <param name="containerHeight">Height of the page container</param>
        /// <param name="containerWidth">Width of the page container</param>
        /// <returns>The amount of albums</returns>
        public int GetMaxFittingAmount(double containerHeight, double containerWidth)
        {
            int fitX = (int)Math.Floor(containerWidth / gridElementWidth);
            int fitY = (int)Math.Floor(containerHeight / gridElementHeight);
            return fitX*fitY;
        }
    }
}
