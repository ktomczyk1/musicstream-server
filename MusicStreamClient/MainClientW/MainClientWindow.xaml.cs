﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MusicStreamClient.MainClientW;
using MusicStreamServer;

namespace MusicStreamClient
{
    /// <summary>
    /// Window displaying the actual application features
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    public partial class MainClientWindow : Window
    {
        private Button activeTabButton; //selected tab button
        private Queue<string> informatorQueue = new Queue<string> { };
        private bool informatorShown = false;

        //pages of application
        MCWContentLibrary contentLibraryView;
        MCWAlbumInfo albumInfoView;
        MCWPlaylist playlistView;
        MCWUser userView;
        MCWSoundPlayer soundPlayerBar;
        public MCWContentLibrary ContentLibraryView => contentLibraryView;
        public MCWAlbumInfo AlbumInfoView => albumInfoView;
        public MCWPlaylist PlaylistView => playlistView;
        public MCWSoundPlayer SoundPlayerBar => soundPlayerBar;

        public MainClientWindow()
        {
            InitializeComponent();

            //setting the localized names
            LibraryTabButton.Content = globNames.mainWControlsLibrary;
            AInfoTabButton.Content = globNames.mainWContorlsAlbumInfo;
            PlaylistTabButton.Content = globNames.mainWControlsPlaylist;

            //data initialisation
            App currApp = (App)Application.Current;
            activeTabButton = LibraryTabButton;
            if (currApp != null)
                UserTabButton.Content = currApp.Username;
            contentLibraryView = new MCWContentLibrary();
            albumInfoView = new MCWAlbumInfo();
            playlistView = new MCWPlaylist();
            userView = new MCWUser(0);
            soundPlayerBar = new MCWSoundPlayer();
            MiddleContentFrame.Navigate(contentLibraryView);
            PlayerFrame.Navigate(soundPlayerBar);
        }

        /// <summary>
        /// Event handler for tab buttons (in upper part of window) clicking
        /// </summary>
        private void TabButtonClick(object sender, RoutedEventArgs e)
        {
            Button clickedTab = (Button)sender;
            if(clickedTab!=null && clickedTab!=activeTabButton)
            {
                activeTabButton.Style = (Style)FindResource("UpperTabButton");
                clickedTab.Style = (Style)FindResource("UpperTabActiveButton");
                activeTabButton = clickedTab;
                if (clickedTab == LibraryTabButton)
                    MiddleContentFrame.Navigate(contentLibraryView);
                else if (clickedTab == AInfoTabButton)
                    MiddleContentFrame.Navigate(albumInfoView);
                else if (clickedTab == PlaylistTabButton)
                    MiddleContentFrame.Navigate(playlistView);
                else if (clickedTab == UserTabButton)
                    MiddleContentFrame.Navigate(userView);
            }
        }

        /// <summary>
        /// Navigates to the Album info page with new album loaded
        /// </summary>
        /// <param name="infAlbum">Album to load on album info page</param>
        /// <param name="playlistId">Playlist associated with album</param>
        public void GoToAlbumInfoView(Album infAlbum, uint playlistId)
        {
            albumInfoView.LoadNewAlbum(infAlbum, playlistId);
            MiddleContentFrame.Navigate(albumInfoView);
            //changing active tab
            activeTabButton.Style = (Style)FindResource("UpperTabButton");
            activeTabButton = AInfoTabButton;
            activeTabButton.Style = (Style)FindResource("UpperTabActiveButton");
        }

        /// <summary>
        /// Adds new string to display on informator bar at bottom of window
        /// If queue is empty, shows the informator bar
        /// </summary>
        /// <param name="info">Text to show in informator bar</param>
        public void CallInformator(string info)
        {
            informatorQueue.Enqueue(info);
            if (!informatorShown)
                ShowInformator();
        }
        /// <summary>
        /// Adds new string to display on informator, started with debug label
        /// Works only on debug mode, otherwise, does nothing
        /// <see cref="CallInformator(string)"/>
        /// </summary>
        /// <param name="info">Text to show in informator bar</param>
        public void CallDebugInformator(string info)
        {
#if DEBUG
            CallInformator("⚠️ [DEBUG]:" + info);
#endif
        }
        /// <summary>
        /// Displays informator bar and next text from queue
        /// After delay checks whether hide informator or show next text
        /// </summary>
        private void ShowInformator()
        {
            Informator.Content = informatorQueue.Dequeue();
            if(!informatorShown)
            {
                //actual showing the informator (if it was hidden)
                Informator.Visibility = Visibility.Visible;
                informatorShown = true;
            }
            Task t = new Task(async delegate{
                await Task.Delay(3000);
                if (informatorQueue.Count > 0)
                    ShowInformator();
                else
                    HideInformator();
            });
            t.RunSynchronously();
        }
        /// <summary>
        /// Hides informator bar
        /// </summary>
        private void HideInformator()
        {
            if(informatorShown)
            {
                Informator.Visibility = Visibility.Collapsed;
                informatorShown = false;
            }
        }
    }
}
