﻿using MusicStreamServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MusicStreamClient.MainClientW
{
    /// <summary>
    /// Page displayed as tab of MainClientWindow.
    /// Contains playlist of songs and options to clear or save it.
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    public partial class MCWPlaylist : Page
    {
        private List<Song> songList;
        private Dictionary<uint, bool> fullSongInfo; //"is song info collected" dictionary, key is song ID
        //album info is needed only for album name and it's being saved in song object 
        private Dictionary<uint, bool> fullAlbumInfo; //"is album info collected" dictionary, key is album ID
        private int playingSongIndex = -1;  //index on playlist of the song that's being played

        private bool missingInfo = false;

        /// <summary>
        /// Constructor. Loads empty playlist.
        /// </summary>
        public MCWPlaylist()
        {
            InitializeComponent();

            //setting localised names
            NoLabel.Content = globNames.playlistWNoLabel;
            NameLabel.Content = globNames.playlistWNameLabel;
            AlbumLabel.Content = globNames.playlistWAlbumLabel;
            ArtistLabel.Content = globNames.playlistWArtistLabel;
            LengthLabel.Content = globNames.playlistWLengthLabel;
            ClearPlaylistButton.Content = globNames.playlistWClearButton;
            LoopCheck.Content = globNames.playlistWLoopCheckbox;
            SavePlaylistButton.Content = globNames.playlistWSaveButton;
            NameBoxLabel.Content = globNames.playlistWPlaylistName;

            //initialising lists with empty containers
            songList = new List<Song> { };
            fullSongInfo = new Dictionary<uint, bool> { };
            fullAlbumInfo = new Dictionary<uint, bool> { };
            SongListView.ItemsSource = songList;

            //assigning event handlers
            App currApp = (App)Application.Current;
            currApp.GetCommunicateListener.albumInfoResultEvent +=
                delegate (Album album, uint playlistId, float rating, uint trackCount) { UpdateAlbum(album); };
            currApp.GetCommunicateListener.songInfoResultEvent +=
                delegate (ListedSong song, uint albumId, float rating) { UpdateSong(song, albumId); };
        }

        /// <summary>
        /// Adds song to the playlist.
        /// Plays the song if none is being played.
        /// </summary>
        /// <param name="song">Song to add (as ListedSong object)</param>
        /// <param name="album">Album the song belongs to (only name and ID needed)</param>
        public void AddSong(ListedSong song, Album album)
        {
            if( songList.Exists(x => x.Id == song.Id)) //checking if song already in playlist
            {
                MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                if (mcw != null)
                    mcw.CallInformator("Song already on the playlist.");
            }
            else
            {
                //adding the song
                Song nSong = new Song(song.Id, song.Name, song.Artist, song.FormalLength);
                nSong.AssingNewAlbum(album);
                nSong.AssignNewPlaylistTrackNumber((uint)(songList.Count + 1));
                songList.Add(nSong);
                fullAlbumInfo.Add(song.Id, true);
                fullSongInfo.Add(song.Id, true);
                SongListView.Items.Refresh();
            }

            //starting an added song if none is playing
            if(playingSongIndex == -1)
            {
                ChangeCurrentPlayedSong(song.Id);
                MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                if (mcw != null)
                    mcw.SoundPlayerBar.ChangeSong(songList[playingSongIndex]);
            }
        }
        /// <summary>
        /// Adds song to the playlist.
        /// Plays the song if none is being played.
        /// </summary>
        /// <param name="song">Song to add (as ListedSong object)</param>
        /// <param name="albumId">ID of album the song belongs to</param>
        public void AddSong(ListedSong song, uint albumId)
        {
            if (songList.Exists(x => x.Id == song.Id)) //checking if song already in playlist
            {
                MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                if (mcw != null)
                    mcw.CallInformator("Song already on the playlist.");
            }
            else
            {
                //adding the song
                Song nSong = new Song(song.Id, song.Name, song.Artist, song.FormalLength);
                nSong.AssingNewAlbum(new Album(albumId,"","","",null));
                nSong.AssignNewPlaylistTrackNumber((uint)(songList.Count + 1));
                songList.Add(nSong);
                if(!fullAlbumInfo.ContainsKey(song.Id))
                    fullAlbumInfo.Add(song.Id, false);
                if(!fullSongInfo.ContainsKey(song.Id))
                    fullSongInfo.Add(song.Id, true);
                SongListView.Items.Refresh();
            }

            //starting routines for collecting missing informations
            missingInfo = true;
            CheckForMissingInfo();
            Task t = new Task(async delegate {
                while (missingInfo)
                {
                    await Task.Delay(App.MISSING_INFO_CHECK_DELAY);
                    CheckForMissingInfo();
                }});
            t.Start();

            //starting an added song if none is playing
            if (playingSongIndex == -1)
            {
                ChangeCurrentPlayedSong(song.Id);
                MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                if (mcw != null)
                    mcw.SoundPlayerBar.ChangeSong(songList[playingSongIndex]);
            }
        }
        /// <summary>
        /// Adds many songs from one album to the playlist.
        /// Plays the first song if none is being played.
        /// </summary>
        /// <param name="songs">List of songs to add (as ListedSongs)</param>
        /// <param name="album">Album all the songs belong to (common for all).</param>
        public void AddManySongs(List<ListedSong> songs, Album album)
        {
            foreach (ListedSong song in songs) //iterating through the songs
            {
                if (!songList.Exists(x => x.Id == song.Id))
                {
                    //if song is not in the playlist already add it
                    Song nSong = new Song(song.Id, song.Name, song.Artist, song.FormalLength);
                    nSong.AssingNewAlbum(album);
                    nSong.AssignNewPlaylistTrackNumber((uint)(songList.Count + 1));
                    songList.Add(nSong);
                    if(!fullAlbumInfo.ContainsKey(song.Id))
                        fullAlbumInfo.Add(song.Id, true);
                    if (!fullSongInfo.ContainsKey(song.Id))
                        fullSongInfo.Add(song.Id, true);
                } 
            }
            SongListView.Items.Refresh();

            //starting first added song if none is playing
            if (playingSongIndex == -1 && songs.Count>0)
            {
                ChangeCurrentPlayedSong(songs[0].Id);
                MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                if (mcw != null)
                    mcw.SoundPlayerBar.ChangeSong(songList[playingSongIndex]);
            }
        }
        /// <summary>
        /// Adds many songs to the playlist.
        /// Plays the first song if none is being played.
        /// </summary>
        /// <param name="ids">IDs of the songs to add</param>
        public void AddManySongs(List<uint> ids)
        {
            foreach(uint id in ids)     //iterating through song IDs
            {
                if (!songList.Exists(x => x.Id == id))
                {
                    //if song is not in the playlist, add it
                    Song nSong = new Song(id, "", "", 0);
                    nSong.AssignNewPlaylistTrackNumber((uint)(songList.Count + 1));
                    songList.Add(nSong);
                    if (!fullAlbumInfo.ContainsKey(id))
                        fullAlbumInfo.Add(id, false);
                    if (!fullSongInfo.ContainsKey(id))
                        fullSongInfo.Add(id, false);
                }
            }
            SongListView.Items.Refresh();

            //starting routines for collecting missing informations
            missingInfo = true;
            CheckForMissingInfo();
            Task t = new Task(async delegate {
                while (missingInfo)
                {
                    await Task.Delay(App.MISSING_INFO_CHECK_DELAY);
                    CheckForMissingInfo();
                }});
            t.Start();

            //starting the first of added songs if none is playing
            if (playingSongIndex == -1 && ids.Count > 0)
            {
                ChangeCurrentPlayedSong(ids[0]);
                MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                if (mcw != null)
                    mcw.SoundPlayerBar.ChangeSong(songList[playingSongIndex]);
            }
        }
        /// <summary>
        /// Clears the playlist and removes every song from it.
        /// </summary>
        public void ClearPlaylist()
        {
            songList.Clear();
            SongListView.Items.Refresh();
            fullAlbumInfo.Clear();
            fullSongInfo.Clear();
            ChangeCurrentPlayedSong(null);
            MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
            if (mcw != null)
                mcw.SoundPlayerBar.ChangeSong(null);
        }

        /// <summary>
        /// Event handler that runs when new song info is received. 
        /// Updates the information about that song if it's in the playlist
        /// and checks for missing info.
        /// </summary>
        /// <param name="song">ListedSong object constructed from received info</param>
        /// <param name="albumId">ID of album associated with that song</param>
        public void UpdateSong(ListedSong song, uint albumId)
        {
            int index = songList.FindIndex(x => x.Id == song.Id);
            if(index != -1)
            {
                Song nSong = new Song(song.Id, song.Name, song.Artist, song.FormalLength);
                nSong.AssingNewAlbum(new Album(albumId, "", "", "", null));
                nSong.AssignNewPlaylistTrackNumber((uint)index + 1);
                songList[index] = nSong;
                fullSongInfo[song.Id] = true;
                CheckForMissingInfo();
            }
        }
        /// <summary>
        /// Event handler that runs when new album info is received. 
        /// Updates the information about that album if it's in the playlist
        /// and checks for missing info.
        /// </summary>
        /// <param name="album">Album object constructed from received info</param>
        public void UpdateAlbum(Album alb)
        {
            foreach(Song s in songList.FindAll(x => x.AlbumId == alb.Id))
            {
                s.AssingNewAlbum(alb);
                fullAlbumInfo[s.Id] = true;
            }
            CheckForMissingInfo();
        }
        /// <summary>
        /// Checks if there is song or album ID on page iformation about are unknown.
        /// If yes, sends request for that information.
        /// </summary>
        private void CheckForMissingInfo()
        {
            try
            {
                if(fullSongInfo.ContainsValue(false))
                {
                    uint id = fullSongInfo.First(x => x.Value == false).Key;
                    App currApp = (App)Application.Current;
                    currApp.SendInfoRequest(id, false);
                }
                else
                {
                    if (fullAlbumInfo.ContainsValue(false))
                    {
                        uint id = fullAlbumInfo.First(x => x.Value == false).Key;
                        uint albumId = songList.Find(x => x.Id == id).AlbumId;
                        App currApp = (App)Application.Current;
                        currApp.SendInfoRequest(albumId, true);
                    }
                    else
                    {
                        Application.Current.Dispatcher.Invoke((Action)delegate
                        {
                            SongListView.Items.Refresh();
                        });
                        missingInfo = false;
                    }
                }
            }
            catch (Exception) { }
        }
        /// <summary>
        /// Aborts any further checking for missing info.
        /// </summary>
        public void AbortMissingInfoCheck() { missingInfo = false; }

        /// <summary>
        /// Changes song marked as being played to selected one.
        /// </summary>
        /// <remarks>Doesn't change the actually played song,
        /// only the selection on playlist. For changing played song
        /// refer to <see cref="MCWSoundPlayer.ChangeSong(Song)"/></remarks>
        /// <param name="id">ID of selected song or null if supposed to
        /// unmark played song and mark none song.</param>
        public void ChangeCurrentPlayedSong(uint? id)
        {
            if (id == null)
                playingSongIndex = -1;
            else
            {
                try
                {
                    songList[playingSongIndex].IsPlaying = false;
                }
                catch (Exception) { }
                playingSongIndex = songList.FindIndex(x => x.Id == id);
                try
                {
                    songList[playingSongIndex].IsPlaying = true;
                }
                catch (Exception) { }
                SongListView.Items.Refresh();
            }
        }
        /// <summary>
        /// Song in the playlist at first index.
        /// </summary>
        /// <returns>Song or null if there's no song in playlist.</returns>
        public Song FirstSong()
        {
            if (songList.Count > 0)
                return songList[0];
            else
                return null;
        }
        /// <summary>
        /// Song in the playlist at last index.
        /// </summary>
        /// <returns>Song or null if there's no song in playlist.</returns>
        public Song LastSong()
        {
            if (songList.Count > 0)
                return songList[songList.Count-1];
            else
                return null;
        }
        /// <summary>
        /// Song in the playlist after one being currently played.
        /// </summary>
        /// <remarks>
        /// Depending on the selection of the LoopCheck checkbox, after last song
        /// returned is first song or null.
        /// </remarks>
        /// <returns>Song or null if there's no song in playlist, none is being 
        /// played or current is last one to play.</returns>
        public Song NextSong()
        {
            if (playingSongIndex > -1 && playingSongIndex < songList.Count)
            {
                if (playingSongIndex + 1 < songList.Count)
                    return songList[playingSongIndex + 1];
                else
                {
                    if (LoopCheck.IsChecked ?? false)
                        return songList[0];
                    else
                        return null;
                }
            }
            else
                return null;
        }
        /// <summary>
        /// Song in the playlist before one being currently played 
        /// or first on playlist if none is being played.
        /// </summary>
        /// <remarks>
        /// Depending on the selection of the LoopCheck checkbox, "before" first song
        /// returned is first or last song.
        /// </remarks>
        /// <returns>Song or null if there's no song in playlist.</returns>
        public Song PrevSong()
        {
            if (playingSongIndex > -1 && playingSongIndex < songList.Count)
            {
                if (playingSongIndex > 0)
                    return songList[playingSongIndex - 1];
                else
                {
                    if (LoopCheck.IsChecked ?? false)
                        return songList[songList.Count - 1];
                    else
                        return songList[0];
                }
            }
            else
            {
                if (songList.Count == 0)
                    return null;
                else
                    return songList[0];
            }
        }
        /// <summary>
        /// Currently played song.
        /// </summary>
        /// <returns>Song or null if there's no song in playlist.</returns>
        public Song CurrentSong()
        {
            if (playingSongIndex > -1 && playingSongIndex < songList.Count)
                return songList[playingSongIndex];
            else
                return null;
        }

        /// <summary>
        /// Event handler for double clicking on the song in the list.
        /// Changes currently played song to selected one.
        /// </summary>
        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Song selSong = songList[SongListView.SelectedIndex];
                MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                mcw.SoundPlayerBar.ChangeSong(selSong);
                ChangeCurrentPlayedSong(selSong.Id);
            }
            catch (Exception) { }
        }
        /// <summary>
        /// Event handler for "Clear playlist" button clicking.
        /// Clears the playlist, removes every song from it and changes played song to none.
        /// </summary>
        private void ClearPlaylistButton_Click(object sender, RoutedEventArgs e)
        {
            ClearPlaylist();
        }
        /// <summary>
        /// Event handler for "Save playlist" button.
        /// Constructs and sens the request to save current playlist with name
        /// entered in NameBox.
        /// </summary>
        private void SavePlaylistButton_Click(object sender, RoutedEventArgs e)
        {
            string playlistName = NameBox.Text.Trim();
            if(playlistName.Length == 0)
            {
                MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                if (mcw != null)
                    mcw.CallInformator("You must choose a name for playlist.");
            }
            else
            {
                List<uint> idList = new List<uint> { };
                uint totalLength = 0;
                foreach (Song s in songList)
                {
                    idList.Add(s.Id);
                    totalLength += s.FormalLength;
                }
                Playlist pl = new Playlist(0, 0, totalLength, idList);
                pl.SetCreationTime(DateTime.Now);
                pl.SetName(playlistName);
                App currApp = (App)Application.Current;
                if (currApp != null)
                    currApp.SendPlaylistSaveRequest(pl);
            }
        }
    }
}
