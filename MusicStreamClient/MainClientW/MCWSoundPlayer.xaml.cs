﻿using MusicStreamServer;
using NAudio.Wave;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MusicStreamClient.MainClientW
{
    /// <summary>
    /// Page displayed at the bottom of MainClientWindow.
    /// Responsible for playing the song, its progress, chaning etc.
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    public partial class MCWSoundPlayer : Page
    {
        private ProgressDisplay progressBarDisplay;
        private Song activeSong;
        private bool paused = true;

        private byte[] songDataStream;
        private uint loadedSongDataSections = 0;
        private uint totalSectionsCount = 0;
        private uint playedSectionCount = 0;
        private uint samplingRate, bitDepth, channels;
        private Task playTask;
        private WaveOutEvent waveOut = new WaveOutEvent();
        private MyWaveProvider waveProvider;

        public MCWSoundPlayer()
        {
            InitializeComponent();

            progressBarDisplay = new ProgressDisplay(1, (uint)ProgressBar.ActualWidth);
            ProgressBar.DataContext = progressBarDisplay;
            activeSong = new Song(0, "", "", 0);
            SongLabel.DataContext = activeSong;
            TimeStampPlayed.DataContext = new Tuple<uint>(0);
            TimeStampFull.DataContext = new Tuple<uint>(0);

            App currApp = (App)Application.Current;
            if (currApp != null)
                currApp.GetCommunicateListener.coverartDownloadedEvent += UpdateCoverart;
        }
        ~MCWSoundPlayer()
        {
            //probably doesn't work as intended
            try
            {
                waveOut.Stop();
                waveOut.Dispose();
                songDataStream = null;
                playTask.Dispose();
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Changes the currently played song to selected one.
        /// </summary>
        /// <param name="nSong">New song to play. May be null value to play nothing.</param>
        public void ChangeSong(Song nSong)
        {
            waveOut.Stop();
            if (nSong != null)
            {
                activeSong = nSong;
                progressBarDisplay.ResetTotalTime(nSong.FormalLength);
                TimeStampPlayed.DataContext = new Tuple<uint>(0);
                TimeStampFull.DataContext = new Tuple<uint>(nSong.FormalLength);
                MiniAlbumCover.DataContext = ImageFileComposer.GetAlbumPath(nSong.AlbumId);

                //sending the stream request (and coverart, eventually)
                App currApp = (App)Application.Current;
                if (currApp != null)
                    currApp.SendSongStreamRequest(nSong.Id, 0);
                if (!ImageFileComposer.IsAlreadyDownloaded(nSong.AlbumId) && currApp != null && nSong.AlbumId!=0)
                    currApp.SendCoverartRequest(nSong.AlbumId, 0);

                //task to repeat sending request if data is not received (it happens)
                Task repeatRequestTask = new Task(async delegate
                {
                    await Task.Delay(300);
                    while (loadedSongDataSections == 0)
                    {
                        currApp = (App)Application.Current;
                        if (currApp != null && activeSong.FormalLength > 0)
                            currApp.SendSongStreamRequest(nSong.Id, 0);
                        await Task.Delay(500);
                    }
                });
                repeatRequestTask.Start();
            }
            else
            {
                //empty song - nothing is being played
                activeSong = new Song(0, "", "", 0);
                progressBarDisplay.ResetTotalTime(1);
                TimeStampPlayed.DataContext = new Tuple<uint>(0);
                TimeStampFull.DataContext = new Tuple<uint>(0);
                MiniAlbumCover.DataContext = "";
            }

            //resetting the data
            SongLabel.DataContext = activeSong;
            loadedSongDataSections = 0;
            songDataStream = null;
            //"kicking" the GC to clean memory freed after loading new song
            if (totalSectionsCount > 0)
                GC.AddMemoryPressure(totalSectionsCount * Song.BYTES_PER_SECTION);
            totalSectionsCount = 0;
            //GC.Collect(); //Solved by code above, but used to prove that problem was with GC.
        }
        /// <summary>
        /// Starts/resumes the song and updates button appearance.
        /// </summary>
        public void SongPlay()
        {
            if (activeSong.FormalLength > 0)
            {
                paused = false;
                PlayButtonPol.Visibility = Visibility.Hidden;
                PauseButtonPol.Visibility = Visibility.Visible;
            }
            else
                SongPause();
        }
        /// <summary>
        /// Pauses the song and updates button appearance.
        /// </summary>
        public void SongPause()
        {
            paused = true;
            PlayButtonPol.Visibility = Visibility.Visible;
            PauseButtonPol.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Synchronizes the internal buffer of waveProvider with data stream in this page instance
        /// and reads how much song has been played. Updates progress bar and time counter based
        /// on that information. If song ends, starts next song.
        /// </summary>
        public void SynchronizeBuffers()
        {
            int played = waveProvider.SynchronizeBuffers(songDataStream,
                                (int)loadedSongDataSections * (int)Song.BYTES_PER_SECTION);
            playedSectionCount = (uint)played / Song.BYTES_PER_SECTION;
            uint playLength = (uint)((float)playedSectionCount / (float)totalSectionsCount
                        * activeSong.FormalLength);
            progressBarDisplay.UpdatePlay(playLength);
            TimeStampPlayed.DataContext = new Tuple<uint>(playLength);
            if (playedSectionCount >= totalSectionsCount && totalSectionsCount != 0)
            {
                MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                Song nSong = mcw.PlaylistView.NextSong();
                ChangeSong(nSong);
                if (nSong != null)
                    mcw.PlaylistView.ChangeCurrentPlayedSong(nSong.Id);
                else
                    mcw.PlaylistView.ChangeCurrentPlayedSong(null);
            }
        }

        /// <summary>
        /// Handles the song stream packet received from CommunicateListener.
        /// <see cref="CommunicateListener"/>
        /// Contains the main song playing task definition.
        /// </summary>
        /// <param name="msg">Packet</param>
        /// <param name="bytesRead">Amount of bytes read</param>
        public void HandleSongStream(byte[] msg, int bytesRead)
        {
            if ((COMM_PREFIX)msg[0] != COMM_PREFIX.COMMS_SONG_STREAM)
                throw new ArgumentException("Message not of COMMS_SONG_STREAM type.");

            uint readId = BitConverter.ToUInt32(msg, 1);
            if (readId == activeSong.Id && activeSong.FormalLength > 0)
            {
                if (songDataStream == null)
                {
                    //initialising the data and tasks if stream is null
                    samplingRate = BitConverter.ToUInt16(msg, 5);
                    bitDepth = msg[7];
                    channels = msg[8];
                    totalSectionsCount = BitConverter.ToUInt32(msg, 9);
                    songDataStream = new byte[totalSectionsCount * Song.BYTES_PER_SECTION];

                    waveProvider = new MyWaveProvider(new WaveFormat(
                        (int)samplingRate, (int)bitDepth, (int)channels));
                    
                    // MAIN SOUND PLAYING TASK!!!                                 !IMPORTANT CODE BELOW!
                    ///<summary>
                    ///Task responsible for playing the song.
                    ///Reacts to changes in data of SoundPlayer accordingly
                    ///</summary>
                    playTask = new Task(async delegate
                    {
                        waveOut.Stop();
                        try
                        {
                            waveOut.Init(waveProvider);
                            waveOut.Play();
                            if (paused)
                                waveOut.Pause();
                        }
                        catch (Exception) { waveOut.Stop(); }

                        while (waveOut.PlaybackState != PlaybackState.Stopped)
                        {
                            try
                            {
                                Application.Current.Dispatcher.Invoke((Action)delegate
                                {
                                    MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                                    if (mcw != null)
                                        mcw.SoundPlayerBar.SynchronizeBuffers();
                                    else
                                        waveOut.Stop();
                                });
                            }
                            catch (Exception) { waveOut.Stop(); }

                            if (playedSectionCount >= loadedSongDataSections
                                    && loadedSongDataSections < totalSectionsCount && activeSong != null)
                            {
                                waveOut.Pause();
                                App currApp = (App)Application.Current;
                                if (currApp != null && activeSong.FormalLength > 0)
                                    currApp.SendSongStreamRequest(activeSong.Id, loadedSongDataSections);
                                await Task.Delay(500);
                            }

                            if (paused && waveOut.PlaybackState == PlaybackState.Playing)
                                waveOut.Pause();
                            if (!paused && waveOut.PlaybackState == PlaybackState.Paused)
                                waveOut.Play();
                            await Task.Delay(50);
                        }
                    });
                    playTask.Start();
                }

                //loading the section of song stream
                uint sentSectionsFromMsg = BitConverter.ToUInt32(msg, 17);
                uint sentSectionsFromLen = (bytesRead > 32) ?
                    (uint)((bytesRead - 32) / Song.BYTES_PER_SECTION) : 0;
                uint sentSections = Math.Min(sentSectionsFromLen, sentSectionsFromMsg);
                uint firstIndex = BitConverter.ToUInt32(msg, 13);

                if (firstIndex <= loadedSongDataSections && sentSections > 0)
                {
                    for (int iS = 0; iS < sentSections; iS++)
                    {
                        Array.Copy(msg, 32 + iS * Song.BYTES_PER_SECTION,
                            songDataStream, (firstIndex + iS) * Song.BYTES_PER_SECTION, Song.BYTES_PER_SECTION);
                    }
                    loadedSongDataSections = firstIndex + sentSections;
                    uint loadLength = (uint)((float)loadedSongDataSections / (float)totalSectionsCount
                        * activeSong.FormalLength);
                    progressBarDisplay.UpdateLoad(loadLength);
                }

                if (loadedSongDataSections < totalSectionsCount)
                {
                    App currApp = (App)Application.Current;
                    if (currApp != null)
                        currApp.SendSongStreamRequest(activeSong.Id, loadedSongDataSections);
                }
            }
        }
        /// <summary>
        /// Event handler that runs when new album is downloaded. 
        /// Updates the coverart if played song belongs to album of that coverart.
        /// </summary>
        /// <param name="id">ID of album which coverart has been downloaded</param>
        private void UpdateCoverart(uint id)
        {
            if(activeSong!=null)
            {
                if(activeSong.AlbumId == id)
                    MiniAlbumCover.DataContext = ImageFileComposer.GetAlbumPath(id);
            }
        }

        /// <summary>
        /// Event handler for 3 buttons clicking.
        /// Checks which one has been clicked and either changes song 
        /// to next/previous or pauses/resumes the song.
        /// </summary>
        private void SongButton_Click(object sender, RoutedEventArgs e)
        {
            if(sender == PrevSongButton || sender == NextSongButton)
            {
                MainClientWindow mcw = (MainClientWindow)Application.Current.MainWindow;
                Song nsong = (sender == PrevSongButton) ? 
                    mcw.PlaylistView.PrevSong() : mcw.PlaylistView.NextSong() ;
                ChangeSong(nsong);
                if(nsong!=null)
                    mcw.PlaylistView.ChangeCurrentPlayedSong(nsong.Id);
                else
                    mcw.PlaylistView.ChangeCurrentPlayedSong(null);
            }
            else if(sender == PlayPauseButton)
            {
                if (paused)
                    SongPlay();
                else
                    SongPause();
            }
        }
        /// <summary>
        /// Event handler for clicking on progress bar.
        /// Reads the position of click and sets player to according fragment of the song.
        /// </summary>
        private void ProgressBarClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != ProgressBar)
                throw new Exception("Method can be used only for ProgressBar canvas.");

            double posX = e.GetPosition(ProgressBar).X;
            double maxX = ProgressBar.ActualWidth;
            double percentClick = Math.Min(1, Math.Max(0, posX / maxX));
            uint sectionClicked = (uint)(percentClick * totalSectionsCount);
            if (sectionClicked > loadedSongDataSections)
                sectionClicked = loadedSongDataSections;
            waveProvider.SetPosition((int)(sectionClicked * Song.BYTES_PER_SECTION));
        }
        /// <summary>
        /// Event handler for changing window size.
        /// Resizes the progressbar accordingly.
        /// </summary>
        private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            progressBarDisplay.Resize((uint)ProgressBar.ActualWidth);
        }
    }
}
