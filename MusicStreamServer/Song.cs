﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml.Linq;
using NLayer;

namespace MusicStreamServer
{
    /// <summary>
    /// Class for objects representing songs with full information
    /// about them and the actual track of sound, in case of server-side usage.
    /// </summary>
    public class Song
    {
        public const uint BYTES_PER_SECTION = 256;

        private uint songId;
        private uint albumId;
        private string albumName;
        private uint albumTrackNumber;
        private uint playlistTrackNumber;
        private bool playing;

        private string songName;
        private string songArtist;
        private string songPath;
        private uint formalLength;

        private bool loaded = false;
        private bool loading = false;
        private uint samplingRate;
        private uint bitDepth;
        private ulong sectionCount;
        private bool[] sectionsLoaded;
        private byte[] audio;
        private DateTime lastRequested;
        private Task loadingTask;

        public uint Id => songId;
        public uint AlbumId => albumId;
        public string AlbumName => albumName;
        public string Name => songName;
        public string Artist => songArtist;
        public uint FormalLength => formalLength;
        public uint TrackNumber => albumTrackNumber;
        public uint PlaylistTrackNumber => playlistTrackNumber;
        /// <summary>
        /// Indicates whether the song is being played currently.
        /// Used in client only.
        /// </summary>
        public bool IsPlaying { get => playing; set => playing = value; }

        internal bool Loaded => loaded;
        internal bool Loading => loading;
        internal uint SamplingRate => samplingRate;
        internal uint BitDepth => bitDepth;
        internal ulong SectionCount => sectionCount;

        /// <summary>
        /// Creates Song object based on given parameters.
        /// </summary>
        /// <param name="id">Song ID</param>
        /// <param name="name">Song name</param>
        /// <param name="artist">Song artist name</param>
        /// <param name="length">Length of the song, in seconds</param>
        public Song(uint id, string name, string artist, uint length)
        {
            songId = id;
            songName = name;
            songArtist = artist;
            formalLength = length;
            playing = false;
            albumName = ""; albumId = 0; albumTrackNumber = 0; playlistTrackNumber = 0;
        }
        /// <summary>
        /// Reads the XML file element and creates Song object based on it.
        /// </summary>
        /// <param name="songElement">&lt;song/&gt; XML element</param>
        /// <param name="albumIdVal">Album ID</param>
        /// <param name="album">Album name</param>
        /// <param name="trackNo">Index on tracklist</param>
        public Song(XElement songElement, uint albumIdVal, string album, uint trackNo)
        {
            loaded = false;
            playing = false;
            try
            {
                songId = Convert.ToUInt32(songElement.Attribute("id").Value);
                songName = songElement.Attribute("name").Value;
                songArtist = songElement.Attribute("artist").Value;
                formalLength = Convert.ToUInt32(songElement.Attribute("length").Value);
                songPath = songElement.Attribute("path").Value;
                albumTrackNumber = trackNo;
                albumId = albumIdVal;
                albumName = album;
            }
            catch (Exception) { throw new Exception("XML incorrect"); }
        }
        /// <summary>
        /// Changes the album name and ID assigned to the song and its track index.
        /// </summary>
        /// <param name="alb">Album being source of album name, ID and track index of song.</param>
        public void AssingNewAlbum(Album alb)
        {
            albumName = alb.Name;
            albumId = alb.Id;
            if (alb.SongIDList.Count > 0)
                albumTrackNumber = (uint)(alb.SongIDList.FindIndex(x => x == songId) + 1);
            else
                albumTrackNumber = 0;
        }
        /// <summary>
        /// Changes index of the song on its tracklist.
        /// </summary>
        /// <param name="num">New index on tracklist</param>
        public void AssignNewPlaylistTrackNumber(uint num) { playlistTrackNumber = num; }

        /// <summary>
        /// Try to load the song from file.
        /// </summary>
        internal void LoadSong()
        {
            if (loading)
                throw new Exception("Already loading");
            try
            {
                loading = true;
                using (MpegFile fileReader = new MpegFile(songPath))
                {
                    samplingRate = (uint)fileReader.SampleRate;
                    bitDepth = 16;
                    long totalSampleLength = fileReader.Length / sizeof(float);
                    sectionCount = (ulong)Math.Ceiling((double)totalSampleLength*2 / (double)BYTES_PER_SECTION);
                    sectionsLoaded = new bool[sectionCount];
                    for (int i = 0; (uint)i < sectionCount; i++)
                        sectionsLoaded[i] = false;
                    audio = new byte[sectionCount * BYTES_PER_SECTION];

                    short sample;
                    byte[] bSample;
                    int loopLength = (int)BYTES_PER_SECTION / 2;
                    float[] audioFloatBuff = new float[loopLength];
                    int readFloats = 0;
                    int offset = 0;
                    for (int iSec = 0; (uint)iSec < sectionCount; iSec++)
                    {
                        offset = iSec * (int)BYTES_PER_SECTION;
                        readFloats = fileReader.ReadSamples(audioFloatBuff, 0, loopLength);
                        for (int i = 0; i < readFloats; i++)
                        {
                            sample = (short)(audioFloatBuff[i] * 0.8 * Int16.MaxValue);
                            bSample = BitConverter.GetBytes(sample);
                            audio[offset + 2 * i] = bSample[0];
                            audio[offset + 2 * i + 1] = bSample[1];
                        }
                        sectionsLoaded[iSec] = true;
                    }
                }

                loaded = true;
            }
            catch (Exception)
            {
                System.Console.WriteLine("Failed to load song " + songName);
            }
            finally { loading = false; }
        }
        /// <summary>
        /// Unload the song and free the memory it took.
        /// </summary>
        internal void DiscardSong()
        {
            loaded = false;
            audio = null;
            GC.AddMemoryPressure((long)sectionCount * BYTES_PER_SECTION);
        }
        /// <summary>
        /// Checks if last request time was enough time ago and discards song if so.
        /// </summary>
        /// <param name="timeout">Time that has to pass since last request to discard it, in seconds</param>
        internal void CheckForTimeout(long timeout)
        {
            if (DateTime.Now.Subtract(lastRequested).TotalSeconds >= timeout)
                DiscardSong();
        }
        /// <summary>
        /// Gets the section of loaded song.
        /// </summary>
        /// <param name="sectionIndex">Index of section</param>
        /// <returns>Byte array conaining data from selecte section</returns>
        internal byte[] RequestSection(uint sectionIndex)
        {
            if (sectionIndex >= sectionCount)
                throw new ArgumentException("Index out of range");
            lastRequested = DateTime.Now;
            byte[] result = new byte[BYTES_PER_SECTION];
            if (sectionsLoaded[sectionIndex])
                Array.Copy(audio, sectionIndex * BYTES_PER_SECTION, result, 0, BYTES_PER_SECTION);
            else
                throw new Exception("Song not loaded");
            return result;
        }
        internal bool FirstSectionAvailable() { return (sectionsLoaded==null)? false : sectionsLoaded[0]; }
        internal void StartLoadingTask() { loadingTask = new Task(LoadSong); loadingTask.Start(); }
        /// <summary>
        /// Sets time of last request to current time.
        /// </summary>
        internal void BumpRequestTime() { lastRequested = DateTime.Now; }
    }

    /// <summary>
    /// Class for objects representing song general informations,
    /// without their content.
    /// </summary>
    public class ListedSong
    {
        public uint Id { get; set; }
        public uint TrackNo { get; set; }
        public string Name { get; set; }
        public string Artist { get; set; }
        public uint FormalLength { get; set; }

        /// <summary>
        /// Default ListedSong constructor
        /// </summary>
        /// <param name="id">Song ID</param>
        /// <param name="no">Index on tracklist</param>
        /// <param name="songName">Name of the song</param>
        /// <param name="art">Name of the song artist</param>
        /// <param name="len">Length of the song, in seconds</param>
        public ListedSong(uint id, uint no, string songName, string art, uint len)
        {
            Id = id;
            TrackNo = no;
            Name = songName;
            Artist = art;
            FormalLength = len;
        }
        /// <summary>
        /// Creates ListedSong basing on Song object.
        /// </summary>
        /// <param name="source">Source Song object</param>
        /// <param name="trackNo">Index this song has on tracklist</param>
        public ListedSong(Song source, uint trackNo)
        {
            Id = source.Id;
            TrackNo = trackNo;
            Name = source.Name;
            Artist = source.Artist;
            FormalLength = source.FormalLength;
        }

        //comparing functions, System.Comparison delegates, first one is for simplifying routines
        private static int CompareNull(ListedSong x, ListedSong y)
        {
            if (x.Equals(null))
            {
                if (y.Equals(null))
                    return 0;
                else
                    return -1;
            }
            if (y.Equals(null))
                return 1;
            else
                return 2;
        }
        /// <summary>
        /// System.Comparison<T> Delegate
        /// compares properties Name of 2 ListedSong objects
        /// </summary>
        /// <param name="x">First object to compare</param>
        /// <param name="y">Second object to compare</param>
        /// <returns>0 if objects are equal, number greater than 0 if first one is greater,
        /// number lesser than 0 if second one is greater</returns>
        /// <see cref="System.Comparison{T}"/>
        public static int CompareNames(ListedSong x, ListedSong y)
        {
            int nC = CompareNull(x, y);
            if (nC == 2)
                return String.Compare(x.Name, y.Name);
            else
                return nC;
        }
        /// <summary>
        /// System.Comparison<T> Delegate
        /// compares properties Artist of 2 ListedSong objects
        /// </summary>
        /// <param name="x">First object to compare</param>
        /// <param name="y">Second object to compare</param>
        /// <returns>0 if objects are equal, number greater than 0 if first one is greater,
        /// number lesser than 0 if second one is greater</returns>
        /// <see cref="System.Comparison{T}"/>
        public static int CompareArtists(ListedSong x, ListedSong y)
        {
            int nC = CompareNull(x, y);
            if (nC == 2)
                return String.Compare(x.Artist, y.Artist);
            else
                return nC;
        }
        /// <summary>
        /// System.Comparison<T> Delegate
        /// compares properties FromalLength of 2 ListedSong objects
        /// </summary>
        /// <param name="x">First object to compare</param>
        /// <param name="y">Second object to compare</param>
        /// <returns>0 if objects are equal, number greater than 0 if first one is greater,
        /// number lesser than 0 if second one is greater</returns>
        /// <see cref="System.Comparison{T}"/>
        public static int CompareLengths(ListedSong x, ListedSong y)
        {
            int nC = CompareNull(x, y);
            if (nC == 2)
                return ((int)x.FormalLength - (int)y.FormalLength);
            else
                return nC;
        }
        /// <summary>
        /// Compares ratings 2 ListedSong objects
        /// </summary>
        /// <param name="x">First object to compare</param>
        /// <param name="y">Second object to compare</param>
        /// <param name="ratings">Dictonary of song ratings with keys being Id property of song</param>
        /// <returns>0 if ratings are equal, number greater than 0 if rating of first one is greater,
        /// number lesser than 0 if rating of second one is greater</returns>
        public static int CompareRatings(ListedSong x, ListedSong y, Dictionary<uint,float> ratings)
        {
            int nC = CompareNull(x, y);
            if (nC == 2)
                return Math.Sign(ratings[x.Id] - ratings[y.Id]);
            else
                return nC;
        }
    }

    /// <summary>
    /// Class for objects representing an album of songs.
    /// </summary>
    public class Album
    {
        private uint albumId;
        private List<uint> songIdList;

        private string albumName;
        private string albumArtist;
        private string albumCoverArtPath;
        private uint totalFormalLength;

        public uint Id => albumId;
        public string Name => albumName;
        public string Artist => albumArtist;
        public List<uint> SongIDList => songIdList;
        public string CoverArtPath => albumCoverArtPath;
        public uint FormalLength => totalFormalLength;

        public uint AlbumTrackCount => (uint) songIdList.Count;

        /// <summary>
        /// Default constructor of album object.
        /// </summary>
        /// <param name="id">Album ID</param>
        /// <param name="name">Album name</param>
        /// <param name="artist">Album artist name</param>
        /// <param name="coverArt">Path to coverart image</param>
        /// <param name="songs">List of song IDs belonging to that album</param>
        public Album(uint id, string name, string artist, string coverArt, List<uint> songs)
        {
            albumId = id;
            albumName = name;
            albumArtist = artist;
            albumCoverArtPath = coverArt;
            songIdList = songs ?? new List<uint> { };
            totalFormalLength = 0;
        }
        /// <summary>
        /// Reads the XML file fragment and creates album and songs objects based on it.
        /// </summary>
        /// <param name="albumElement">&lt;album/&gt; XML element</param>
        /// <returns>The tuple containing:<list type="number">
        /// <item>Album object</item>
        /// <item>List of song objects</item>
        /// </list>created from XML element</returns>
        public static Tuple<Album, List<Song>> GetFromXML(XElement albumElement)
        {
            try
            {
                uint nAlbumId = Convert.ToUInt32(albumElement.Attribute("id").Value);
                string nAlbumName = albumElement.Attribute("name").Value;
                string nAlbumArtist = albumElement.Attribute("artist").Value;
                string nAlbumCoverArtPath = albumElement.Attribute("path").Value;
                List<XElement> songXMLlist = new List<XElement>( albumElement.Elements("song") );
                List<uint> idSongList = new List<uint> { };
                List<Song> songList = new List<Song> { };
                for(uint i = 0; i < songXMLlist.Count; i++)
                {
                    try
                    {
                        Song parsed = new Song(songXMLlist[(int)i], nAlbumId, nAlbumName, i + 1);
                        idSongList.Add(parsed.Id);
                        songList.Add(parsed);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Warning: Incorrect song XML in album of ID {0} at position {1}.", 
                                            nAlbumId, i);
                    }
                }
                Album finalAlbum = new Album(nAlbumId, nAlbumName, nAlbumArtist, nAlbumCoverArtPath, idSongList);
                return new Tuple<Album, List<Song>>(finalAlbum, songList);
            }
            catch(Exception) { throw new Exception("XML incorrect"); }
        }

        /// <summary>
        /// Recalculates total formal length of album based on given source of songs
        /// </summary>
        /// <param name="songSource">List of all loaded songs</param>
        /// <returns>True if all songs in album were found in provided list. False otherwise.</returns>
        public bool RecalcTotalFormalLength(List<Song> songSource)
        {
            bool foundAll = true;
            uint newTotalLenght = 0;
            foreach(uint sid in songIdList)
            {
                try
                {
                    Song foundSong = songSource.Find(x => x.Id == sid);
                    if (foundSong != null)
                        newTotalLenght += foundSong.FormalLength;
                    else
                        foundAll = false;
                }
                catch (Exception) { foundAll = false; }
            }
            totalFormalLength = newTotalLenght;
            return foundAll;
        }
        /// <summary>
        /// Sets total formal length to given value.
        /// </summary>
        /// <param name="len">New total length of album, in seconds</param>
        public void SetTotalFormalLength(uint len) { totalFormalLength = len; }
        /// <summary>
        /// Sets the coverart image path for album.
        /// </summary>
        /// <param name="path">Path to new coverart image</param>
        public void SetNewCoverart(string path) { albumCoverArtPath = path; }

        //comparing functions, System.Comparison delegates, first one is for simplifying routines
        private static int CompareNull(Album x, Album y)
        {
            if (x.Equals(null))
            {
                if (y.Equals(null))
                    return 0;
                else
                    return -1;
            }
            if (y.Equals(null))
                return 1;
            else
                return 2;
        }
        /// <summary>
        /// System.Comparison<T> Delegate
        /// compares properties Name of 2 Album objects
        /// </summary>
        /// <param name="x">First object to compare</param>
        /// <param name="y">Second object to compare</param>
        /// <returns>0 if objects are equal, number greater than 0 if first one is greater,
        /// number lesser than 0 if second one is greater</returns>
        /// <see cref="System.Comparison{T}"/>
        public static int CompareNames(Album x, Album y)
        {
            int nC = CompareNull(x, y);
            if (nC == 2)
                return String.Compare(x.Name, y.Name);
            else
                return nC;
        }
        /// <summary>
        /// System.Comparison<T> Delegate
        /// compares properties Artist of 2 Album objects
        /// </summary>
        /// <param name="x">First object to compare</param>
        /// <param name="y">Second object to compare</param>
        /// <returns>0 if objects are equal, number greater than 0 if first one is greater,
        /// number lesser than 0 if second one is greater</returns>
        /// <see cref="System.Comparison{T}"/>
        public static int CompareArtists(Album x, Album y)
        {
            int nC = CompareNull(x, y);
            if (nC == 2)
                return String.Compare(x.Artist, y.Artist);
            else
                return nC;
        }
        /// <summary>
        /// System.Comparison<T> Delegate
        /// compares properties FormalLength of 2 Album objects
        /// </summary>
        /// <param name="x">First object to compare</param>
        /// <param name="y">Second object to compare</param>
        /// <returns>0 if objects are equal, number greater than 0 if first one is greater,
        /// number lesser than 0 if second one is greater</returns>
        /// <see cref="System.Comparison{T}"/>
        public static int CompareLengths(Album x, Album y)
        {
            int nC = CompareNull(x, y);
            if (nC == 2)
                return ((int)x.FormalLength - (int)y.FormalLength);
            else
                return nC;
        }
        /// <summary>
        /// Compares mean ratings of songs in 2 Album objects
        /// </summary>
        /// <param name="x">First object to compare</param>
        /// <param name="y">Second object to compare</param>
        /// <param name="ratings">Dictonary of song ratings with keys being Id property of song</param>
        /// <returns>0 if ratings are equal, number greater than 0 if rating of first one is greater,
        /// number lesser than 0 if rating of second one is greater</returns>
        public static int CompareRatings(Album x, Album y, Dictionary<uint, float> ratings)
        {
            int nC = CompareNull(x, y);
            if (nC == 2)
            {
                float ratingX = 0f, ratingY = 0f;
                uint numOfX = 0, numOfY = 0;
                float rat;
                foreach(uint id in x.SongIDList)
                {
                    rat = ratings[id];
                    if(rat != 0)
                    {
                        numOfX++;
                        ratingX += rat;
                    }
                }
                foreach (uint id in y.SongIDList)
                {
                    rat = ratings[id];
                    if (rat != 0)
                    {
                        numOfY++;
                        ratingY += rat;
                    }
                }
                ratingX /= numOfX;
                ratingY /= numOfY;
                return Math.Sign(ratingX - ratingY);
            }
            else
                return nC;
        }
    }

    /// <summary>
    /// Structure for containing playlist of songs with information about creation and owner
    /// </summary>
    public struct Playlist
    {
        private uint playlistId;
        private string playlistName;
        private uint ownerUserId;
        private uint totalFormalLength;
        private DateTime creationTime;
        private List<uint> songIdList;

        public uint Id => playlistId;
        public string Name => playlistName;
        public uint Owner => ownerUserId;
        public uint Length => totalFormalLength;
        public DateTime CreationTime => creationTime;
        public List<uint> Songs => songIdList;

        /// <summary>
        /// Creates playlist structure with chosen parameters.
        /// </summary>
        /// <param name="id">Playlist ID</param>
        /// <param name="owner">User ID of playlist owner</param>
        /// <param name="length">Summed up length of tracks in playlist, in seconds</param>
        /// <param name="songs">List of song IDs of songs in playlist</param>
        public Playlist(uint id, uint owner, uint length, List<uint> songs)
        {
            playlistId = id;
            ownerUserId = owner;
            songIdList = songs ?? new List<uint> { };
            totalFormalLength = length;
            playlistName = "";
            creationTime = DateTime.Now;
        }
        /// <summary>
        /// Changes the name of playlist.
        /// </summary>
        /// <param name="name">Desired name</param>
        public void SetName(string name) { playlistName = name; }
        /// <summary>
        /// Changes the creation time of playlist.
        /// </summary>
        /// <param name="time">Desired creation time</param>
        public void SetCreationTime(DateTime time) { creationTime = time; }
        /// <summary>
        /// Creates a copy of playlist with changed ID.
        /// </summary>
        /// <param name="newId">New playlist ID value</param>
        /// <returns>Copy of playlist</returns>
        public Playlist ReindexedCopy(uint newId)
        {
            Playlist np = new Playlist(newId, ownerUserId, totalFormalLength, songIdList);
            np.SetName(playlistName);
            np.SetCreationTime(creationTime);
            return np;
        }
    }
}
