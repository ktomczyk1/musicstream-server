﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MusicStreamServer
{
    /// <summary>
    /// Class of objects to handle communication with client,
    /// created after every accepted connection
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    class CommunicationHandler
    {
        private Socket commSocket;
        /// <summary>
        /// ID of user associated with that communication handler,
        /// set after successfully logging in.
        /// </summary>
        private uint connectedUser = 0;

        public CommunicationHandler(Socket handledSocket)
        {
            commSocket = handledSocket;
            Thread commThread = new Thread(CommunicateWithClient);
            commThread.Start();
        }

        /// <summary>
        /// Initiates communication with client and if it logs in successfully
        /// continues handling the messages until communication is aborted.
        /// </summary>
        private async void CommunicateWithClient()
        {
            byte[] sDataBuffer = new byte[256];
            int recByteLenght = 0;
            bool communicationEstablished = false;

            try
            {
                recByteLenght = await commSocket.ReceiveAsync(sDataBuffer, SocketFlags.None);
            }
            catch (Exception) { }

            if(recByteLenght == 256)
            {
                //checking the content of packet and if logged in succesfully
                //otherwise checking if registered successfully
                if (sDataBuffer[0] == (byte)COMM_PREFIX.COMMC_LOGIN_TRY)
                    communicationEstablished = LoginRoutine(sDataBuffer);
                else if(sDataBuffer[0] == (byte)COMM_PREFIX.COMMC_REGISTER_TRY)
                    RegisterRoutine(sDataBuffer);
            }
            


            //if logged in successfully communications begins
            
            //event handlers
            void logoutFunc(uint id)
            {
                ConditionalSendShortComm(id, COMM_PREFIX.COMMS_LOGOUT);
                if (id == connectedUser)
                    communicationEstablished = false;
            }
            void activityFunc(uint id) { ConditionalSendShortComm(id, COMM_PREFIX.COMMS_REACT_ISACTIVE); }
            if (communicationEstablished)
            {
                Program.AssignToUserLogoutEvent(connectedUser, logoutFunc);
                Program.AssingToTimeoutCheckEvent(connectedUser, activityFunc);
            }

            //proper communication handling loop
            byte[] ans = new byte[256];
            while (communicationEstablished)
            {
                try
                {
                    recByteLenght = commSocket.Receive(sDataBuffer);
                    Program.UpdateUserActivity(connectedUser);
                    COMM_PREFIX prefix = (COMM_PREFIX)sDataBuffer[0];
                    switch (prefix)
                    {
                        case COMM_PREFIX.COMMC_LOGOUT_REQ:
                            ans[0] = (byte)COMM_PREFIX.COMMS_LOGOUT;
                            commSocket.Send(ans);
                            communicationEstablished = false;
                            break;
                        case COMM_PREFIX.COMMC_SEARCH_ALBUMS:
                        case COMM_PREFIX.COMMC_SEARCH_SONGS:
                            await Task.Run(() => {
                                try { RespondToSearch(sDataBuffer); }
                                catch (Exception) { }
                            });
                            break;
                        case COMM_PREFIX.COMMC_INFO_ALBUM:
                        case COMM_PREFIX.COMMC_INFO_SONG:
                            await Task.Run(() => {
                                try { RespondToInfo(sDataBuffer); }
                                catch (Exception) { }
                            });
                            break;
                        case COMM_PREFIX.COMMC_PLAYLIST_GETLIST:
                        case COMM_PREFIX.COMMC_PLAYLIST_LOAD:
                        case COMM_PREFIX.COMMC_PLAYLIST_SAVE:
                        case COMM_PREFIX.COMMC_PLAYLIST_DELETE:
                            await Task.Run(() => {
                                try { RespondToPlaylist(sDataBuffer); }
                                catch (Exception) { }
                            });
                            break;
                        case COMM_PREFIX.COMMC_COVERART_REQUEST:
                            await Task.Run(() => {
                                try { RespondToCoverart(sDataBuffer); }
                                catch (Exception) { }
                            });
                            break;
                        case COMM_PREFIX.COMMC_SONG_REQUEST:
                            await Task.Run(() => {
                                try { RespondToSongRequest(sDataBuffer); }
                                catch (Exception) { }
                            });
                            break;
                        case COMM_PREFIX.COMMC_SONG_RATE:
                            await Task.Run(() => {
                                try { RespondToRating(sDataBuffer); }
                                catch (Exception) { }
                            });
                            break;
                        case COMM_PREFIX.COMMC_REACT_ACTIVE:
                            Program.UpdateUserActivity(connectedUser);
                            break;
                        case COMM_PREFIX.COMM_FAILURE:
                            CommFailureReact();
                            break;
                        default:
                            ans[0] = (byte)COMM_PREFIX.COMM_FAILURE;
                            commSocket.Send(ans);
                            break;
                    }

                }
                catch(Exception)
                {
                    communicationEstablished = false;
                }
            }

            //ending the communication
            Program.LogoutUser(connectedUser);
            if(commSocket.Connected)
                commSocket.Disconnect(false);
            commSocket.Dispose();
        }

        //All of these are used in main communication loop.
        //These names are self-explainatory. What's the point of writing summary for each one...
        //All of them can throw ArgumentException if wrong type of packet is provided
        //Also can throw socketException if sending fails (and maybe some other exceptions
        //depending on content, I guess).
        private void RespondToSearch(byte[] packet)
        {
            COMM_PREFIX prefix = (COMM_PREFIX)packet[0];
            if (prefix != COMM_PREFIX.COMMC_SEARCH_ALBUMS && prefix != COMM_PREFIX.COMMC_SEARCH_SONGS)
                throw new ArgumentException("Packet not of COMMC_SEARCH_... type.");
            byte placesToSearch = (byte)(packet[1] & 0x0F);
            byte sortOrder = (byte)( (packet[1] >> 4) & 0x0F);
            byte searchQueryLength = Math.Min(packet[2],(byte)240);
            int startingIndex = BitConverter.ToInt32(packet, 4);
            //string query = BitConverter.ToString(packet, 16, searchQueryLength);
            string query = Encoding.UTF8.GetString(packet, 16, searchQueryLength);
            List<uint> foundIds;
            if (prefix == COMM_PREFIX.COMMC_SEARCH_SONGS)
                foundIds = Program.FindSongIds(query, placesToSearch, sortOrder);
            else
                foundIds = Program.FindAlbumIds(query, placesToSearch, sortOrder);
            uint totalMatches = (uint)foundIds.Count;
            uint passedMatches = 0;

            //constructing the answer
            byte[] answer = new byte[256];
            for (int i = startingIndex; i < totalMatches && passedMatches < 60; i++, passedMatches++)
                Array.Copy(BitConverter.GetBytes(foundIds[i]), 0, answer, 16 + passedMatches * 4, 4);
            answer[12] = (byte)passedMatches;
            Array.Copy(BitConverter.GetBytes(startingIndex), 0, answer, 4, 4);
            Array.Copy(BitConverter.GetBytes(totalMatches), 0, answer, 8, 4);
            if (prefix == COMM_PREFIX.COMMC_SEARCH_SONGS)
                answer[0] = (byte)COMM_PREFIX.COMMS_SONG_LIST;
            else
                answer[0] = (byte)COMM_PREFIX.COMMS_ALBUM_LIST;

            //sending the answer
            commSocket.Send(answer);
        }
        private void RespondToInfo(byte[] packet)
        {
            COMM_PREFIX prefix = (COMM_PREFIX)packet[0];
            if(prefix != COMM_PREFIX.COMMC_INFO_ALBUM && prefix != COMM_PREFIX.COMMC_INFO_SONG)
                throw new ArgumentException("Packet not of COMMC_INFO_... type.");
            uint wantedID = BitConverter.ToUInt32(packet, 4);
            bool doesExist = true;
            ushort totalLength = 0;
            uint associatedId = 0;
            string name = "", artist = "";
            byte rating = 0, trackNum = 0;
            if(prefix == COMM_PREFIX.COMMC_INFO_ALBUM)
            {
                Album wantedAlbum = Program.GetAlbum(wantedID);
                if(wantedAlbum!=null)
                {
                    totalLength = (ushort) wantedAlbum.FormalLength;
                    name = wantedAlbum.Name;
                    artist = wantedAlbum.Artist;
                    associatedId = wantedAlbum.Id + Program.ALBUM_PLAYLISTS_OFFSET;
                    rating = Rating.RatingConverter(Program.GetAlbumRating(wantedID));
                    trackNum = (byte)wantedAlbum.AlbumTrackCount;
                }
                else
                    doesExist = false;
            }
            else
            {
                Song wantedSong = Program.GetSong(wantedID);
                if (wantedSong != null)
                {
                    totalLength = (ushort)wantedSong.FormalLength;
                    name = wantedSong.Name;
                    artist = wantedSong.Artist;
                    associatedId = wantedSong.AlbumId;
                    rating = Rating.RatingConverter(Program.GetSongRating(wantedID));
                    trackNum = (byte)wantedSong.TrackNumber;
                }
                else
                    doesExist = false;
            }

            //constructing the answer
            byte[] answer = new byte[256];
            if (prefix == COMM_PREFIX.COMMC_INFO_ALBUM)
                answer[0] = (byte)COMM_PREFIX.COMMS_ALBUM_INFO;
            else
                answer[0] = (byte)COMM_PREFIX.COMMS_SONG_INFO;
            if (doesExist)
            {
                answer[1] = 0;
                Array.Copy(BitConverter.GetBytes(wantedID), 0, answer, 2, 4);
                Array.Copy(BitConverter.GetBytes(associatedId), 0, answer, 6, 4);
                Array.Copy(BitConverter.GetBytes(totalLength), 0, answer, 10, 2);
                answer[12] = (byte)Math.Min(240, Encoding.UTF8.GetBytes(name).Length);
                answer[13] = (byte)Math.Min(240-answer[12], Encoding.UTF8.GetBytes(artist).Length);
                answer[14] = trackNum;
                answer[15] = rating;
                byte[] totalStringAsBytes = Encoding.UTF8.GetBytes(String.Join("", name, artist).PadRight(240));
                Array.Copy(totalStringAsBytes, 0, answer, 16, 240);
            }
            else
                answer[1] = 1;

            //sending the answer
            commSocket.Send(answer);
        }
        private void RespondToPlaylist(byte[] packet)
        {
            COMM_PREFIX prefix = (COMM_PREFIX)packet[0];
            if ( prefix!=COMM_PREFIX.COMMC_PLAYLIST_GETLIST && prefix!=COMM_PREFIX.COMMC_PLAYLIST_LOAD
                && prefix!=COMM_PREFIX.COMMC_PLAYLIST_SAVE && prefix!=COMM_PREFIX.COMMC_PLAYLIST_DELETE)
                throw new ArgumentException("Packet not of COMMC_PLAYLIST_... type.");
            byte[] answer = new byte[256];
            switch(prefix)
            {
                case COMM_PREFIX.COMMC_PLAYLIST_GETLIST:
                    {
                        uint userId = connectedUser;
                        List<uint> associatedIds = Program.FindPlaylistsOfUser(userId);
                        byte playlistAmount = (byte)Math.Min(associatedIds.Count, 60);

                        //constructing an answer
                        answer[0] = (byte)COMM_PREFIX.COMMS_PLAYLIST_LIST;
                        Array.Copy(BitConverter.GetBytes(userId), 0, answer, 1, 4);
                        answer[5] = playlistAmount;
                        for (int i = 0; i < playlistAmount; i++)
                            Array.Copy(BitConverter.GetBytes(associatedIds[i]), 0, answer, 16 + 4 * i, 4);
                        break;
                    }
                case COMM_PREFIX.COMMC_PLAYLIST_LOAD:
                    {
                        uint wantedId = BitConverter.ToUInt32(packet, 1);
                        Playlist foundPlaylist = Program.GetPlaylist(wantedId);
                        if( !foundPlaylist.Equals(new Playlist()) )
                        {
                            //checking for readin permissions
                            if (foundPlaylist.Owner == connectedUser || foundPlaylist.Owner == 0) 
                            {
                                uint ownerId = foundPlaylist.Owner;
                                ushort totalLength = (ushort)foundPlaylist.Length;
                                byte[] nameBytes = Encoding.UTF8.GetBytes(foundPlaylist.Name.PadRight(52));
                                byte amountOfTracks = (byte)Math.Min(foundPlaylist.Songs.Count, 48);

                                //composing an answer
                                answer[0] = (byte)COMM_PREFIX.COMMS_PLAYLIST_SONGS;
                                Array.Copy(BitConverter.GetBytes(wantedId), 0, answer, 1, 4);
                                Array.Copy(BitConverter.GetBytes(ownerId), 0, answer, 5, 4);
                                answer[9] = amountOfTracks;
                                Array.Copy(BitConverter.GetBytes(totalLength), 0, answer, 10, 2);
                                Array.Copy(nameBytes, 0, answer, 12, 52);
                                for (int i = 0; i < amountOfTracks; i++)
                                    Array.Copy(BitConverter.GetBytes(foundPlaylist.Songs[i]), 0, answer, 64 + 4 * i, 4);
                            }
                            else    //user don't have permissions to read that playlist
                            {
                                answer[0] = (byte)COMM_PREFIX.COMMS_PLAYLIST_RESP_FAILED;
                                answer[1] = (byte)prefix;
                                Array.Copy(BitConverter.GetBytes(wantedId), 0, answer, 2, 4);
                                answer[6] = 2;
                            }
                        }
                        else    //empty playlist (not found)
                        {
                            answer[0] = (byte)COMM_PREFIX.COMMS_PLAYLIST_RESP_FAILED;
                            answer[1] = (byte)prefix;
                            Array.Copy(BitConverter.GetBytes(wantedId), 0, answer, 2, 4);
                            answer[6] = 1;
                        }
                        break;
                    }
                case COMM_PREFIX.COMMC_PLAYLIST_SAVE:
                    {
                        DateTime creationTime = new DateTime(BitConverter.ToInt64(packet, 1));
                        byte trackAmount = Math.Min(packet[9], (byte)48);
                        string name = Encoding.UTF8.GetString(packet, 12, 52);
                        List<uint> songIds = new List<uint> { };
                        for (int i = 0; i < trackAmount; i++)
                            songIds.Add(BitConverter.ToUInt32(packet, 64 + 4 * i));
                        ushort totalLength = BitConverter.ToUInt16(packet, 10);
                        Playlist nPlaylist = new Playlist(0, connectedUser, totalLength, songIds);
                        nPlaylist.SetName(name);
                        nPlaylist.SetCreationTime(creationTime);
                        if (Program.FindPlaylistsOfUser(connectedUser).Count < Program.PLAYLISTS_PER_USER_LIMIT)
                        {
                            uint result = Program.SavePlaylist(nPlaylist);
                            if (result != 0)
                            {
                                //on success
                                answer[0] = (byte)COMM_PREFIX.COMMS_PLAYLIST_RESP_SUCCESS;
                                answer[1] = (byte)prefix;
                                Array.Copy(BitConverter.GetBytes(result), 0, answer, 2, 4);
                            }
                            else
                            {
                                //on fail to save
                                answer[0] = (byte)COMM_PREFIX.COMMS_PLAYLIST_RESP_FAILED;
                                answer[1] = (byte)prefix;
                                Array.Copy(BitConverter.GetBytes((uint)0), 0, answer, 2, 4);
                                answer[6] = 2;
                            }
                        }
                        else
                        {
                            //too many playlists
                            answer[0] = (byte)COMM_PREFIX.COMMS_PLAYLIST_RESP_FAILED;
                            answer[1] = (byte)prefix;
                            Array.Copy(BitConverter.GetBytes((uint)0), 0, answer, 2, 4);
                            answer[6] = 1;
                        }
                        break;
                    }
                case COMM_PREFIX.COMMC_PLAYLIST_DELETE:
                    {
                        uint wantedId = BitConverter.ToUInt32(packet, 1);
                        Playlist foundPlaylist = Program.GetPlaylist(wantedId);
                        if (!foundPlaylist.Equals(new Playlist()))
                        {
                            //checking for readin permissions
                            if (foundPlaylist.Owner == connectedUser)
                            {
                                bool result = Program.DeletePlaylist(wantedId);
                                if(result)
                                {
                                    //on success
                                    answer[0] = (byte)COMM_PREFIX.COMMS_PLAYLIST_RESP_SUCCESS;
                                    answer[1] = (byte)prefix;
                                    Array.Copy(BitConverter.GetBytes(wantedId), 0, answer, 2, 4);
                                }
                                else
                                {
                                    //on fail
                                    answer[0] = (byte)COMM_PREFIX.COMMS_PLAYLIST_RESP_FAILED;
                                    answer[1] = (byte)prefix;
                                    Array.Copy(BitConverter.GetBytes(wantedId), 0, answer, 2, 4);
                                    answer[6] = 0;
                                }
                            }
                            else    //user don't have permissions to read that playlist
                            {
                                answer[0] = (byte)COMM_PREFIX.COMMS_PLAYLIST_RESP_FAILED;
                                answer[1] = (byte)prefix;
                                Array.Copy(BitConverter.GetBytes(wantedId), 0, answer, 2, 4);
                                answer[6] = 2;
                            }
                        }
                        else            //empty playlist (not found)
                        {
                            answer[0] = (byte)COMM_PREFIX.COMMS_PLAYLIST_RESP_FAILED;
                            answer[1] = (byte)prefix;
                            Array.Copy(BitConverter.GetBytes(wantedId), 0, answer, 2, 4);
                            answer[6] = 1;
                        }
                        break;
                    }
            }

            //sending the answer
            commSocket.Send(answer);
        }
        private void RespondToCoverart(byte[] packet)
        {
            COMM_PREFIX prefix = (COMM_PREFIX)packet[0];
            if (prefix != COMM_PREFIX.COMMC_COVERART_REQUEST)
                throw new ArgumentException("Packet not of COMMC_COVERART_REQUEST type.");

            uint id = BitConverter.ToUInt32(packet, 1);
            uint offset = BitConverter.ToUInt32(packet, 5);
            string coverpath = null;
            if (Program.GetAlbum(id) != null)
                coverpath = Program.GetAlbum(id).CoverArtPath;
            if(coverpath == null || coverpath == "")
            {
                byte[] answer = new byte[256];
                answer[0] = (byte)COMM_PREFIX.COMMS_COVERART_NONE;
                Array.Copy(BitConverter.GetBytes(id), 0, answer, 1, 4);
            }
            else
            {
                byte[] answer = new byte[32*1024];
                answer[0] = (byte)COMM_PREFIX.COMMS_COVERART_STREAM;
                Array.Copy(BitConverter.GetBytes(id), 0, answer, 1, 4);
                Array.Copy(BitConverter.GetBytes(offset), 0, answer, 9, 4);
                try
                {
                    FileStream fs = File.Open(coverpath, FileMode.Open, FileAccess.Read, FileShare.Read);
                    uint totalLength = (uint)fs.Length;
                    Array.Copy(BitConverter.GetBytes(totalLength), 0, answer, 5, 4);
                    if(totalLength > offset)
                    {
                        uint bytesToRead = Math.Min( totalLength - offset, 32755);
                        fs.Position = offset;
                        fs.Read(answer, 13, (int)bytesToRead);
                    }
                    fs.Close();
                }
                catch (Exception e) { throw e; }

                commSocket.Send(answer);
            }
        }
        private void RespondToSongRequest(byte[] packet)
        {
            COMM_PREFIX prefix = (COMM_PREFIX)packet[0];
            if (prefix != COMM_PREFIX.COMMC_SONG_REQUEST)
                throw new ArgumentException("Packet not of COMMC_SONG_REQUEST type.");

            uint id = BitConverter.ToUInt32(packet, 1);
            uint startIndex = BitConverter.ToUInt32(packet, 5);
            Song song = Program.GetSong(id);
            if(song!=null)
            {
                if(song.Loaded == false)
                {
                    if(!song.Loading)
                    {
                        try
                        {
                            //song.LoadSong();
                            song.StartLoadingTask();
                            Task.Delay(100);
                            while (!song.FirstSectionAvailable())
                                Task.Delay(200);
                        }
                        catch (Exception exep) { throw new Exception("Song loading failure", exep); }
                    }
                    else
                    {
                        Task.Delay(200);
                        while (!song.FirstSectionAvailable())
                            Task.Delay(200);
                    }
                }
                song.BumpRequestTime();

                //composing an answer
                byte[] answer = new byte[32 * 1034];
                answer[0] = (byte)COMM_PREFIX.COMMS_SONG_STREAM;
                Array.Copy(BitConverter.GetBytes(song.Id), 0, answer, 1, 4);
                Array.Copy(BitConverter.GetBytes((ushort)song.SamplingRate), 0, answer, 5, 2);
                answer[7] = (byte)song.BitDepth;
                answer[8] = 2;
                Array.Copy(BitConverter.GetBytes((uint)song.SectionCount), 0, answer, 9, 4);
                Array.Copy(BitConverter.GetBytes(startIndex), 0, answer, 13, 4);
                uint sectionsSent = (startIndex < song.SectionCount) ? (uint)(song.SectionCount-startIndex) : 0;
                sectionsSent = Math.Min(sectionsSent, 32736 / Song.BYTES_PER_SECTION);
                Array.Copy(BitConverter.GetBytes(sectionsSent), 0, answer, 17, 4);
                Array.Copy(BitConverter.GetBytes(Song.BYTES_PER_SECTION), 0, answer, 21, 4);
                byte[] dataSection = new byte[0];
                //bool loadedSection = false;
                for(int iS = 0; iS < sectionsSent; iS++)
                {
                    dataSection = song.RequestSection(startIndex + (uint)iS);
                    Array.Copy(dataSection, 0, answer, 32 + iS * Song.BYTES_PER_SECTION, Song.BYTES_PER_SECTION);
                }

                //sending the answer
                commSocket.Send(answer);
            }
            else
            {

            }
        }
        private void RespondToRating(byte[] packet)
        {
            COMM_PREFIX prefix = (COMM_PREFIX)packet[0];
            if (prefix != COMM_PREFIX.COMMC_SONG_RATE)
                throw new ArgumentException("Packet not of COMMC_SONG_RATE type.");

            uint songId = BitConverter.ToUInt32(packet, 1);
            byte rating = packet[5];
            byte[] answer = new byte[256];

            if (!Rating.IsValid(rating))
            {
                //rating invalid
                answer[0] = (byte)COMM_PREFIX.COMMS_SONG_RATE_FAIL;
                Array.Copy(BitConverter.GetBytes(songId), 0, answer, 1, 4);
                answer[5] = 3;
            }
            else if(Program.GetSong(songId) == null)
            {
                //song not found
                answer[0] = (byte)COMM_PREFIX.COMMS_SONG_RATE_FAIL;
                Array.Copy(BitConverter.GetBytes(songId), 0, answer, 1, 4);
                answer[5] = 1;
            }
            else
            {
                Rating nRating = new Rating(connectedUser, songId, rating);
                bool canRate = Program.RateSong(nRating);
                if(canRate)
                {
                    answer[0] = (byte)COMM_PREFIX.COMMS_SONG_RATE_SUCCESS;
                    Array.Copy(BitConverter.GetBytes(songId), 0, answer, 1, 4);
                    answer[5] = Rating.RatingConverter(Program.GetSongRating(songId));
                }
                else
                {   //can't rate, already rated
                    answer[0] = (byte)COMM_PREFIX.COMMS_SONG_RATE_FAIL;
                    Array.Copy(BitConverter.GetBytes(songId), 0, answer, 1, 4);
                    answer[5] = 2;
                }
            }

            //sending the answer
            commSocket.Send(answer);
        }

        //Empty function for reacting to communication failures...
        private void CommFailureReact()
        {
            //maybe add here something in the future
        }

        //Just cut out routines from CommunicateWithClient() to tidy up
        //They take sDataBuffer, obviously (previously read 256 bytes long packet)
        //Login routine returns the new communicationEstablished value
        //Also, can throw ArgumentException if wrong packet type, just in case
        private bool LoginRoutine(byte[] sDataBuffer)
        {
            if ((COMM_PREFIX)sDataBuffer[0] != COMM_PREFIX.COMMC_LOGIN_TRY)
                throw new ArgumentException("Packet not of COMMC_LOGIN_TRY type.");
            bool commEstablished = false;

            byte[] nameArr = new byte[20];
            Array.Copy(sDataBuffer, 4, nameArr, 0, 20);
            string username = Encoding.ASCII.GetString(nameArr);
            uint? id = Program.GetUserId(username);
            if (id == null)
            {
                //user with that username doesn't exist
                sDataBuffer.Initialize();
                sDataBuffer[0] = (byte)COMM_PREFIX.COMMS_LOGIN_FAILED_USERNAME;
                commSocket.Send(sDataBuffer, SocketFlags.None);
            }
            else
            {
                //checking the password
                byte[] pswdHash = new byte[32];
                Array.Copy(sDataBuffer, 32, pswdHash, 0, 32);
                byte[] localHash = Program.GetPasswordHash(username);
                if (Enumerable.SequenceEqual(pswdHash, localHash))
                {
                    //checking if user is logged in
                    bool loggedIn = Program.TryUserLogin(id ?? 0, pswdHash);
                    if (loggedIn)
                    {
                        //logged in successfully
                        commEstablished = true;
                        connectedUser = id ?? 0;
                        Console.WriteLine("User {0} logged in.", username);
                        sDataBuffer.Initialize();
                        sDataBuffer[0] = (byte)COMM_PREFIX.COMMS_LOGIN_SUCCESS;
                        Array.Copy(BitConverter.GetBytes(id ?? 0), 0, sDataBuffer, 1, 4);
                        commSocket.Send(sDataBuffer, SocketFlags.None);
                    }
                    else
                    {
                        //user alredy online
                        sDataBuffer.Initialize();
                        sDataBuffer[0] = (byte)COMM_PREFIX.COMMS_LOGIN_FAILED_INUSE;
                        commSocket.Send(sDataBuffer, SocketFlags.None);
                    }
                }
                else
                {
                    //wrong password
                    sDataBuffer.Initialize();
                    sDataBuffer[0] = (byte)COMM_PREFIX.COMMS_LOGIN_FAILED_PASSWORD;
                    commSocket.Send(sDataBuffer, SocketFlags.None);
                }
            }

            return commEstablished;
        }
        private void RegisterRoutine(byte[] sDataBuffer)
        {
            if((COMM_PREFIX)sDataBuffer[0]!=COMM_PREFIX.COMMC_REGISTER_TRY)
                throw new ArgumentException("Packet not of COMMC_REGISTER_TRY type.");

            //extracting the content of message
            byte[] nameArr = new byte[20];
            Array.Copy(sDataBuffer, 4, nameArr, 0, 20);
            string username = Encoding.ASCII.GetString(nameArr);
            byte emailLeng = Math.Min(sDataBuffer[3], (byte)192);
            byte[] emailArr = new byte[emailLeng];
            Array.Copy(sDataBuffer, 64, emailArr, 0, emailLeng);
            string email = Encoding.UTF8.GetString(emailArr);
            byte[] pswdHash = new byte[32];
            Array.Copy(sDataBuffer, 32, pswdHash, 0, 32);

            //registering (if can) and answering
            uint? id = Program.GetUserId(username);
            bool emailTaken = Program.IsEmailTaken(email);
            if (emailTaken || id != null)
            {
                sDataBuffer.Initialize();
                sDataBuffer[0] = (byte)COMM_PREFIX.COMMS_REGISTER_FAILED_TAKEN;
                byte takenByte = emailTaken ? (byte)2 : (byte)0;
                if (id != null)
                    takenByte += 1;
                sDataBuffer[1] = takenByte;
                commSocket.Send(sDataBuffer, SocketFlags.None);
            }
            else
            {
                sDataBuffer.Initialize();
                id = Program.RegisterUser(username, email, pswdHash);
                sDataBuffer[0] = (byte)COMM_PREFIX.COMMS_REGISTER_SUCCESS;
                Array.Copy(BitConverter.GetBytes(id ?? 0), 0, sDataBuffer, 1, 4);
                commSocket.Send(sDataBuffer, SocketFlags.None);
            }
        }

        /// <summary>
        /// Used in event handlers. Sends simple packet with only the prefix if
        /// id matches the one assigned to communicate handler.
        /// </summary>
        /// <param name="id">Logged in and assigned to that object user ID.</param>
        /// <param name="pref">Type prefix of message.</param>
        private void ConditionalSendShortComm(uint id, COMM_PREFIX pref)
        {
            if (id == connectedUser)
            {
                try
                {
                    byte[] msg = new byte[256];
                    msg[0] = (byte)pref;
                    commSocket.Send(msg);
                }
                catch (Exception) { }
            }
        }
    }
}
