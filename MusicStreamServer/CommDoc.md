﻿# Communication protocol  
The communication between client and server is based on 
data packets sent via sockets on port **7200**.
Each packet begins with a byte that indicates type of data
it provides and its structure. 
Two lengths of packets are sent:  
* **short** - 256 bytes long - standard one, used in 
most cases and is used to begin the communication, used
mainly for delivering communicates, requests and ID lists
* **long** - 32 KiB long (32 768 bytes) - used only for
streaming, where it contains bigger chunks of data, send
only by server upon a request from client

#### Packet type prefixes
Prefix | Name | Length | Origin
--- | --- | --- | ---
0x00 | COMMS_LOGIN_FAILED_UNKNOWN | short | server
0x01 | COMMS_LOGIN_FAILED_PASSWORD | short | server
0x02 | COMMS_LOGIN_FAILED_USERNAME | short | server
0x03 | COMMS_LOGIN_FAILED_INUSE | short | server
0x04 | COMMS_LOGIN_SUCCESS | short | server
0x05 | COMMS_REGISTER_SUCCESS | short | server
0x06 | COMMS_REGISTER_FAILED_TAKEN | short | server
0x07 | COMMS_REGISTER_FAILED_OTHER | short | server
0x08 | COMMS_LOGOUT | short | server
0x10 | COMMS_REACT_ISACTIVE | short | server
0x20 | COMMS_ALBUM_LIST | short | server
0x21 | COMMS_SONG_LIST | short | server
0x22 | COMMS_PLAYLIST_LIST | short | server
0x23 | COMMS_PLAYLIST_SONGS | short | server
0x30 | COMMS_PLAYLIST_RESP_FAILED | short | server
0x31 | COMMS_PLAYLIST_RESP_SUCCESS | short | server
0x40 | COMMS_ALBUM_INFO | short | server
0x41 | COMMS_SONG_INFO | short | server
0x50 | COMMS_SONG_RATE_FAIL | short | server
0x51 | COMMS_SONG_RATE_SUCCESS | short | server
0x60 | COMMS_SONG_STREAM | long | server
0x64 | COMMS_COVERART_STREAM | long | server
0x70 | COMMS_SONG_NOTFOUND | short | server
0x74 | COMMS_COVERART_NONE | short | server
0x80 | COMMC_LOGIN_TRY | short | client
0x85 | COMMC_REGISTER_TRY | short | client
0x88 | COMMC_LOGOUT_REQ | short | client
0x90 | COMMC_REACT_ACTIVE | short | client
0xA0 | COMMC_SEARCH_ALBUMS | short | client
0xA1 | COMMC_SEARCH_SONGS | short | client
0xB0 | COMMC_INFO_ALBUM | short | client
0xB1 | COMMC_INFO_SONG | short | client
0xC0 | COMMC_PLAYLIST_GETLIST | short | client
0xC1 | COMMC_PLAYLIST_LOAD | short | client
0xC2 | COMMC_PLAYLIST_SAVE | short | client
0xC3 | COMMC_PLAYLIST_DELETE | short | client
0xD0 | COMMC_SONG_RATE | short | client
0xE0 | COMMC_SONG_REQUEST | short | client
0xE4 | COMMC_COVERART_REQUEST | short | client

#### Data types<a name="dataTypes"></a>
Unless specified otherwise, all numeric data is unsigned
integer of length according to number of bytes it takes
represents in packet - for example every ID takes 4 bytes,
thus type of IDs is unsigned 32-bit integer.

All album names, song names, playlist names and artist names
are UTF-8 encoded strings of characters. 
Usernames are ASCII encoded string of characters maximally
20 characters long.

###### Ratings custom fixed point type value
Ratings are custom-type numeric data. 
They are unsigned 8-bit numeric
value with fixed decimal point. Value of each bit are:

Bit position | 128s | 64s | 32s | 16s| 8s | 4s | 2s | 1s
--- | --- | --- |
Value | 4 | 2 | 1 | 0.5 | 0.25 | 0.125 | 0.0625 | 0.03125 

So, for example:  
```
[0b10010011] has value of:
1*4 + 0*2 + 0*1 + 1*0.5 + 0*0.25 + 0*0.125 + 1*0.0625 + 1*0.03125 =
= 4 + 0.5 + 0.0625 + 0.03125 = 4.59375
```
Suggested routine for conversion is reading these 8 bits
as unsigned 8-bit integer, dividing it by 32 and saving
the result to a floating-point type.  
Additionaly to rules above, ratings which values are neither
in range from 1 to 5 (inclusive) nor equal 0 should be 
treated as incorrect.

#### Request-answer pairs

Request packet type | Possible answer packet types
--- | ---
COMMC_LOGIN_TRY | COMMS_LOGIN_FAILED_UNKNOWN, COMMS_LOGIN_FAILED_PASSWORD, COMMS_LOGIN_FAILED_USERNAME, COMMS_LOGIN_FAILED_INUSE, COMMS_LOGIN_SUCCESS
COMMC_REGISTER_TRY | COMMS_REGISTER_SUCCESS, COMMS_REGISTER_FAILED_TAKNEN, COMMS_REGISTER_FAILED_OTHER
COMMC_LOGOUT_REQ | COMMS_LOGOUT
COMMS_REACT_ISACTIVE<sup>1</sup> | COMMC_REACT_ACTIVE
COMMC_SEARCH_ALBUMS | COMMS_ALBUM_LIST
COMMC_SEARCH_SONGS | COMMS_SONG_LIST
COMMC_INFO_ALBUM | COMMS_ALBUM_INFO
COMMC_INFO_SONG | COMMS_SONG_INFO
COMMC_PLAYLIST_GETLIST | COMMS_PLAYLIST_LIST, COMMS_PLAYLIST_RESP_FAILED
COMMC_PLAYLIST_LOAD | COMMS_PLAYLIST_SONGS, COMMS_PLAYLIST_RESP_FAILED
COMMC_PLAYLIST_SAVE | COMMS_PLAYLIST_RESP_SUCCESS, COMMS_PLAYLIST_RESP_FAILED_
COMMC_PLAYLIST_DELETE | COMMS_PLAYLIST_RESP_SUCCESS, COMMS_PLAYLIST_RESP_FAILED
COMMC_SONG_RATE | COMMS_SONG_RATE_SUCCESS, COMMS_SONG_RATE_FAIL
COMMC_SONG_REQUEST | COMMS_SONG_STREAM, COMMS_SONG_NOTFOUND
COMMC_COVERART_REQUEST | COMMS_COVEART_STREAM, COMMS_COVERART_NONE

<sup>1</sup>**0x10 - COMMS_REACT_ISACTIVE** is the only request sent by the server.

### Logging in packets  
###### 0x00 - COMMS_LOGIN_FAILED_UNKNOWN  
A communicate that logging in failed from unspecified reasons.

| Byte(s) | Content |  
| --- | :--- |
0 | Packet type - value **0x00**
1-255 | Not used

###### 0x01 - COMMS_LOGIN_FAILED_PASSWORD
A communicate that logging in failed because 
provided password is incorrect for given username.

| Byte(s) | Content |  
| --- | :--- |
0 | Packet type - value **0x01**
1-255 | Not used

###### 0x02 - COMMS_LOGIN_FAILED_USERNAME
A communicate that logging in failed because 
there's no user with given username.

| Byte(s) | Content |  
| --- | :--- |
0 | Packet type - value **0x02**
1-255 | Not used

###### 0x03 - COMMS_LOGIN_FAILED_INUSE
A communicate that logging in failed because 
somebody is currently using account with given
username.  
It's adviced that this communicate should be followed by request 
**COMMS_REACT_ISACTIVE** sent to that logged in user
for checking if it's active and eventually logging it out.

| Byte(s) | Content |  
| --- | :--- |
0 | Packet type - value **0x03**
1-255 | Not used

###### 0x04 - COMMS_LOGIN_SUCCESS
A communicate that logging in was successful.

| Byte(s) | Content |  
| --- | :--- |
0 | Packet type - value **0x04**
1-4 | Unique user ID representing logged in user
5-255 | Not used

###### 0x08 - COMMS_LOGOUT
A communicate that user has been logged out by the server.

| Byte(s) | Content |  
| --- | :--- |
0 | Packet type - value **0x08**
1-255 | Not used

###### 0x80 - COMMC_LOGIN_TRY
A request to log in.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x80**
1-3 | Not used
4 - 23 | Username, space-padded on right to size of 20 <sup>1</sup>
24-29 | Not used
32-63 | **SHA256** hash value of password
64-255 | Not used

<sup>1</sup>username must be max 20 ASCII characters long

###### 0x88 - COMMC_LOGOUT_REQ
A request to log out.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x88**
1-255 | Not used

### Registering packets

###### 0x05 - COMMS_REGISTER_SUCCESS
A communicate that registering was successful.

| Byte(s) | Content |  
| --- | :--- |
0 | Packet type - value **0x05**
1-4 | Unique user ID representing registered user
5-255 | Not used

###### 0x06 - COMMS_REGISTER_FAILED_TAKEN
A communicate that registering has failed because
username or e-mail is already used by other user.

| Byte(s) | Content |  
| --- | :--- |
0 | Packet type - value **0x06**
1 | **1** if only username is already used, **2** if only e-mail adress is used, **3** if both are already used
2-255 | Not used

###### 0x07 - COMMS_REGISTER_FAILED_OTHER
A communicate that registering has failed but both
username and e-mail are free to use.

| Byte(s) | Content |  
| --- | :--- |
0 | Packet type - value **0x07**
1-255 | Not used

###### 0x85 - COMMC_REGISTER_TRY
A request to register.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x85**
1-2 | Not used
3 | E-mail adress length (in bytes, not characters)
4 - 23 | Username, space-padded on right to size of 20 <sup>1</sup>
24-29 | Not used
32-63 | **SHA256** hash value of password
64-255 | E-mail adress

<sup>1</sup>username must be max 20 ASCII characters long


### Activity packets

###### 0x10 - COMMS_REACT_ISACTIVE
A request to confirm activity of user.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x10**
1-255 | Not used

###### 0x90 - COMMC_REACT_ACTIVE
Answer confirming activity of user.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x90**
1-4 | User ID
5-255 | Not used


### Searching and info packets

###### 0x20 - COMMS_ALBUM_LIST
List of album IDs that matches criteria 
in previous search request.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x20**
1-3 | Not used
4-7 | Starting index (how many albums were skipped)<sup>1</sup>
8-11 | Total amount of albums matching the criteria
12 | Length of returned list
13-15 | Not used
16-256 | List of album IDs (each 4 bytes long - max 60 values)

<sup>1</sup> because list can be max 60 IDs long, 
user must decide in search request which fragment 
of sorted list of albums matching criteria he wants to
receive by choosing the starting index

###### 0x21 - COMMS_SONG_LIST
List of song IDs that matches criteria 
in previous search request.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x21**
1-3 | Not used
4-7 | Starting index (how many songs were skipped)<sup>1</sup>
8-11 | Total amount of songs matching the criteria
12 | Length of returned list
13-15 | Not used
16-256 | List of song IDs (each 4 bytes long - max 60 values)

<sup>1</sup> because list can be max 60 IDs long, 
user must decide in search request which fragment 
of sorted list of songs matching criteria he wants to
receive by choosing the starting index

###### 0x40 - COMMS_ALBUM_INFO
A set of informations about chosen album.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x40**
1 | Non-zero if album of specified ID doesn't exist, **0x00** otherwise
2-5 | Album ID
6-9 | ID of playlist associated with that album's song list
10-11 | Total length of album (in seconds)
12 | Length of album's name (bytes, not characters)
13 | Length of artist's name (bytes, not characters)
14 | Number of songs in album
15 | Mean rating of songs in album (as 8-bit fixed point value - see: <a href="#dataTypes">**Data types**</a>)
16-255 | Album's name and artist's name

###### 0x41 - COMMS_SONG_INFO
A set of informations about chosen song.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x41**
1 | Non-zero if song of specified ID doesn't exist, **0x00** otherwise
2-5 | Song ID
6-9 | Album ID of album this song belongs to
10-11 | Total length of song (in seconds)
12 | Length of song's name (bytes, not characters)
13 | Length of artist's name (bytes, not characters)
14 | Track number in album
15 | Song's rating (as 8-bit fixed point value - see: <a href="#dataTypes">**Data types**</a>)
16-255 | Song's name and artist's name

###### 0xA0 - COMMC_SEARCH_ALBUMS
A request to search albums matching specified criteria.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0xA0**
1 | Places to search and final order (see below the table)
2 | Length of search query (bytes, not characters)
3 | Not used
4-7 | How many albums should be skipped from final list<sup>1</sup>
8-15 | Not used
16 - 255 | Search query

<sup>1</sup> because list can be max 60 IDs long, 
user must decide in search request which fragment 
of sorted list of albums matching criteria he wants to
receive by choosing the starting index

Byte of package at index 1 contains two types of informations.
The 4 younger bits of it specify where to search for search query matches:
* if bit at position of ones is high - search in song names
* if bit at position of twos is high - search in album names
* if bit at position of fours is high - search in artist (of albums) names
* bit at position of eights is not used

The 4 older bits specify the order in which the list of matching
albums should be sorted. If bits at position of 
thirty-twos and sixteens are accordingly:
* low and low (0bxx00xxxxxx) sort by album name
* low and high (0bxx01xxxxxx) sort by album artist name
* high and low (0bxx10xxxxxx) sort by length of albums
* high and high (0bxx11xxxxxx) sort by mean rating of songs in album

(where x means any value of bit)  
Additionaly, if bit at position of one-hudred-eigths 
is high - sort albums in reversed order. 
Bit at position of sixty-fours is not used.  
It's advised that default value of byte 
at position 1 should be **0x0F**.

###### 0xA1 - COMMC_SEARCH_SONGS
A request to search songs matching specified criteria.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0xA1**
1 | Places to search and final order (see below the table)
2 | Length of search query (bytes, not characters)
3 | Not used
4-7 | How many songs should be skipped from final list<sup>1</sup>
8-15 | Not used
16 - 255 | Search query

<sup>1</sup> because list can be max 60 IDs long, 
user must decide in search request which fragment 
of sorted list of songs matching criteria he wants to
receive by choosing the starting index

Byte of package at index 1 contains two types of informations.
The 4 younger bits of it specify where to search for search query matches:
* if bit at position of ones is high - search in song names
* if bit at position of twos is high - search in album names
* if bit at position of fours is high - search in artist (of songs) names
* bit at position of eights is not used

The 4 older bits specify the order in which the list of matching
songs should be sorted. If bits at position of 
thirty-twos and sixteens are accordingly:
* low and low (0bxx00xxxxxx) sort by song name
* low and high (0bxx01xxxxxx) sort by song artist name
* high and low (0bxx10xxxxxx) sort by length of songs
* high and high (0bxx11xxxxxx) sort by rating of songs

(where x means any value of bit)  
Additionaly, if bit at position of one-hudred-eigths 
is high - sort songs in reversed order. 
Bit at position of sixty-fours is not used.  
It's advised that default value of byte 
at position 1 should be **0x0F**.

###### 0xB0 - COMMC_INFO_ALBUM
A request to provide information about the album.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0xB0**
1-3 | Not used
4-7 | Album ID
8-255 | Not used

###### 0xB1 - COMMC_INFO_SONG
A request to provide information about the song.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0xB1**
1-3 | Not used
4-7 | Song ID
8-255 | Not used

### Song rating packets

###### 0x50 - COMMS_SONG_RATE_FAIL
A communicate that rating a song has failed.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x50**
1-4 | Song ID
5 | Reason of failure (see below the table)
6-255 | Not used

Byte at position 1 contains the ID of failure reason.
These are:
* 0x00 - Unknown or unspecified reason
* 0x01 - Song with given ID doesn't exist
* 0x02 - Song has been already rated
* 0x03 - Rating incorrect

###### 0x51 - COMMS_SONG_RATE_SUCCESS
A communicate that rating a song was successful.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x51**
1-4 | Song ID
5 | Updated song rating (as 8-bit fixed point value - see: <a href="#dataTypes">**Data types**</a>)
6-255 | Not used

###### 0xD0 - COMMC_SONG_RATE
A request to rate a song.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0xD0**
1-4 | Song ID
5 | Selected song rating (as 8-bit fixed point value - see: <a href="#dataTypes">**Data types**</a>)
6-255 | Not used

### Playlist related packets

###### 0x22 - COMMS_PLAYLIST_LIST
A list of IDs of playlists owned by specific user.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x22**
1-4 | User ID
5 | Amount of playlists sent (max 60)
6-7 | Total amount of playlists owned by user
8-9 | How many playlists of that user were skipped
10-15 | Not used
16-255 | List of playlist IDs (each 4 bytes long - max 60 values)

###### 0x23 - COMMS_PLAYLIST_SONGS
A communicate with the content of one of playlist - 
info and list of songs.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x23**
1-4 | Playlist ID
5-8 | Owner ID
9 | Amount of tracks in playlist
10-11 | Total length of playlist (in seconds)
12-63 | Playlist name
64-255 | List of song IDs (each 4 bytes long - max 48 values)

###### 0x30 - COMMS_PLAYLIST_RESP_FAILED
A communicate that playlist-realated operation has failed.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x30**
1 | Failed operation type as its request type
2-5 | Related playlist ID (if assigned, otherwise **0x00**)
6 | Reason of failure (see below)
7-255 | Not used

Failure codes for **0xC0 - COMMC_PLAYLIST_GETLIST** answer:
* 0x00 - reason unknown or unspecified

Failure codes for **0xC1 - COMMC_PLAYLIST_LOAD** answer:
* 0x00 - reason unknown or unspecified
* 0x01 - playlist doesn't exist
* 0x02 - user doesn't have permissions to read that playlist

Failure codes for **0xC2 - COMMC_PLAYLIST_SAVE** answer:
* 0x00 - reason unknown or unspecified
* 0x01 - user has too many playlists
* 0x02 - server failed to save the playlist

Failure codes for **0xC3 - COMMC_PLAYLIST_DELETE** answer:
* 0x00 - reason unknown or unspecified
* 0x01 - playlist doesn't exist
* 0x02 - user doesn't have permissions to delete that playlist


###### 0x31 - COMMS_PLAYLIST_RESP_SUCCESS
A communicate that playlist-realated operation was successful.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x31**
1 | Succeeded operation type as its request type - value **0xC2** or **0xC3**.
2-5 | Related playlist ID
6-255 | Not used

###### 0xC0 - COMMC_PLAYLIST_GETLIST
A request to get IDs of playlists owned by the user.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0xC0**
1-2 | How many lowest IDs of playlist owned by user should be skipped.<sup>1</sup>
3-255 | Not used

<sup>1</sup>Because only 60 playlist IDs can be sent in one packet, 
if user owns more playlists multiple request need to be sent with 
a proper amount of IDs from beginning skipped each time.

###### 0xC1 - COMMC_PLAYLIST_LOAD
A request to get a certain playlist.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0xC1**
1-4 | Playlist ID
5-255 | Not used

###### 0xC2 - COMMC_PLAYLIST_SAVE
A request to save a specified playlist.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0xC2**
1-8 | Creation time (as ticks elapsed since midnight 1 Jan 2001, signed 8 bit integer)
9 | Amount of tracks in playlist
10-11 | Playlist length, in seconds
12-63 | Playlist name
64-255 | List of song IDs (each 4 bytes long - max 48 values)

###### 0xC3 - COMMC_PLAYLIST_DELETE
A request to delete a specified playlist.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0xC3**
1-4 | ID of playlist to delete
5-255 | Not used

### Coverart sending packets

###### 0x64 - COMMS_COVERART_STREAM
A stream of album coverart image data.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x64**
1-4 | Album ID
5-8 | Total length of image data (in bytes)
9-12 | Offset from beginning of image data stream (starting index)
13-32 767 | Stream of PNG image data

###### 0x74 - COMMS_COVERART_NONE
A communicate that either given album doesn't have 
a coverart or album with given ID doesn't exist.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x74**
1-4 | Album ID
5-255 | Not used

###### 0xE4 - COMMC_COVERART_REQUEST
A request to stream album coverart image data.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0xE4**
1-4 | Album ID
5-8 | Offset from beggining of data stream (starting index)

### Music streaming packets

###### 0x60 - COMMS_SONG_STREAM
A stream of song data.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x60**
1-4 | Song ID
5-6 | Sampling rate of song (in Hz)
7 | Bit depth of song
8 | Number of channels
9-12 | Total amount of sections<sup>1</sup>
13-16 | First section index<sup>1</sup>
17-20 | Sections sent in this packet<sup>1</sup>
21-24 | Bytes of data in one section (should be always **CONSTANT**, thus can be omitted)<sup>1</sup>
25-31 | Not used
32-32 767 | Stream of raw music data

<sup>1</sup>Each song stream is streamed in data sections
of specified length - in one respond there can be many
sections sent. First section index indicates the index
of first of them, the following ones have ascending
indices and all sections concated give total music data
stream. The amount of sections read by client
should be based both on number sent in bytes 17-20 and
total received length of packet.
**All packets should have full length.**

###### 0x70 - COMMS_SONG_NOTFOUND
A communicate that song with given ID doesn't exist.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0x70**
1-4 | Song ID
5-255 | Not used

###### 0xE0 - COMMC_SONG_REQUEST
A request to stream song.

| Byte(s) | Content |  
| :-----: | :--- |
0 | Packet type - value **0xE0**
1-4 | Song ID
5-8 | First section index<sup>1</sup>
9-255 | Not used

<sup>1</sup>Each song stream is streamed in data sections
of specified length - in one respond there can be many
sections sent, this number indicates index of the first
of them