﻿using System;
using System.Linq;

namespace MusicStreamServer
{
    public delegate void UserEvent(uint id);

    /// <summary>
    /// Class for objects representing MusicStream users.
    /// </summary>
    public class User
    {
        private uint userId;
        private string username;
        private byte[] password;
        private string emailAdress;
        private bool isLogged;
        private DateTime lastActivity;
        private byte permissions;

        /// <summary>
        /// Event launched when user has been logged out.
        /// </summary>
        /// <see cref="UserEvent"/>
        public event UserEvent userLogoutEvent;
        /// <summary>
        /// Event launched when user last activity time is being checked for timeout
        /// or its being set to value that it'll exceed timeout.
        /// </summary>
        public event UserEvent timeoutCheckEvent;
        
        public uint Id => userId;
        public string Username => username;
        public string Email => emailAdress;
        public bool IsLogged => isLogged;
        internal byte[] PasswordHash => password;

        /// <summary>
        /// Creates an user.
        /// </summary>
        /// <param name="id">User ID</param>
        /// <param name="name">Username</param>
        /// <param name="pswrd">SHA256 password hash</param>
        /// <param name="email">Email adress of user</param>
        /// <param name="perms">Permissions of user</param>
        public User(uint id, string name, byte[] pswrd, string email, byte perms)
        {
            userId = id;
            username = name;
            password = pswrd;
            emailAdress = email;
            permissions = perms;
            isLogged = false;
            lastActivity = DateTime.Now;
        }

        /// <summary>
        /// Tries to log in the user with given password
        /// </summary>
        /// <param name="pswrd">SHA 256 hash of the password provided by client</param>
        /// <returns>True if logged in succesfully</returns>
        public bool TryLogin(byte[] pswrd)
        {
            if(Enumerable.SequenceEqual(pswrd,password) && isLogged == false)
            {
                isLogged = true;
                lastActivity = DateTime.Now;
                return true;
            }
            return false;
        }
        /// <summary>
        /// Updates last activity time of the user to current time.
        /// </summary>
        public void UpdateActivity() { lastActivity = DateTime.Now; }
        /// <summary>
        /// Logs out the user.
        /// </summary>
        public void Logout()
        {
            isLogged = false;
            try { userLogoutEvent(userId); } catch (Exception) { }
        }

        /// <summary>
        /// Checks if user last activity exceeds given timeout.
        /// </summary>
        /// <param name="timeout">Minimal time since last user activity, in seconds</param>
        /// <returns>Whether time since last user activity exceeds given timeout</returns>
        public bool CheckTimeout(uint timeout)
        {
            int timeDiff = (int)Math.Floor(DateTime.Now.Subtract(lastActivity).TotalSeconds);
            if (timeDiff > timeout)
            {
                try { timeoutCheckEvent(userId); } catch (Exception) { }
                return true;
            }
            else
                return false;
        }
        /// <summary>
        /// Sets last activity time of user so it will be at least as
        /// long ago as given timeout.
        /// </summary>
        /// <param name="timeout">Time since last activity to be set, in seconds</param>
        public void ForceTimeout(uint timeout)
        {
            int timeDiff = (int)Math.Floor(DateTime.Now.Subtract(lastActivity).TotalSeconds);
            if (timeDiff > timeout)
                lastActivity = DateTime.Now.AddSeconds(-timeout);
            try { timeoutCheckEvent(userId); } catch (Exception) { }
        }
        /// <summary>
        /// Checks if user last activity time exceeds given timeout and
        /// if so, logs out the user.
        /// </summary>
        /// <param name="timeout">Minimal time since last user activity, in seconds</param>
        /// <returns>True, if logged out the user because of timeout, false otherwise</returns>
        public bool UnactiveLogout(uint timeout)
        {
            int timeDiff = (int) Math.Floor(DateTime.Now.Subtract(lastActivity).TotalSeconds);
            if(timeDiff > timeout)
            {
                isLogged = false;
                try { userLogoutEvent(userId); } catch (Exception) { }
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return "User #" + userId + ": " + username + (isLogged ? " [ONLINE]":" [OFFLINE]");
        }
    }

    /// <summary>
    /// Structure representing the song rating
    /// </summary>
    public struct Rating
    {
        public uint UserId { get; }
        public uint SongId { get; }
        public byte Value { get; } //interpreted as integer gives 32 times of the actual value
        public float TrueValue => (float)Value / 32;

        /// <summary>
        /// Creates the rating
        /// </summary>
        /// <param name="user">User ID of user that rated</param>
        /// <param name="song">Song ID of rated song</param>
        /// <param name="val">Rating in 32-times value fixed point format</param>
        public Rating(uint user, uint song, byte val)
        {
            UserId = user;
            SongId = song;
            Value = val;
        }
        /// <summary>
        /// Converts actual value to 32-times value format.
        /// </summary>
        /// <param name="trueVal">Actual precise value</param>
        /// <returns>32-times value fixed point format</returns>
        public static byte RatingConverter(float trueVal)
        {
            if (trueVal > 5 || trueVal < 1)
                return 0;
            return (byte)(uint)(trueVal * 32);
        }
        /// <summary>
        /// Check whether given rating is valid value (between 1-5 or 0).
        /// </summary>
        /// <param name="rating">Rating in 32-times value fixed point format.</param>
        /// <returns>True if valid.</returns>
        public static bool IsValid(byte rating)
        {
            float tVal = (float)rating / 32;
            if(tVal > 5 || tVal < 1)
            {
                if (tVal != 0)
                    return false;
            }
            return true;
        }
    }
}