﻿using System;

namespace MusicStreamServer
{
    /// <summary>
    /// Communicate prefix enum type.
    /// Prefix defines the type of data packet and is always first byte of it.
    /// Prefixes are part of the communication protocol.
    /// </summary>
    /// <see cref="CommDoc.md"/>
    public enum COMM_PREFIX : Byte
    {
        COMMS_LOGIN_FAILED_UNKNOWN  = 0x00,
        COMMS_LOGIN_FAILED_PASSWORD = 0x01,
        COMMS_LOGIN_FAILED_USERNAME = 0x02,
        COMMS_LOGIN_FAILED_INUSE    = 0x03,
        COMMS_LOGIN_SUCCESS         = 0x04,
        COMMS_REGISTER_SUCCESS      = 0x05,
        COMMS_REGISTER_FAILED_TAKEN = 0x06,
        COMMS_REGISTER_FAILED_OTHER = 0x07,
        COMMS_LOGOUT                = 0x08,
        COMMS_REACT_ISACTIVE        = 0x10,
        COMMS_ALBUM_LIST            = 0x20,
        COMMS_SONG_LIST             = 0x21,
        COMMS_PLAYLIST_LIST         = 0x22,
        COMMS_PLAYLIST_SONGS        = 0x23,
        COMMS_PLAYLIST_RESP_FAILED  = 0x30,
        COMMS_PLAYLIST_RESP_SUCCESS = 0x31,
        COMMS_ALBUM_INFO            = 0x40,
        COMMS_SONG_INFO             = 0x41,
        COMMS_SONG_RATE_FAIL        = 0x50,
        COMMS_SONG_RATE_SUCCESS     = 0x51,
        COMMS_SONG_STREAM           = 0x60,
        COMMS_COVERART_STREAM       = 0x64,
        COMMS_SONG_NOTFOUND         = 0x70,
        COMMS_COVERART_NONE         = 0x74,

        COMMC_LOGIN_TRY             = 0x80,
        COMMC_REGISTER_TRY          = 0x85,
        COMMC_LOGOUT_REQ            = 0x88,
        COMMC_REACT_ACTIVE          = 0x90,
        COMMC_SEARCH_ALBUMS         = 0xA0,
        COMMC_SEARCH_SONGS          = 0xA1,
        COMMC_INFO_ALBUM            = 0xB0,
        COMMC_INFO_SONG             = 0xB1,
        COMMC_PLAYLIST_GETLIST      = 0xC0,
        COMMC_PLAYLIST_LOAD         = 0xC1,
        COMMC_PLAYLIST_SAVE         = 0xC2,
        COMMC_PLAYLIST_DELETE       = 0xC3,
        COMMC_SONG_RATE             = 0xD0,
        COMMC_SONG_REQUEST          = 0xE0,
        COMMC_COVERART_REQUEST      = 0xE4,

        COMM_FAILURE                = 0xFF
    }
}