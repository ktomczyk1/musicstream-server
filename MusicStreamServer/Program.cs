﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MusicStreamServer
{
    /// <summary>
    /// Main class of MusicStream server.
    /// Loads data, initiates communication and provides tools for it.
    /// Containts Main() function.
    /// </summary>
    /// <copyright>Kamil Tomczyk, 2019</copyright>
    class Program
    {
        private static string CONN_ADRESS = "127.0.0.1";
        private static int CONN_PORT = 7200;

        //lists of data structures used by the server
        static List<Song> songLibrary;
        static List<Album> albumLibrary;
        static List<Rating> ratingsCollection;
        static List<Playlist> playlistCollection;
        static List<User> userCollection;

        //queues of data to save in files
        static Queue<byte[]> ratingsToSave;
        static Queue<byte[]> usersToSave;
        static Queue<byte[]> playlistsToSave;
        static Queue<uint> playlistsToDelete;

        static uint highestUserId = 1;
        static uint highestPlaylistId = 1;
        /// <summary>
        /// Offset added to album ID to get playlist ID associated with that album.
        /// </summary>
        public const uint ALBUM_PLAYLISTS_OFFSET = 1000_000;
        /// <summary>
        /// Limit of playlists one user can own.
        /// </summary>
        public const int PLAYLISTS_PER_USER_LIMIT = 60;

        /// <summary>
        /// Path to XML file where music data is stored.
        /// </summary>
        private static string musicDataPath = "./library1.xml";


        static void Main(string[] args)
        {
            LoadConf();
            LoadSongs();
            LoadUsers();
            LoadPlaylists();
            LoadRatings();
            StartSavingTask(10_000);
            StartActivityCheckingTask(10, 100);
            StartServer();

        }

        /// <summary>
        /// Starts the infinite loop that listens for connections with clients
        /// and starts communication handlers for each.
        /// </summary>
        /// <seealso cref="CommunicationHandler"/>
        static void StartServer()
        {
            IPAddress ipAddress = IPAddress.Parse(CONN_ADRESS);
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, CONN_PORT);

            try
            {
                Socket servSocket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                servSocket.NoDelay = true;
                servSocket.Bind(localEndPoint);
                servSocket.Listen(32);
                Console.WriteLine("Server started and awaiting connections...");
                while(true)
                {
                    try
                    {
                        Socket handSocket = servSocket.Accept();
                        new CommunicationHandler(handSocket);
                    }
                    catch(Exception)
                    {
                        Console.WriteLine("Failed to accept a socket connection or create communicatin handler.");
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("MAJOR ERROR - FAILED TO START THE SERVER");
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Starts task regularly checking for data to save in files.
        /// That data includes new users, playlists, ratings and
        /// deleted playlist.
        /// </summary>
        /// <param name="delay">Delay between next run of checking
        /// for data to save, in miliseconds.</param>
        /// <remarks>Doesn't throw an exception when saving
        /// the data fails, but writes error message. 
        /// Observe console output.</remarks>
        static void StartSavingTask(int delay)
        {
            Task t = new Task(async delegate
            {
                while(true)
                {
                    try
                    {
                        //saving user data
                        if (usersToSave.Count > 0)
                        {
                            FileInfo ufo = new FileInfo("./userDatafile.bin");
                            FileStream fs = ufo.Open(FileMode.Open, FileAccess.Write, FileShare.None);
                            fs.Seek(0, SeekOrigin.End);
                            byte[] buffer;
                            while (usersToSave.Count > 0)   //saving subsequent users
                            {
                                buffer = usersToSave.Dequeue();
                                try
                                {
                                    fs.Write(buffer, 0, buffer.Length);
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Failed when writing user data.");
                                    //eventually do something with buffer...
                                }
                            }
                            fs.Close();
                        }
                        //saving playlist data
                        if (playlistsToSave.Count > 0)
                        {
                            FileInfo pfo = new FileInfo("./playlistDatafile.bin");
                            FileStream fs = pfo.Open(FileMode.Open, FileAccess.Write, FileShare.None);
                            fs.Seek(0, SeekOrigin.End);
                            byte[] buffer;
                            while (playlistsToSave.Count > 0)  //saving subsequent playlists
                            {
                                buffer = playlistsToSave.Dequeue();
                                try
                                {
                                    fs.Write(buffer, 0, buffer.Length);
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Failed when writing playlist data.");
                                    //eventually do something with buffer...
                                }
                            }
                            fs.Close();
                        }
                        //saving omitted playlist list
                        if (playlistsToSave.Count > 0)
                        {
                            FileInfo dfo = new FileInfo("./playlistDeleteList.bin");
                            FileStream fs = dfo.Open(FileMode.Open, FileAccess.Write, FileShare.None);
                            fs.Seek(0, SeekOrigin.End);
                            byte[] buffer;
                            while (playlistsToDelete.Count > 0)   //saving subsequent IDs to omit
                            {
                                buffer = BitConverter.GetBytes(playlistsToDelete.Dequeue());
                                try
                                {
                                    fs.Write(buffer, 0, 4);
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Failed when writing omitted playlist data.");
                                    //eventually do something with buffer...
                                }
                            }
                            fs.Close();
                        }
                        //saving ratings data
                        if (ratingsToSave.Count > 0)
                        {
                            FileInfo rfo = new FileInfo("./ratingsDatafile.bin");
                            FileStream fs = rfo.Open(FileMode.Open, FileAccess.Write, FileShare.None);
                            fs.Seek(0, SeekOrigin.End);
                            byte[] buffer;
                            while (ratingsToSave.Count > 0)   //saving subsequent ratings
                            {
                                buffer = ratingsToSave.Dequeue();
                                try
                                {
                                    fs.Write(buffer, 0, 9);
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Failed when writing rating data.");
                                    //eventually do something with buffer...
                                }
                            }
                            fs.Close();
                        }
                    }
                    catch(Exception)
                    {
                        Console.WriteLine("Major ERROR while saving data to files.");
                    }

                    await Task.Delay(delay);
                }
            });
            t.Start();
        }
        /// <summary>
        /// Starts task regularly checking for users activity,
        /// and logging out unactive ones.
        /// </summary>
        /// <param name="delay">Delay between next run of checking
        /// user activity, in seconds.</param>
        /// <param name="timeout">Time after which inactive users should 
        /// be logged out, in seconds. Must be at least 2 times
        /// greater than delay.</param>
        /// <exception cref="ArgumentException">Thrown when timeout is too small.</exception>
        static void StartActivityCheckingTask(int delay, int timeout)
        {
            if (timeout < 2 * delay)
                throw new ArgumentException("Timeout must be at least 2 times greater than delay.");

            Task t = new Task(async delegate
            {
                while(true)
                {
                    foreach(User u in userCollection)
                    {
                        if(u.IsLogged)
                        {
                            //checking shorter timeout firstly forces logged but inactive
                            //users to respond
                            if (u.CheckTimeout((uint)(timeout - 2 * delay)))
                                u.UnactiveLogout((uint)timeout);
                        }
                    }
                    await Task.Delay(delay*1000);
                }
            });
            t.Start();
        }

        /// <summary>
        /// Loads server configuration data from .conf file
        /// </summary>
        static void LoadConf()
        {
            try
            {
                FileInfo ufo = new FileInfo("./settings.conf");
                if(ufo.Exists)
                {
                    FileStream fs = ufo.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
                    StreamReader sr = new StreamReader(fs);
                    string line = "";
                    string[] elements;
                    while(!sr.EndOfStream)
                    {
                        line = sr.ReadLine();
                        if(line.Length>0)
                        {
                            if(line[0] != '#' && line[0] != '\n')
                            {
                                if(line.Contains("="))
                                {
                                    elements = line.Split('=', 2);
                                    switch(elements[0].ToLower()) //checking key
                                    {
                                        case "ipadress":
                                            CONN_ADRESS = elements[1].Trim();
                                            break;
                                        case "port":
                                            try
                                            {
                                                CONN_PORT = int.Parse(elements[1].Trim());
                                            }
                                            catch (Exception) { }
                                            break;
                                        case "library":
                                            musicDataPath = elements[1].Trim();
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    sr.Close();
                }
                else
                {
                    FileStream fs = ufo.Open(FileMode.OpenOrCreate, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);
                    sw.WriteLine("# MusicStream server config file");
                    sw.WriteLine("ipadress=" + CONN_ADRESS);
                    sw.WriteLine("port=" + CONN_PORT.ToString());
                    sw.WriteLine("library=" + musicDataPath);
                    sw.Close();
                }
            }
            catch(Exception)
            {
                Console.Write("ERROR: major error while configuration file reading.");
            }
        }
        /// <summary>
        /// Loads all the songs and albums from XML file where their data is saved.
        /// Used at server startup only.
        /// </summary>
        /// <remarks>Doesn't throw Exceptions in case of failure, but writes
        /// ERROR message. Always observe console output.</remarks>
        static void LoadSongs()
        {
            songLibrary = new List<Song> { };
            albumLibrary = new List<Album> { };
            playlistCollection = new List<Playlist> { };
            try
            {
                //file where music data is saved
                FileStream fs = new FileStream(musicDataPath, FileMode.Open);
                XDocument doc = XDocument.Load(fs);
                List<XElement> albEls = new List<XElement>(doc.Element("musicLibary").Elements("album"));
                Album tempAlbum;
                List<Song> tSongList;
                for(int i = 0; i < albEls.Count; i++)  //reading subsequentalbums
                {
                    try
                    {
                        (tempAlbum, tSongList) = Album.GetFromXML(albEls[i]);
                        songLibrary.AddRange(tSongList);
                        albumLibrary.Add(tempAlbum);
                    }
                    catch (Exception) { }
                }
                foreach (Album a in albumLibrary)
                {
                    a.RecalcTotalFormalLength(songLibrary);

                    //playlist associated with albums
                    Playlist p = new Playlist(a.Id + ALBUM_PLAYLISTS_OFFSET, 0, a.FormalLength, a.SongIDList);
                    p.SetName(a.Name + "-" + a.Artist);
                    playlistCollection.Add(p);
                }
            }
            catch (Exception)
            {
                Console.Write("ERROR: major error while song library reading.");
            }
            Console.WriteLine("Loaded {0} songs and {1} albums.", songLibrary.Count, albumLibrary.Count);
        }
        /// <summary>
        /// Finds songs that match specified criteria, sorted in specified order
        /// </summary>
        /// <param name="pattern">Regex pattern to match</param>
        /// <param name="places">4 bits specifying where to look for matches</param>
        /// <param name="order">4 bits specifying the order of sorted list</param>
        /// <returns>Up to 60 IDs of matching songs</returns>
        /// <see>COMMC_SEARCH_SONGS in CommDoc.md</see>
        /// <seealso cref="FindAlbumIds(string, byte, byte, int)"/>
        public static List<uint> FindSongIds(string pattern, byte places, byte order)
        {
            //looking for matches
            List<ListedSong> matchingSongs = new List<ListedSong> { };
            if (pattern.Length > 0)
            {
                Regex regExp = new Regex(Regex.Escape(pattern), RegexOptions.IgnoreCase);
                foreach (Song s in songLibrary)
                {
                    if ((places & 1) > 0)       //checking song names
                    {
                        if (regExp.IsMatch(s.Name))
                        {
                            matchingSongs.Add(new ListedSong(s, 0));
                            continue;
                        }
                    }
                    if ((places & 2) > 0)  //checking album names
                    {
                        if (regExp.IsMatch(albumLibrary.Find(x => x.Id == s.AlbumId).Name))
                        {
                            matchingSongs.Add(new ListedSong(s, 0));
                            continue;
                        }
                    }
                    if ((places & 4) > 0)  //checking artists
                    {
                        if (regExp.IsMatch(s.Artist))
                        {
                            matchingSongs.Add(new ListedSong(s, 0));
                            continue;
                        }
                    }
                }
            }
            else
            {
                foreach (Song s in songLibrary)
                    matchingSongs.Add(new ListedSong(s, 0));
            }

            //reordering the list
            switch(order&7)
            {
                case 0:
                    matchingSongs.Sort(ListedSong.CompareNames);
                    break;
                case 1:
                    matchingSongs.Sort(ListedSong.CompareArtists);
                    break;
                case 2:
                    matchingSongs.Sort(ListedSong.CompareLengths);
                    break;
                
            }
            if ((order & 8) > 0)
                matchingSongs.Reverse();

            //recovering IDs
            List<uint> ids = new List<uint> { };
            foreach (ListedSong s in matchingSongs)
                ids.Add(s.Id);
            return ids;
        }
        /// <summary>
        /// Finds albums that match specified criteria, sorted in specified order
        /// </summary>
        /// <param name="pattern">Regex pattern to match</param>
        /// <param name="places">4 bits specifying where to look for matches</param>
        /// <param name="order">4 bits specifying the order of sorted list</param>
        /// <returns>Up to 60 IDs of matching albums</returns>
        /// <see>COMMC_SEARCH_ALBUMS in CommDoc.md</see>
        /// <seealso cref="FindSongIds(string, byte, byte, int)"/>
        public static List<uint> FindAlbumIds(string pattern, byte places, byte order)
        {
            //looking for matches
            List<Album> matchingAlbums = new List<Album> { };
            if (pattern.Length > 0)
            {
                Regex regExp = new Regex(Regex.Escape(pattern), RegexOptions.IgnoreCase);
                foreach (Album a in albumLibrary)
                {
                    if ((places & 1) > 0)       //checking song names
                    {
                        List<Song> albumSongs = songLibrary.FindAll(x => a.SongIDList.Contains(x.Id));
                        if (albumSongs.Exists(x => regExp.IsMatch(x.Name)))
                        {
                            matchingAlbums.Add(a);
                            continue;
                        }
                    }
                    if ((places & 2) > 0)  //checking album names
                    {
                        if (regExp.IsMatch(a.Name))
                        {
                            matchingAlbums.Add(a);
                            continue;
                        }
                    }
                    if ((places & 4) > 0)  //checking artists
                    {
                        if (regExp.IsMatch(a.Artist))
                        {
                            matchingAlbums.Add(a);
                            continue;
                        }
                    }
                }
            }
            else
                matchingAlbums = albumLibrary;

            //reordering the list
            switch (order & 7)
            {
                case 0:
                    matchingAlbums.Sort(Album.CompareNames);
                    break;
                case 1:
                    matchingAlbums.Sort(Album.CompareArtists);
                    break;
                case 2:
                    matchingAlbums.Sort(Album.CompareLengths);
                    break;
            }
            if ((order & 8) > 0)
                matchingAlbums.Reverse();

            //recovering IDs
            List<uint> ids = new List<uint> { };
            foreach (Album a in matchingAlbums)
                ids.Add(a.Id);
            return ids;
        }

        /// <summary>
        /// Loads all the users from binary file where they're saved
        /// Used at server startup only.
        /// </summary>
        /// <remarks>Doesn't throw Exceptions in case of failure, but writes
        /// ERROR message. Always observe console output.</remarks>
        static void LoadUsers()
        {
            userCollection = new List<User> { };
            usersToSave = new Queue<byte[]> { };
            try
            {
                //file where user data is saved
                FileInfo ufo = new FileInfo("./userDatafile.bin");
                if(ufo.Exists)
                {
                    FileStream fs = ufo.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
                    byte[] buffer = new byte[56];
                    uint id;
                    string username, email;
                    byte[] passHash = new byte[32];
                    byte emailLen, perms;
                    byte[] emailBuffer = new byte[256];
                    User nUser;
                    while((fs.Length-fs.Position)>=58) //reading subsequent users
                    {
                        try
                        {
                            //file structure: id-nickname-password-permissions-emailLength-email
                            fs.Read(buffer, 0, 56);
                            id = BitConverter.ToUInt32(buffer, 0);
                            username = Encoding.ASCII.GetString(buffer, 4, 20);
                            Array.Copy(buffer, 24, passHash, 0, 32);
                            perms = (byte)fs.ReadByte();
                            emailLen = (byte)fs.ReadByte();
                            fs.Read(emailBuffer, 0, emailLen);
                            email = Encoding.UTF8.GetString(emailBuffer, 0, emailLen);
                            nUser = new User(id, username, passHash, email, perms);
                            userCollection.Add(nUser);
                            highestUserId = Math.Max(highestUserId, id);
                        }
                        catch (Exception) { }
                    }
                    fs.Close();
                }
                else  //if file doesn't exist yet
                {
                    ufo.Create();
                }
            }
            catch (Exception)
            {
                Console.Write("ERROR: major error while user data reading.");
            }
            Console.WriteLine("Loaded {0} user profiles.", userCollection.Count);
        }
        /// <summary>
        /// User ID of user of given username.
        /// </summary>
        /// <param name="nickname">Username of choosen user</param>
        /// <returns>ID of found user, null if user not found.</returns>
        public static uint? GetUserId(string nickname)
        {
#if DEBUG
            //allows for quick logging in to non-existing accounts while debugging
            if (nickname.TrimEnd().Length < 3)
                return 0;
#endif
            int index = userCollection.FindIndex(x => x.Username == nickname);
            if (index == -1)
                return null;
            else
                return userCollection[index].Id;
        }
        /// <summary>
        /// Check if given email adress is already used by any registered user.
        /// </summary>
        /// <param name="email">Email adress</param>
        /// <returns>True if email is used by someone, false otherwise</returns>
        public static bool IsEmailTaken(string email) { return userCollection.Exists(x => x.Email == email); }
        /// <summary>
        /// Try to login user with given parameters.
        /// </summary>
        /// <param name="id">ID of user to login</param>
        /// <param name="passHash">SHA256 has of password provided by user</param>
        /// <returns>True if logged in, false if failed for any reason.</returns>
        /// <remarks>Reason of failure should be checked by using other funcions
        /// <see cref="GetPasswordHash(string)"/><see cref="GetUserId(string)"/></remarks>
        public static bool TryUserLogin(uint id, byte[] passHash)
        {
#if DEBUG
            //allows for quick logging in to non-existing accounts while debugging
            if (id == 0)
                return true;
#endif
            if (!userCollection.Exists(x => x.Id == id))
                return false;
            bool loginCheck = userCollection.Find(x => x.Id == id).TryLogin(passHash);
            //below: if someone's trying to login to online account - check if that
            //user is active by forcing to respond in 15 seconds
            if (loginCheck == false)
                userCollection.Find(x => x.Id == id).ForceTimeout(15);
            return loginCheck;
        }
        /// <summary>
        /// Logouts the user.
        /// </summary>
        /// <param name="id">ID of user to logout</param>
        public static void LogoutUser(uint id)
        {
            if (userCollection.Exists(x => x.Id == id))
                userCollection.Find(x => x.Id == id).Logout();
        }
        /// <summary>
        /// Adds event handler to logout event of selected User object.
        /// </summary>
        /// <param name="userId">ID of selected user</param>
        /// <param name="func">Event handler</param>
        /// <see cref="User.userLogoutEvent"/>
        public static void AssignToUserLogoutEvent(uint userId, UserEvent func)
        {
            if (userCollection.Exists(x => x.Id == userId))
                userCollection.Find(x => x.Id == userId).userLogoutEvent += func;
        }
        /// <summary>
        /// Adds event handler to timeout checking event of selected User object.
        /// </summary>
        /// <param name="userId">ID of selected user</param>
        /// <param name="func">Event handler</param>
        /// <see cref="User.timeoutCheckEvent"/>
        public static void AssingToTimeoutCheckEvent(uint userId, UserEvent func)
        {
            if (userCollection.Exists(x => x.Id == userId))
                userCollection.Find(x => x.Id == userId).timeoutCheckEvent += func;
        }
        /// <summary>
        /// Update last activity time of selected user.
        /// </summary>
        /// <param name="userId">ID of selected user</param>
        public static void UpdateUserActivity(uint userId)
        {
            if (userCollection.Exists(x => x.Id == userId))
                userCollection.Find(x => x.Id == userId).UpdateActivity();
        }
        /// <summary>
        /// Password hash that should be provided to log into specified account
        /// </summary>
        /// <param name="nickname">Username of selected user account</param>
        /// <returns>SHA256 hash of registered password</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown when user with given username doesn't exist.</exception>
        public static byte[] GetPasswordHash(string nickname)
        {
#if DEBUG
            //allows for quick logging in to non-existing accounts while debugging
            //with password same as username
            if (nickname.TrimEnd().Length < 3)
            {
                using (SHA256 sha256Hash = SHA256.Create())
                {
                    byte[] res = sha256Hash.ComputeHash(Encoding.ASCII.GetBytes(nickname));
                    return res;
                }
            }
#endif
            int index = userCollection.FindIndex(x => x.Username == nickname);
            if (index > -1)
                return userCollection[index].PasswordHash;
            else
                throw new ArgumentOutOfRangeException("User doesn't exist.");
        }
        /// <summary>
        /// Register account of user with given parameters.
        /// </summary>
        /// <param name="nickname">Username of user</param>
        /// <param name="email">Email adress of user</param>
        /// <param name="passHash">SHA256 hash of password that'll need to 
        /// be provided while logging into that account.</param>
        /// <returns>User ID of registered account</returns>
        /// <exception cref="ArgumentException">Thrown when user with that username
        /// or email already is registered.</exception>
        /// <remarks>Funcion may register user and return ID, but
        /// program may fail to save user data in file.
        /// <see cref="StartSavingTask(int)"/></remarks>
        public static uint RegisterUser(string nickname, string email, byte[] passHash)
        {
            if (userCollection.Exists(x => x.Username == nickname))
                throw new ArgumentException("User already exists");
            if (userCollection.Exists(x => x.Email == email))
                throw new ArgumentException("User already exists");
            uint id = ++highestUserId;
            User nUser = new User(id, nickname, passHash, email, 0);
            userCollection.Add(nUser);

            //sending registration email
            try
            {
                /*string mailContent = "Welcome " + nickname + "!\nYou have been successfully registered.";
                MailMessage mailM = new MailMessage("noreply@musicStream", email, 
                    "Registration confirmed", mailContent);

                SmtpClient postClient = new SmtpClient("localhost");

                postClient.Send(mailM);
                Console.WriteLine("Sent an email");*/
            }
            catch (Exception) { } //doesn't work, maybe for future updates

            //constructing data to save in file
            try
            {
                byte[] emailArr = Encoding.UTF8.GetBytes(email);
                byte emailLen = (byte)emailArr.Length;
                byte[] toFileArr = new byte[58 + emailLen];
                Array.Copy(BitConverter.GetBytes(id), 0, toFileArr, 0, 4);
                Array.Copy(Encoding.ASCII.GetBytes(nickname.PadRight(20), 0, 20), 0, toFileArr, 4, 20);
                Array.Copy(passHash, 0, toFileArr, 24, 32);
                toFileArr[56] = 0;
                toFileArr[57] = emailLen;
                Array.Copy(emailArr, 0, toFileArr, 58, emailLen);
                usersToSave.Enqueue(toFileArr);
            }
            catch (Exception) { Console.WriteLine("Failed to enqueue user data to save;"); } 
                //probably to change or expand...
            return id;
        }
        
        /// <summary>
        /// Album of given ID
        /// </summary>
        /// <param name="id">ID of selected album</param>
        /// <returns>The album if found, null otherwise.</returns>
        public static Album GetAlbum(uint id)
        {
            return albumLibrary.Find(x => x.Id == id);
        }
        /// <summary>
        /// Song of given ID
        /// </summary>
        /// <param name="id">ID of selected song</param>
        /// <returns>The song if found, null otherwise.</returns>
        public static Song GetSong(uint id)
        {
            return songLibrary.Find(x => x.Id == id);
        }

        /// <summary>
        /// Loads all the playlists from binary file where they're saved
        /// excluding playlist from file containing list of playlists to delete.
        /// Used at server startup only.
        /// </summary>
        /// <remarks>Doesn't throw Exceptions in case of failure, but writes
        /// ERROR message. Always observe console output.</remarks>
        static void LoadPlaylists()
        {
            if (playlistCollection == null)
                playlistCollection = new List<Playlist> { };
            playlistsToSave = new Queue<byte[]> { };
            playlistsToDelete = new Queue<uint> { };

            try
            {
                //file where playlist data is saved
                FileInfo pfo = new FileInfo("./playlistDatafile.bin");
                if (pfo.Exists)
                {
                    FileStream fs = pfo.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
                    byte[] buffer = new byte[43];
                    uint id, owner;
                    DateTime creationTime;
                    byte trackCount;
                    string name;
                    List<uint> songs;
                    byte[] tempBuffer = new byte[4];
                    while ((fs.Length - fs.Position) >= 43)  //reading subsequent playlists
                    {
                        try
                        {
                            //data order id-owner-creationTime-trackCount-name-songs
                            fs.Read(buffer, 0, 43);
                            id = BitConverter.ToUInt32(buffer, 0);
                            owner = BitConverter.ToUInt32(buffer, 4);
                            creationTime = new DateTime(BitConverter.ToInt64(buffer, 8));
                            trackCount = buffer[16];
                            name = Encoding.UTF8.GetString(buffer, 17, 26);
                            songs = new List<uint> { };
                            for (int i = 0; i < trackCount; i++) //reading song IDs in playlist
                            {
                                fs.Read(tempBuffer, 0, 4);
                                songs.Add(BitConverter.ToUInt32(tempBuffer,0));
                            }
                            Playlist nPlaylist = new Playlist(id, owner, 0, songs);
                            nPlaylist.SetName(name);
                            nPlaylist.SetCreationTime(creationTime);
                            playlistCollection.Add(nPlaylist);
                            highestPlaylistId = Math.Max(highestPlaylistId,id);
                        }
                        catch (Exception) { }
                    }
                    fs.Close();
                }
                else   //if file doesn't exist yet
                {
                    pfo.Create();
                }
            }
            catch (Exception)
            {
                Console.Write("ERROR: major error while playlist data reading.");
            }

            try
            {
                //file where playlists to delete are saved
                FileInfo dfo = new FileInfo("./playlistDeleteList.bin");
                if (dfo.Exists)
                {
                    FileStream fs = dfo.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
                    byte[] buffer = new byte[4];
                    uint id;
                    while ((fs.Length - fs.Position) >= 4)  //reading subsequent playlist IDs
                    {
                        try
                        {
                            fs.Read(buffer, 0, 4);
                            id = BitConverter.ToUInt32(buffer, 0);
                            if (id < ALBUM_PLAYLISTS_OFFSET)
                                playlistCollection.RemoveAll(x => x.Id == id);
                        }
                        catch (Exception) { }
                    }
                    fs.Close();
                }
                else  //if file doesn't exist yet
                {
                    dfo.Create();
                }
            }
            catch (Exception)
            {
                Console.Write("ERROR: major error while deleted playlist list reading.");
            }

            Console.WriteLine("Loaded {0} user-owned playlists.", playlistCollection.Count-albumLibrary.Count);
        }
        /// <summary>
        /// Playlist of given ID
        /// </summary>
        /// <param name="id">ID of selected playlist</param>
        /// <returns>The playlist if found, new Playlist structure otherwise.</returns>
        public static Playlist GetPlaylist(uint id)
        {
            return playlistCollection.Find(x => x.Id == id);
        }
        /// <summary>
        /// Find all playlists that belong to given user.
        /// </summary>
        /// <param name="userId">ID of selected user</param>
        /// <returns>List of IDs of all playlist that belong to user</returns>
        public static List<uint> FindPlaylistsOfUser(uint userId)
        {
            List<Playlist> matches = playlistCollection.FindAll(x => x.Owner == userId);
            List<uint> ans = new List<uint> { };
            foreach (Playlist p in matches)
                ans.Add(p.Id);
            return ans;
        }
        /// <summary>
        /// Registers the given playlist (if possible) in data structures.
        /// </summary>
        /// <param name="list">The playlist to register</param>
        /// <returns>ID assigned to registered playlist</returns>
        /// <remarks>
        /// <para>Funcion may register playlist and return ID, but
        /// program may fail to save playlist in file.
        /// <see cref="StartSavingTask(int)"/></para>
        /// <para>Function doesn't check for limit of playlists
        /// owned by 1 user, do it before calling it if you want to limit it.</para>
        /// </remarks>
        public static uint SavePlaylist(Playlist list)
        {
            uint nId = ++highestPlaylistId; //1 higher than highest existing ID and auto-increment
            playlistCollection.Add(list.ReindexedCopy(nId));
            //constring byte array to save into the file
            try
            {
                byte songCount = (byte)list.Songs.Count;
                byte[] toFileArr = new byte[43 + 4 * songCount];

                //data order id-owner-creationTime-trackCount-name-songs
                Array.Copy(BitConverter.GetBytes(nId), 0, toFileArr, 0, 4);
                Array.Copy(BitConverter.GetBytes(list.Owner), 0, toFileArr, 4, 4);
                Array.Copy(BitConverter.GetBytes(list.CreationTime.Ticks), 0, toFileArr, 8, 8);
                toFileArr[16] = songCount;
                Array.Copy(Encoding.UTF8.GetBytes(list.Name.PadRight(26)), 0, toFileArr, 17, 26);
                for (int i = 0; i < songCount; i++)
                    Array.Copy(BitConverter.GetBytes(list.Songs[i]), 0, toFileArr, 43 + 4 * i, 4);
                playlistsToSave.Enqueue(toFileArr);
            }
            catch (Exception) { Console.WriteLine("Failed to enqueue playlist data to save."); return 0; }
            return nId;
        }
        /// <summary>
        /// Register the given playlist (if possible) in data structures to
        /// being omitted/deleted while loading and deletes it from list.
        /// </summary>
        /// <param name="id">ID of playlist to delete</param>
        /// <returns>True if playlist can be deleted, 
        /// false otherwise (playlist with that ID doesn't exist or
        /// is album-assigned playlist)</returns>
        /// <remarks>Funcion may delete playlist from list and register it in data structures, but
        /// program may fail to save it in file of playlists to omit/delete.
        /// <see cref="StartSavingTask(int)"/></remarks>
        public static bool DeletePlaylist(uint id)
        {
            if(id<ALBUM_PLAYLISTS_OFFSET && playlistCollection.Exists(x=> x.Id==id) )
            {
                playlistsToDelete.Enqueue(id);
                playlistCollection.RemoveAll(x => x.Id == id);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Loads all the ratings from binary file where they're saved.
        /// Used at server startup only.
        /// </summary>
        /// <remarks>Doesn't throw Exceptions in case of failure, but writes
        /// ERROR message. Always observe console output.</remarks>
        static void LoadRatings()
        {
            ratingsCollection = new List<Rating> { };
            ratingsToSave = new Queue<byte[]> { };

            try
            {
                //ratings data file
                FileInfo rfo = new FileInfo("./ratingsDatafile.bin");
                if (rfo.Exists)
                {
                    FileStream fs = rfo.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
                    byte[] buffer = new byte[9];
                    uint song, user;
                    Rating nRating;
                    while ((fs.Length - fs.Position) >= 9) //reading subsequent ratings
                    {
                        try
                        {
                            //data order songid-ownerid-rating
                            fs.Read(buffer, 0, 9);
                            song = BitConverter.ToUInt32(buffer, 0);
                            user = BitConverter.ToUInt32(buffer, 4);
                            nRating = new Rating(user, song, buffer[8]);
                            if (!ratingsCollection.Exists(x => (x.SongId == song && x.UserId == user)))
                                ratingsCollection.Add(nRating);
                        }
                        catch (Exception) { }
                    }
                    fs.Close();
                }
                else    //if file doesn't exist yet
                {
                    rfo.Create();
                }
            }
            catch (Exception)
            {
                Console.Write("ERROR: major error while ratings data reading.");
            }
            Console.WriteLine("Loaded {0} ratings.", ratingsCollection.Count);
        }
        /// <summary>
        /// Rating of selected song.
        /// </summary>
        /// <param name="id">ID of choosen song.</param>
        /// <returns>Rating of selected song (mean of all user ratings).
        /// Returns 0 if song with given ID doesn't exist or has no ratings.</returns>
        public static float GetSongRating(uint id)
        {
            List<Rating> matches = ratingsCollection.FindAll(x => x.SongId == id);
            if (matches.Count == 0)
                return 0;
            uint totalRating = 0;
            foreach (Rating r in matches)
                totalRating += r.Value;
            return (float)totalRating / 32 / matches.Count;
        }
        /// <summary>
        /// Rating of selected album.
        /// </summary>
        /// <param name="id">ID of choosen album</param>
        /// <returns>Mean from ratings of all found songs that belong to that album
        /// and have been rated. 0 if no song matches criteria (no song is rated,
        /// album doesn't have songs or album with given ID doesn't exist.</returns>
        public static float GetAlbumRating(uint id)
        {
            float totalRating = 0;
            uint totalRated = 0;
            Album a = albumLibrary.Find(x => x.Id == id);
            if (a == null)
                return 0;
            foreach (uint sid in a.SongIDList)
            {
                float rat = GetSongRating(sid);
                if (rat > 0)
                {
                    totalRated++;
                    totalRating += rat;
                }
            }
            return (totalRated > 0) ? (totalRating / totalRated) : 0;
        }
        /// <summary>
        /// Registers the given rating (if possible) in data structures.
        /// </summary>
        /// <param name="rating">The rating to register</param>
        /// <returns>True if rating is correct and possible to register
        /// false, if rating of that song by that user already exists</returns>
        /// <remarks>Funcion may register rating and return true, but
        /// program may fail to save rating in file.
        /// <see cref="StartSavingTask(int)"/></remarks>
        public static bool RateSong(Rating rating)
        {
            if (!ratingsCollection.Exists(x => (x.SongId == rating.SongId) && (x.UserId == rating.UserId)))
            {
                ratingsCollection.Add(rating);
                //saving rating to file
                try
                {
                    byte[] toFileArr = new byte[9];
                    //data order songid-ownerid-rating
                    Array.Copy(BitConverter.GetBytes(rating.SongId), 0, toFileArr, 0, 4);
                    Array.Copy(BitConverter.GetBytes(rating.UserId), 0, toFileArr, 4, 4);
                    toFileArr[8] = rating.Value;
                    ratingsToSave.Enqueue(toFileArr);
                }
                catch (Exception) { Console.WriteLine("Failed to enqeue rating data to save."); }
                //^ probably to change or expand later...
                return true;
            }
            else
                return false;
        }
    }
}
